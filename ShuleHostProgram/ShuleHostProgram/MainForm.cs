﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.IO;
using System.IO.Ports;
using AgAnalyticApi;
using System.Threading;
using ShuleHostProgram.ShuleWebService;

namespace ShuleHostProgram
{
    public partial class MainForm : Form
    {
        enum States { IDLE, CONNECTING, CONNECTED };

        bool automatic = true;         // Decide whether to use automatic or manual port selection.
        bool uselock = false;           // Decide whether to use lock on serial ports. Set to true if threads running concurrently.
        object thelock = new object();  // Use to lock sections of codes that cannot occur at the same time.                                     

        private AgAnApi api = null;
       
        SerialPort pumpPort = null;
        SerialPort nictorPort = null;
     
        volatile bool pumpConnected = false;
        volatile bool nictorConnected = false;
        volatile bool webConnected = false;

        volatile ushort pumpFlowrate = 0;
        volatile ushort pumpLocalSetFlowRate = 0; 
        volatile ushort pumpRemoteSetFlowRate = 0; 
        volatile ushort pumpPressure = 0;
        volatile string allPumpStatus = "unknown";

        int pumpConnCount = 0;
        int nictorConnCount = 0;
        int webCount = 0;

        string[] allSerialPortNames = null;

        public MainForm()
        {
            InitializeComponent();
            Init();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            if (automatic)
            {
                connectButton.Visible = false;
                disconnectButton.Visible = false;
                serialGroupBox.Visible = false;

                EstablishComPortLinks();
            }
            else
            {
                connGroupBox.Visible = false;
            }
        }

        private void Init()
        {
            allSerialPortNames = GetPortNames();
        
            log4net.Config.XmlConfigurator.Configure();
            
            BaudDropDownList.SelectedIndex = 0;
            DataDropDownList.SelectedIndex = 3;
            ParityDropDownList.SelectedIndex = 0;
            StopDropDownList.SelectedIndex = 1;
        }

        private string[] GetPortNames()
        {
            string[] names = allSerialPortNames = SerialPort.GetPortNames();

            foreach (string portName in allSerialPortNames)
            {
                PortDropDownList.Items.Add(portName);
                nictorComboBox.Items.Add(portName);
                pumpPortComboBox.Items.Add(portName);
            }

            return names;
        }

        private void EstablishComPortLinks()
        {
            // Note : Pump and Nictor connection are on the same thread.
            // Otherwise, if one thread happens to ALWAYS grab a port COMa all
            // the time for testing, it will prevent others from grabbing that
            // port for their own testing; eventhough port COMa may be valid
            // for one of the thread. 
            if (!pumpBackgroundWorker.IsBusy)
            {
                pumpBackgroundWorker.RunWorkerAsync();
            }

            // This thread check for the web connection.
            if (!webBackgroundWorker.IsBusy)
            {
                webBackgroundWorker.RunWorkerAsync();
            }

            // This thread carry out the while loop of nictor.
            if (!actionBackgroundWorker.IsBusy)
            {
                actionBackgroundWorker.RunWorkerAsync();
            }

            // Get flow rate and pump status is carried out in this thread.
            if (!pumpStatusBackgroundWorker.IsBusy)
            {
                pumpStatusBackgroundWorker.RunWorkerAsync();
            }
        }

        /// <summary>
        /// Try to search for a port connected to the pump. 
        /// </summary>
        /// <returns></returns>
        private string EstablishPumpConn()
        {
            string msg = null;

            ResetPumpPort();

            if (allSerialPortNames != null && pumpPort == null)
            {
                for (int i = 0; i < allSerialPortNames.Length; i++)
                {
                    string portName = allSerialPortNames[i];

                    if (uselock)
                    {
                        lock (thelock) // Prevent both device access the same port.
                        {
                            // Avoid the nictor port already in use.
                            if (nictorPort != null && nictorPort.PortName == portName)
                            {
                                //msg = "Pump cannot access port " + portName;
                                continue;
                            }
                            // Create serial port.
                            pumpPort = new SerialPort(portName);
                        }
                    }
                    else
                    {
                        // Avoid the nictor port already in use.
                        if (nictorPort != null && nictorPort.PortName == portName)
                        {
                            //msg = "Pump cannot access port " + portName;
                            continue;
                        }
                        // Create serial port.
                        pumpPort = new SerialPort(portName);
                    }

                    // Try each port.
                    try
                    {
                        SetBaudRate();
                        SetDataBits();
                        SetParity();
                        SetStopBits();
                        pumpPort.Open();

                        // Create modbus Api.
                        api = new AgAnApi(pumpPort);
                        api.ReadTimeOut = (int)ReadTimeoutNumUpDown.Value;
                        api.WriteTimeOut = (int)writeTimeoutNumUpDown.Value;
                        api.Retries = (int)retiresNumUpDown.Value;

                        string error = null;
                        bool b = api.GetPumpStatus(out error);  // Try any command eg. get pump status.

                        if (error == null) // Port is ok for use.
                        {
                            PortDropDownList.SelectedIndex = i;
                            msg = String.Format("Pump is connected. ({0})", portName);
                            break;
                        }
                        else // Port is invalid. Reset to null.
                        {
                            ResetPumpPort();
                            msg = String.Format("Could not connect to pump. Check connections. Retrying...({0}, {1})", ++pumpConnCount, portName);
                        }
                    }
                    catch (Exception ex) // Port is invalid for whatever reasons.
                    {
                        ResetPumpPort();
                        msg = String.Format("Could not connect to pump. Check connections. Retrying...({0}, {1})", ++pumpConnCount, portName);
                    }
                }
            }
   
            return msg;
        }

        /// <summary>
        /// Try to search for a port connected to the nictor.
        /// </summary>
        /// <returns></returns>
        private string EstablishNictorConn()
        {
            string msg = null;

            ResetNictorPort();

            if (allSerialPortNames != null && nictorPort == null)
            {
                for (int i = 0; i < allSerialPortNames.Length;  i++)
                {
                    string portName = allSerialPortNames[i];

                    if (uselock)
                    {
                        lock (thelock) // Prevent both device access the same port.
                        {
                            // Avoid the pump port already in use.
                            if (pumpPort != null && pumpPort.PortName == portName)
                            {
                                msg = "Nictor cannot access port ";
                                continue;
                            }
                            // Create serial port.
                            nictorPort = new SerialPort(portName);
                        }
                    }
                    else
                    {
                        // Avoid the pump port already in use.
                        if (pumpPort != null && pumpPort.PortName == portName)
                        {
                            msg = "Nictor cannot access port ";
                            continue;
                        }
                        // Create serial port.
                        nictorPort = new SerialPort(portName);
                    }

                    try
                    {
                        // Wait for 2.5 minutes in case clock of nictor is not exactly 2 mins.
                        nictorPort.ReadTimeout = 150000;  // SerialPort.InfiniteTimeout; ;
                        nictorPort.WriteTimeout = 150000; // SerialPort.InfiniteTimeout; ;
                        //
                        nictorPort.BaudRate = 38400;
                        nictorPort.Parity = Parity.None;
                        nictorPort.DataBits = 8;
                        nictorPort.StopBits = StopBits.One;
                        nictorPort.Handshake = Handshake.None;
                        //
                        nictorPort.Open();

                        byte[] message = new byte[4096];
                        int bytesRead = 0;

                        bool toRead = true;

                        while (toRead) // Keep reading until success or timeout.
                        {
                            bytesRead = nictorPort.Read(message, 0, 4096);

                            if (bytesRead > 0)
                            {
                                toRead = false; // Port read success. Stop reading.
                                msg = String.Format("Wireless sensor network is connected. ({0})", portName);
                                nictorPort.ReadTimeout = SerialPort.InfiniteTimeout; ;
                                nictorPort.WriteTimeout = SerialPort.InfiniteTimeout; ;
                            }
                        }
                        // Port sucess. Exit the for loop.
                        if (!toRead) break;
                    }
                    catch (Exception ex) // Read port error for what ever reason.
                    {
                        ResetNictorPort();
                        msg = String.Format("Could not connect to wireless sensor network. Check connections. Retrying...({0}, {1})", ++nictorConnCount, portName);
                    }
                }
            }
  
            return msg;
        }

        private void ResetNictorPort()
        {
            if (nictorPort != null)
            {
                nictorPort.Close();
                nictorPort = null;
            }
        }

        private void ResetPumpPort()
        {
            if (pumpPort != null)
            {
                pumpPort.Close();
                pumpPort = null;
            }
            if (api != null)
            {
                api = null;
            }
        }

        private string EstablistWebConn()
        {
            string msg = null;

            try
            {
                ShuleWebService.Service webService = new ShuleWebService.Service();
                //ServiceReference1.ServiceSoapClient webService = new ServiceReference1.ServiceSoapClient();

                string webServiceResponse = webService.HelloWorld();
                if (webServiceResponse == "Hello World") 
                {
                    webConnected = true;
                    msg = "Internet is connected."; 
                }
                else
                {
                    webConnected = false;
                    msg = "Could not connect to the Internet. Check connections. Retrying...";
                }
            }
            catch (Exception ex)
            {
                webConnected = false;
                msg = "Could not connect to the Internet. Check connections. Retrying...";
            }

            return msg;
        }

        #region User select options.
        
        private void PortDropDownList_SelectedIndexChanged(object sender, EventArgs e)
        {
             //if (PortDropDownList.Items.Count > 0)
             //{
             //   try
             //  {
                    // Create serial port.
                    //pumpPort = new SerialPort(PortDropDownList.SelectedItem as string);
                    //SetBaudRate();
                    //SetDataBits();
                    //SetParity();
                    //SetStopBits();
                    //pumpPort.Open();

                    // Create modbus Api.
                    //api = new AgAnApi(pumpPort);
                    //api.ReadTimeOut = (int)ReadTimeoutNumUpDown.Value;
                    //api.WriteTimeOut = (int)writeTimeoutNumUpDown.Value;
                    //api.Retries = (int)retiresNumUpDown.Value;
                //}
                //catch (Exception ex)
                //{
                    //MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //}
            //}
        }

        private void BaudDropDownList_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetBaudRate();
        }

        private void SetBaudRate()
        {
            if (pumpPort != null)
            {
                switch (BaudDropDownList.SelectedIndex)
                {
                    case 0:
                        pumpPort.BaudRate = 9600;
                        break;
                    case 1:
                        pumpPort.BaudRate = 14400;
                        break;
                    case 2:
                        pumpPort.BaudRate = 19200;
                        break;
                    case 3:
                        pumpPort.BaudRate = 38400;
                        break;
                    case 4:
                        pumpPort.BaudRate = 57600;
                        break;
                    case 5:
                        pumpPort.BaudRate = 115200;
                        break;
                }
            }
        }

        private void DataDropDownList_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetDataBits();
        }

        private void SetDataBits()
        {
            if (pumpPort != null)
            {
                switch (DataDropDownList.SelectedIndex)
                {
                    case 0:
                        pumpPort.DataBits = 5;
                        break;
                    case 1:
                        pumpPort.DataBits = 6;
                        break;
                    case 2:
                        pumpPort.DataBits = 7;
                        break;
                    case 3:
                        pumpPort.DataBits = 8;
                        break;
                }
            }
        }

        private void ParityDropDownList_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetParity();
        }

        private void SetParity()
        {
            if (pumpPort != null)
            {
                switch (ParityDropDownList.SelectedIndex)
                {
                    case 0:
                        pumpPort.Parity = Parity.None;
                        break;
                    case 1:
                        pumpPort.Parity = Parity.Even;
                        break;
                    case 2:
                        pumpPort.Parity = Parity.Odd;
                        break;
                    case 3:
                        pumpPort.Parity = Parity.Mark;
                        break;
                    case 4:
                        pumpPort.Parity = Parity.Space;
                        break;
                }
            }
        }

        private void StopDropDownList_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetStopBits();
        }

        private void SetStopBits()
        {
            if (pumpPort != null)
            {
                switch (StopDropDownList.SelectedIndex)
                {
                    case 0:
                        pumpPort.StopBits = StopBits.None;
                        break;
                    case 1:
                        pumpPort.StopBits = StopBits.One;
                        break;
                    case 2:
                        pumpPort.StopBits = StopBits.OnePointFive;
                        break;
                    case 3:
                        pumpPort.StopBits = StopBits.Two;
                        break;
                }
            }
        }

        private void ReadTimeoutNumUpDown_ValueChanged(object sender, EventArgs e)
        {
            if (api != null)
            {
                api.ReadTimeOut = (int)ReadTimeoutNumUpDown.Value;
            }
        }

        private void writeTimeoutNumUpDown_ValueChanged(object sender, EventArgs e)
        {
            if (api != null)
            {
                api.WriteTimeOut = (int)writeTimeoutNumUpDown.Value;
            }
        }

        private void retiresNumUpDown_ValueChanged(object sender, EventArgs e)
        {
            if (api != null)
            {
                api.Retries = (int)retiresNumUpDown.Value;
            }
        }


        private void connectButton_Click(object sender, EventArgs e)
        {
            //EstablishPumpConn();
            //EstablishNictorConn();
            //EstablistWebConn();

            connectButton.Enabled = false;
            disconnectButton.Enabled = true;

            if (pumpPort != null)
            {
                pumpPort.Close();
                pumpPort.Open();
                pumpConnected = true;
            }

            if (nictorPort != null)
            {
                nictorPort.Close();
                nictorPort.Open();
                nictorConnected = true;
            }

            // This thread check for the web connection.
            if (!webBackgroundWorker.IsBusy)
            {
                webBackgroundWorker.RunWorkerAsync();
            }

            // This thread carry out the while loop of nictor.
            if (!actionBackgroundWorker.IsBusy)
            {
                actionBackgroundWorker.RunWorkerAsync();
            }

            // Getting flowrate and pump status is carried in this thread.
            if (!pumpStatusBackgroundWorker.IsBusy)
            {
                pumpStatusBackgroundWorker.RunWorkerAsync();
            }
        }

        private void disconnectButton_Click(object sender, EventArgs e)
        {
            connectButton.Enabled = true;
            disconnectButton.Enabled = false;

            if (pumpPort != null)
            {
                pumpConnected = false;
                pumpPort.Close();
            }

            if (nictorPort != null)
            {
                nictorConnected = false;
                nictorPort.Close();
            }

            if (webBackgroundWorker.WorkerSupportsCancellation == true)
            {
                webBackgroundWorker.CancelAsync();
            }

            if (actionBackgroundWorker.WorkerSupportsCancellation == true)
            {
                actionBackgroundWorker.CancelAsync();
            }

            if (pumpStatusBackgroundWorker.WorkerSupportsCancellation == true)
            {
                pumpStatusBackgroundWorker.CancelAsync();
            }
        }

        private void helpButton_Click(object sender, EventArgs e)
        {
            string alertMessage = "Some help information.";
            DialogResult confirm = MessageBox.Show(alertMessage, "Help", MessageBoxButtons.OK);
        }

        private void exitButton_Click(object sender, EventArgs e)
        {
            CloseAction();
        }

        private void CloseAction()
        {
            string alertMessage = "If this program is closed, you will disable all communication with the wireless sensor network (WSN). You can restart communication with the WSN by restarting this program. \r\n\nAre you sure you want to close this program?";
            DialogResult confirm = MessageBox.Show(alertMessage, "Alert", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (confirm == DialogResult.Yes)
            {
                try
                {
                    if (pumpPort != null)
                    {
                        if (pumpPort.IsOpen) pumpPort.Close();
                        pumpPort.Dispose();
                    }

                    if (nictorPort != null)
                    {
                        if (nictorPort.IsOpen) nictorPort.Close();
                        nictorPort.Dispose();
                    }
                }
                catch
                {
                }
                Application.Exit();
            }
        }

        private void pumpStatusButton_Click(object sender, EventArgs e)
        {
            if (api != null)
            {
                ModbusTxTbox.AppendText("Pump Status command has been sent.\r\n");

                string error;
                bool b = api.GetPumpStatus(out error);

                if (error == null)
                {
                    ModbusRxTbox.AppendText(String.Format("Pump Status is {0}.\r\n", b));
                }
                else
                {
                    ModbusRxTbox.AppendText(String.Format("Pump Status Error : {0}.\r\n", error));
                }
            }
            else
            {
                MessageBox.Show("Serial port not connected", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void pumpFaultButton_Click(object sender, EventArgs e)
        {
            if (api != null)
            {
                ModbusTxTbox.AppendText("Pump Fault Status command has been sent.\r\n");

                string error;
                bool b = api.GetPumpFaultStatus(out error);

                if (error == null)
                {
                    ModbusRxTbox.AppendText(String.Format("Pump Fault Status is {0}.\r\n", b));
                }
                else
                {
                    ModbusRxTbox.AppendText(String.Format("Pump Fault Status Error : {0}.\r\n", error));
                }
            }
            else
            {
                MessageBox.Show("Serial port not connected", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void lowWaterLevelButton_Click(object sender, EventArgs e)
        {
            if (api != null)
            {
                ModbusTxTbox.AppendText("Get Low Water Level command has been sent.\r\n");

                string error;
                bool b = api.GetLowWaterLevelStatus(out error);

                if (error == null)
                {
                    ModbusRxTbox.AppendText(String.Format("Low Water Level Status is {0}.\r\n", b));
                }
                else
                {
                    ModbusRxTbox.AppendText(String.Format("Low Water Level Status Error : {0}.\r\n", error));
                }
            }
            else
            {
                MessageBox.Show("Serial port not connected", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void openButton_Click(object sender, EventArgs e)
        {
            if (api != null)
            {
                ModbusTxTbox.AppendText("Pump Open command has been sent.\r\n");

                string error;
                api.Open(out error);

                if (error == null)
                {
                    ModbusRxTbox.AppendText("Open successfully.\r\n");
                }
                else
                {
                    ModbusRxTbox.AppendText(String.Format("Pump Open Error : {0}.\r\n", error));
                }
            }
            else
            {
                MessageBox.Show("Serial port not connected", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void closeButton_Click(object sender, EventArgs e)
        {
            if (api != null)
            {
                ModbusTxTbox.AppendText("Pump Close command has been sent.\r\n");

                string error;
                api.Close(out error);

                if (error == null)
                {
                    ModbusRxTbox.AppendText("Close successfully.\r\n");
                }
                else
                {
                    ModbusRxTbox.AppendText(String.Format("Pump Close Error : {0}.\r\n", error));
                }
            }
            else
            {
                MessageBox.Show("Serial port not connected", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void FlowRateutton_Click(object sender, EventArgs e)
        {
            if (api != null)
            {
                ModbusTxTbox.AppendText("Get Flow Rate / Pressure command has been sent.\r\n");

                ushort localSetFlowRate, remoteSetFlowRate, flowrate, pressure;
                string error;

                api.GetFlowRateAndPresure(out localSetFlowRate, out remoteSetFlowRate, out flowrate, out pressure, out error);

                if (error == null)
                {
                    ModbusRxTbox.AppendText(String.Format("Local Set Flow Rate = {0}, Remote Set Flow Rate = {1}, Flow Rate = {2}, Pressure = {3}.\r\n",
                        localSetFlowRate, remoteSetFlowRate, flowrate, pressure));
                }
                else
                {
                    ModbusRxTbox.AppendText(String.Format("Get Flow Rate/Pressure Error : {0}.\r\n", error));
                }
            }
            else
            {
                MessageBox.Show("Serial port not connected", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void clearLogButton_Click(object sender, EventArgs e)
        {
            this.MessageLogPanel.Clear();
        }

        private void clearTxbutton_Click(object sender, EventArgs e)
        {
            this.ModbusTxTbox.Clear();
        }

        private void clearRxbutton_Click(object sender, EventArgs e)
        {
            this.ModbusRxTbox.Clear();
        }

        #endregion

        private bool SystemConnected()
        {
            return pumpPort != null && nictorPort != null && webConnected;
        }

        private void pumpBackgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker bw = (BackgroundWorker)sender;

            string[] msgs = new string[] { null, null };
            int counter = 0;

            try
            {
                while (true && !bw.CancellationPending) // Nerver stop. Keep checking in case system goes down.
                {
                    if (counter < int.MaxValue) counter++; else counter = 0;

                    string msgPump = null;
                    string msgNictor = null;

                    if (pumpPort == null) msgPump = EstablishPumpConn();
                    if (pumpPort != null) pumpConnected = true; else pumpConnected = false;

                    if (nictorPort == null) msgNictor = EstablishNictorConn();
                    if (nictorPort != null) nictorConnected = true; else nictorConnected = false;

                    msgs[0] = msgPump;
                    msgs[1] = msgNictor;
                    bw.ReportProgress(counter, msgs);
                    e.Result = msgs;

                    // Test: Comment out when finish.
                    //if (pumpConnCount >=2 && pumpConnCount <= 5) pumpConnected = true; else pumpConnected = false;
                    // End test.

                    // Wait sleepTime seconds before retry/recheck again.
                    // Thread sleep acts as a dynamic timer with variable interval.
                    // Only use this if pumpConnected and nictorConnected are checked elsewhere.
                    while (/*webConnected &&*/ pumpConnected && nictorConnected) 
                    { 
                        Thread.Sleep(3600000); 
                    }
              
                    // Wait a little bit before retry.
                    Thread.Sleep(5000);
                    
                    // Force reset. This is the only place where the system is checked after 12 hours. 
                    // Otherwise, the status webConnected && pumpConnected && nictorConnected must be
                    // checked somewhere else if the while loop (above) to be terminated when someting wrong.
                    //if (webConnected && pumpConnected && nictorConnected)
                    //{
                    //    Thread.Sleep(43200000);
                    //    // Sleep enough. Force rest all the system in case something wrong has happened.
                    //    ResetPumpPort();
                    //    ResetNictorPort();
                    //    webConnected = false;
                    //    pumpConnected = false;
                    //    nictorConnected = false;
                    //    msgs[0] = "Pump port is reset after 12 hours.";
                    //    msgs[1] = "Nictor port is reset after 12 hours."; ;
                    //    bw.ReportProgress(counter, msgs);
                    //    e.Result = msgs;
                    //}
                    //else
                    //{
                    //    // Wait a little bit before retry.
                    //    Thread.Sleep(5000);
                    //}
                }
            }
            catch(Exception ex)
            {
                msgs[0] = ex.Message;
                msgs[1] = ex.Message;
                bw.ReportProgress(counter, msgs);
                e.Result = msgs;
            }
        }

        private void pumpBackgroundWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            int count = e.ProgressPercentage;
            string[] msg = e.UserState as string[];
            if (msg[0] != null) { pumpPortTextBox.Text = msg[0]; }
            if (msg[1] != null) { nictorPortTextBox.Text = msg[1]; }
        }

        private void pumpBackgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled)
            {
                pumpPortTextBox.Text = "Canceled!\n";
                nictorPortTextBox.Text = "Canceled!\n";
            }
            else if (e.Error != null)
            {
                pumpPortTextBox.Text += ("Error: " + e.Error.Message);
                nictorPortTextBox.Text += ("Error: " + e.Error.Message);
            }
            else if (e.Result != null)
            {
                string[] msg = e.Result as string[];
                if (msg[0] != null) { pumpPortTextBox.Text = msg[0]; }
                if (msg[1] != null) { nictorPortTextBox.Text = msg[1]; }
            }
        }

        private void actionBackgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker bw = (BackgroundWorker)sender;

            while (true && !bw.CancellationPending) // Nerver stop. Keep checking in case system goes down.
            {
                if (nictorConnected)
                {
                    Read(bw);
                }
                
                // Sleep a bit before retry to read again.
                Thread.Sleep(5000);
            }
        }

        int logcounter = 0;

        private void actionBackgroundWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            int count = e.ProgressPercentage;
            string msg = e.UserState as string;

            if (logcounter < 1000 && msg != null)
            {
                logTextbox.AppendText(logcounter + " : " + msg + "\n");
                logcounter++;
            }
            else if (msg != null)
            {
                logTextbox.AppendText(logcounter + " : " + msg + "\n");
                File.AppendAllText("log.txt", logTextbox.Text);
                logTextbox.Clear();
                logcounter = 0;
            }
        }

        private void actionBackgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled)
            {
                logTextbox.AppendText("Canceled!\n");
            }
            else if (e.Error != null)
            {
                logTextbox.AppendText("Error: " + e.Error.Message);
            }
            else if (e.Result != null)
            {
                string msg = e.Result as string;
                logTextbox.AppendText(msg);
            }
        }

        private void webBackgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            // Report the process.
            BackgroundWorker bw = (BackgroundWorker)sender;

            string msg = null;
            int counter = 0;

            try
            {
                while (true && !bw.CancellationPending)
                {
                    if (counter < int.MaxValue) counter++; else counter = 0;

                    msg = EstablistWebConn();
                    e.Result = msg;
                    bw.ReportProgress(counter, msg);

                    if (webConnected)
                    {
                        Thread.Sleep(3600000);
                        webConnected = false;
                    }
                    else
                    {
                        // Wait a default 5 seconds before retry again.
                        Thread.Sleep(5000);
                    }
                }
            }
            catch(Exception ex)
            {
                e.Result = ex.Message;
                bw.ReportProgress(counter, ex.Message);
            }
        }

        private void webBackgroundWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            int count = e.ProgressPercentage;
            string msg = e.UserState as string;
            if (msg != null) webConnTextBox.Text = msg;
        }

        private void webBackgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled)
            {
                webConnTextBox.Text = "Canceled!\n";
            }
            else if (e.Error != null)
            {
                webConnTextBox.Text += ("Error: " + e.Error.Message);
            }
            else if (e.Result != null)
            {
                string msg = e.Result as string;
                webConnTextBox.Text = msg;
            }
        }

       
        private void clearLogBtn_Click(object sender, EventArgs e)
        {
            logTextbox.Clear();
        }

        private void Read(BackgroundWorker bw)
        {
            //string _coordIeeeAddress = "AA-BB-CC-DD-11-22-33-00";
            string _coordIeeeAddress = "00-00-00-00-00-00-00-01";

            ByteReader rdr = new ByteReader();
            ASCIIEncoding encoder = new ASCIIEncoding();
            byte[] message = new byte[4096];
            int bytesRead = 0;

            const byte EVENT_CMD_TYPE = 0x02;

            const byte NODE_NO_MORE_DATA_CMD_ID = 0xA1;
            const byte NODE_INFO_CMD_ID = 0xA0;
            const byte COORD_INFO_CMD_ID = 0xA2;
            const byte ECHO_DATA_CMD_ID = 0xFE;
            const byte FLOW_DATA_CMD_ID = 0xFD;

            int NID_START_IDX = 0;  // Network ID
            int NID_LENGTH = 4;
            int CMD_TYPE_IDX = 4;   // Command Type
            int CMD_ID_IDX = 5;     // Command ID
            int ERR_CODE_IDX = 6;     // Error code
            int SID_START_IDX = 7;  // Sensor ID
            int SID_LENGTH = 8;
            int BATT_IDX = 15;      // Data (battery)
            int TEMP_IDX = 16;      // Data (temperature)
            int LQI_IDX = 17;
            int NETWORK_STATE_IDX = 18; // Coordinator only
            int NETWORK_INTERVAL_IN_MIN = 19; // Coordinator only
            int NETWORK_WAKE_INTERVAL_IN_SEC = 21;  // Coordinator only
            int ECHO_IDX = 18;
            int FLOW_IDX = 18;
            int ECHO_LENGTH = 4;
            int FLOW_LENGTH = 8;

            int numberOfNodes = 0;

            while (nictorConnected )
            {
                try
                {
                    bytesRead = nictorPort.Read(message, 0, 4096);
                    for (int k = 0; k < bytesRead; k++)
                    {
                        int rxByte = message[k];

                        if (rdr.processRxBytes(rxByte))
                        {
                            if (rdr.cmd_numPayloadRxByte < 254)
                            {
                                rdr.cmd_pRxPayload[0] = (byte)(rdr.cmd_numPayloadRxByte);

                                // Parse message (TBD - Based on rules)
                                try
                                {
                                    // NID
                                    byte[] tempMessage1 = new byte[4];
                                    for (int idx = 0, n = NID_LENGTH - 1 + NID_START_IDX; n >= NID_START_IDX; idx++, n--)
                                    {
                                        tempMessage1[idx] = rdr.cmd_pRxPayload[1 + n];
                                    }
                                    string nidString = BitConverter.ToString(tempMessage1);

                                    // TODO: Check NID is registered

                                    // COMMAND TYPE
                                    string cmdTypeString = BitConverter.ToString(rdr.cmd_pRxPayload, 1 + CMD_TYPE_IDX, 1);

                                    // COMMAND ID
                                    string cmdIdString = BitConverter.ToString(rdr.cmd_pRxPayload, 1 + CMD_ID_IDX, 1);
                                    if (rdr.cmd_pRxPayload[1 + CMD_ID_IDX] == NODE_NO_MORE_DATA_CMD_ID)
                                    {
                                        // No more data to come
                                        // Display 
                                        // Calculate the local time and UTC offset.
                                        TimeZone localZone = TimeZone.CurrentTimeZone;
                                        DateTime baseUTC = System.DateTime.UtcNow;
                                        DateTime localTime = localZone.ToLocalTime(baseUTC);
                                        TimeSpan localOffset = localZone.GetUtcOffset(localTime);
                                        string timestampString = baseUTC.ToString("yyyy-MM-ddTHH:mm:ssZ");
                                        bw.ReportProgress(0,"End of messages from " + _coordIeeeAddress + " at " + timestampString);
                                        bw.ReportProgress(0,"    cmd type:           " + cmdTypeString);
                                        bw.ReportProgress(0,"    cmd id:             " + cmdIdString);
                                        bw.ReportProgress(0,"    Number of nodes:    " + numberOfNodes);
                                        numberOfNodes = 0;

                                       
                                        if (webConnected)
                                        {
                                            // Get any downstream messages
                                            bw.ReportProgress(0, "Downloading message from server...");

                                            ShuleWebService.Service nictorWebService = new ShuleWebService.Service();
                                            //ServiceReference1.ServiceSoapClient nictorWebService = new ServiceReference1.ServiceSoapClient();

                                            //string command = nictorWebService.GetNextCommand(_coordIeeeAddress);
                                            string command = nictorWebService.GetNextCommandAlt(_coordIeeeAddress.Replace("-",""));
                                            Byte[] msg = Enumerable.Range(0, command.Length)
                                                            .Where(x => x % 2 == 0)
                                                            .Select(x => Convert.ToByte(command.Substring(x, 2), 16))
                                                            .ToArray();

                                            bw.ReportProgress(0, "    Message: " + command);
                                            rdr.CMD_buildSendMessage((ushort)msg.Length, msg, nictorPort);


                                            // get pump commands
                                            
                                            bw.ReportProgress(0, "Get pump commands from server...");
                                            nictorWebService = new ShuleWebService.Service();
                                            string pumpCommand = nictorWebService.GetNextPumpCommand(_coordIeeeAddress.Replace("-", ""));
                                            
                                            string anError = "ok";
                                            if (pumpConnected) {
                                                bw.ReportProgress(0, "   Pump command is " + pumpCommand + " and PUMP CONNECTED");
                                                if (pumpCommand == "ON") { api.Open(out anError); }
                                                if (pumpCommand == "OFF") { api.Close(out anError); }
                                                bw.ReportProgress(0, "   Pump command result: " + anError );
                                            }
                                            else
                                            {
                                                bw.ReportProgress(0, "   Pump command is " + pumpCommand + " but PUMP NOT CONNECTED");
                                            }
                                            // Get flow rate from the pump in another thread.
                                            // Get state from another thread.
                                            //nictorWebService.SavePumpFlowRate(pumpFlowrate);
                                            //nictorWebService.SavePumpPressure(pumpPressure);
                                            //nictorWebService.SavePumpLocalSetFlowRate(pumpLocalSetFlowRate);
                                            //nictorWebService.SavePumpRemoteSetFlowRate(pumpRemoteSetFlowRate);
                                            //nictorWebService.SavePumpState(allPumpStatus);

                                        }
                                    }
                                    else
                                    {
                                        // More data to come
                                        numberOfNodes++;

                                        // ERROR CODE
                                        string errorCodeString = BitConverter.ToString(rdr.cmd_pRxPayload, 1 + ERR_CODE_IDX, 1);

                                        // SID
                                        byte[] tempMessage2 = new byte[8];
                                        for (int idx = 0, n = SID_LENGTH - 1 + SID_START_IDX; n >= SID_START_IDX; idx++, n--)
                                        {
                                            tempMessage2[idx] = rdr.cmd_pRxPayload[1 + n];
                                        }
                                        string sidString = BitConverter.ToString(tempMessage2).Replace("-", string.Empty);

                                        // DATA - BATT
                                        byte tempByte = rdr.cmd_pRxPayload[1 + BATT_IDX];
                                        double battValue = (System.Convert.ToDouble(tempByte) * 4) / 100;

                                        // DATA - TEMPERATURE
                                        tempByte = rdr.cmd_pRxPayload[1 + TEMP_IDX];
                                        double tempValue = System.Convert.ToDouble(tempByte);

                                        // DATA - LINK QUALITY
                                        tempByte = rdr.cmd_pRxPayload[1 + LQI_IDX];
                                        double lqiValue = System.Convert.ToDouble(tempByte);

                                        // Extra info from the Coordinator
                                        int networkState = 0;
                                        int networkIntervalInMinutes = 0;
                                        int networkWakeIntervalInSeconds = 0;
                                        if ((rdr.cmd_pRxPayload[1 + CMD_TYPE_IDX] == EVENT_CMD_TYPE) && (rdr.cmd_pRxPayload[1 + CMD_ID_IDX] == COORD_INFO_CMD_ID))
                                        {
                                            tempByte = rdr.cmd_pRxPayload[1 + NETWORK_STATE_IDX];
                                            networkState = System.Convert.ToInt32(tempByte);
                                            networkIntervalInMinutes = (int)((rdr.cmd_pRxPayload[1 + NETWORK_INTERVAL_IN_MIN] & 0x00FF) |
                                                (rdr.cmd_pRxPayload[1 + NETWORK_INTERVAL_IN_MIN + 1] & 0xFF00));
                                            networkWakeIntervalInSeconds = (int)((rdr.cmd_pRxPayload[1 + NETWORK_WAKE_INTERVAL_IN_SEC] & 0x00FF) |
                                                (rdr.cmd_pRxPayload[1 + NETWORK_WAKE_INTERVAL_IN_SEC + 1] & 0xFF00));
                                        }

                                        // Sensor data
                                        string sensorDataString = null;
                                        if ((rdr.cmd_pRxPayload[1 + CMD_TYPE_IDX] == EVENT_CMD_TYPE) && (rdr.cmd_pRxPayload[1 + CMD_ID_IDX] == ECHO_DATA_CMD_ID))
                                        {
                                            byte[] tempMessage3 = new byte[ECHO_LENGTH];
                                            for (int idx = 0, n = ECHO_IDX; n < ECHO_IDX + ECHO_LENGTH; idx++, n++)
                                            {
                                                tempMessage3[idx] = rdr.cmd_pRxPayload[1 + n];
                                            }
                                            sensorDataString = BitConverter.ToString(tempMessage3);

                                        }
                                        else if ((rdr.cmd_pRxPayload[1 + CMD_TYPE_IDX] == EVENT_CMD_TYPE) && (rdr.cmd_pRxPayload[1 + CMD_ID_IDX] == FLOW_DATA_CMD_ID))
                                        {
                                            byte[] tempMessage3 = new byte[FLOW_LENGTH];
                                            for (int idx = 0, n = FLOW_IDX; n < FLOW_IDX + FLOW_LENGTH; idx++, n++)
                                            {
                                                tempMessage3[idx] = rdr.cmd_pRxPayload[1 + n];
                                            }
                                            sensorDataString = BitConverter.ToString(tempMessage3);
                                        }

                                        // Display 
                                        bw.ReportProgress(0,"Valid message received from  " + sidString + " at " + System.DateTime.UtcNow.ToShortTimeString());
                                        bw.ReportProgress(0,"    cmd type:   " + cmdTypeString);
                                        bw.ReportProgress(0,"    cmd id:     " + cmdIdString);
                                        bw.ReportProgress(0,"    error code: " + errorCodeString);
                                        bw.ReportProgress(0,"    batt:       " + battValue.ToString());
                                        bw.ReportProgress(0,"    temp:       " + tempValue.ToString());
                                        bw.ReportProgress(0,"    lqi:        " + lqiValue.ToString());
                                        bw.ReportProgress(0, "   nwk state:       " + networkState.ToString());
                                        bw.ReportProgress(0, "   nwk int:        " + networkIntervalInMinutes.ToString());
                                        bw.ReportProgress(0, "   nwk wake int: " + networkWakeIntervalInSeconds.ToString());
                                        bw.ReportProgress(0,"    sensor dat: " + sensorDataString);


                                        // Save to server
                                        bw.ReportProgress(0,"Uploading to server...");
                                        UploadNodeData uploader = null;
                                        string sidUpper = sidString.ToUpper();
                                        string coordUpper = _coordIeeeAddress.ToUpper().Replace("-", "");
                                        if (sidUpper == coordUpper)
                                        {
                                            // Is coordinator
                                            uploader = new UploadNodeData(sidString, battValue, tempValue, true, networkState, networkIntervalInMinutes, networkWakeIntervalInSeconds, null);
                                        }
                                        else
                                        {
                                            uploader = new UploadNodeData(sidString, battValue, tempValue, false, 0, 0, 0, sensorDataString);
                                        }
                                        Thread t = new Thread(uploader.ThreadProc);
                                        t.Start();
                                    }
                                }
                                catch (Exception ex)
                                {
                                    bw.ReportProgress(0,ex.Message);
                                }
                            }
                            rdr.CMD_initByteProcessing();
                        }
                    }
                }
                catch(IOException ex)
                {
                    bw.ReportProgress(0, "Serial port error.");
                    nictorConnected = false;
                    ResetNictorPort();
                }
                catch(Exception ex)
                {
                    bw.ReportProgress(0, "a socket error has occured");
                    break;
                }

            } // end of while
        }

        private void nictorComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (nictorComboBox.Items.Count > 0)
            {
                try
                {
                    nictorPort = new SerialPort(nictorComboBox.SelectedItem as string);
                    nictorPort.ReadTimeout = SerialPort.InfiniteTimeout; ;
                    nictorPort.WriteTimeout = SerialPort.InfiniteTimeout; ;
                    nictorPort.BaudRate = 38400;
                    nictorPort.Parity = Parity.None;
                    nictorPort.DataBits = 8;
                    nictorPort.StopBits = StopBits.One;
                    nictorPort.Handshake = Handshake.None;
                }
                catch (Exception ex) // Read port error for what ever reason.
                {
                    ResetNictorPort();
                    MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void pumpPortComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (pumpPortComboBox.Items.Count > 0)
            {
                try
                {
                    // Create serial port.
                    pumpPort = new SerialPort(pumpPortComboBox.SelectedItem as string);
                    SetBaudRate();
                    SetDataBits();
                    SetParity();
                    SetStopBits();

                    // Create modbus Api.
                    api = new AgAnApi(pumpPort);
                    api.ReadTimeOut = (int)ReadTimeoutNumUpDown.Value;
                    api.WriteTimeOut = (int)writeTimeoutNumUpDown.Value;
                    api.Retries = (int)retiresNumUpDown.Value;
                }
                catch (Exception ex)
                {
                    ResetPumpPort();
                    MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void pumpStatusBackgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            // Report the process.
            BackgroundWorker bw = (BackgroundWorker)sender;

            int counter = 0;

            while (true && !bw.CancellationPending)
            {
                try
                {
                    if (pumpConnected)
                    {
                        if (counter < int.MaxValue) counter++; else counter = 0;

                        StringBuilder sb = new StringBuilder();

                        string error1 = null, error2 = null, error3 = null, error4 = null;

                        bool pumpStatus = api.GetPumpStatus(out error1);
                        if (error1 == null) sb.Append(String.Format("PumpStatus = {0};", pumpStatus)); else sb.Append("PumpStatus = error;");

                        bool pumpFaultStatus = api.GetPumpFaultStatus(out error2);
                        if (error2 == null) sb.Append(String.Format("PumpFaultStatus = {0};", pumpFaultStatus)); else sb.Append("PumpFaultStatus = error;");

                        bool lowWaterLevelStatus = api.GetLowWaterLevelStatus(out error3);
                        if (error3 == null) sb.Append(String.Format("LowWaterLevelStatus = {0};", lowWaterLevelStatus)); else sb.Append("LowWaterLevelStatus = error;");

                        allPumpStatus = sb.ToString();
                        sb.AppendLine();

                        ushort localSetFlowRate = 0, remoteSetFlowRate = 0, flowrate = 0, pressure = 0;
                        api.GetFlowRateAndPresure(out localSetFlowRate, out remoteSetFlowRate, out flowrate, out pressure, out error4);
                        if (error4 == null)
                        {
                            pumpFlowrate = flowrate;
                            pumpLocalSetFlowRate = localSetFlowRate;
                            pumpRemoteSetFlowRate = remoteSetFlowRate;
                            pumpPressure = pressure;
                        }
                        sb.Append(String.Format("Flowrate = {0}; LocalSetFlowRate = {1}; RemoteSetFlowRate = {2}; Pressure = {3};", pumpFlowrate, pumpLocalSetFlowRate, pumpRemoteSetFlowRate, pumpPressure));
                        sb.AppendLine();

                        string message = sb.ToString();
                        bw.ReportProgress(counter, message);

                        // All four transmission failed. Highly likely something wrong with the port.
                        if(error1 != null && error2 != null && error3 != null && error4 != null)
                        {
                            pumpConnected = false;
                            ResetPumpPort();
                            bw.ReportProgress(counter, "Get pump information failed.");
                        }
                    }
                    else
                    {
                        bw.ReportProgress(counter, "No pump information available.");
                    }
                }
                catch (Exception ex)
                {
                    pumpConnected = false;
                    ResetPumpPort();
                    bw.ReportProgress(counter, "No pump information available." + ex.Message);
                }

                // Checking status and flowrate every 5 mins.
                Thread.Sleep(300000);
            }
        }

        int pumpInfoLogCounter = 0;

        private void pumpStatusBackgroundWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            int count = e.ProgressPercentage;
            string msg = e.UserState as string;

            if (pumpInfoLogCounter < 1000 && msg != null)
            {
                pumpInfoTextBox.AppendText(pumpInfoLogCounter + " : " + msg + "\n");
                pumpInfoLogCounter++;
            }
            else if (msg != null)
            {
                pumpInfoTextBox.AppendText(pumpInfoLogCounter + " : " + msg + "\n");
                System.IO.File.AppendAllText("PumpInfoLog.txt", pumpInfoTextBox.Text);
                pumpInfoTextBox.Clear();
                pumpInfoLogCounter = 0;
            }
        }

        private void pumpStatusBackgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled)
            {
                pumpInfoTextBox.AppendText("Canceled!\n");
            }
            else if (e.Error != null)
            {
                pumpInfoTextBox.AppendText("Error: " + e.Error.Message);
            }
            else if (e.Result != null)
            {
                string msg = e.Result as string;
                pumpInfoTextBox.AppendText(msg);
            }
        }


       

    

    }
}
