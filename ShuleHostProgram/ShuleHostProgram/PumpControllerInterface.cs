﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO.Ports;
using AgAnalyticApi;
using System.ComponentModel;

namespace ShuleHostProgram
{
    public class PumpControllerInterface
    {
        private string command;

        public SerialPort pumpPort = null;
        public AgAnApi api = null;
        public string msg = null;
        public BackgroundWorker bw = null; // Do NOT update the progress report inside another thread.

        public PumpControllerInterface(string _command, AgAnApi api, BackgroundWorker bw)
        {
            command = _command;
            if (command == null) 
                bw.ReportProgress(0, "Command :" + "null");
            else 
                bw.ReportProgress(0, "Command :" + command);
        }

        // How to do progress update? Do not use bw if ThreadProc() is called in another thread.
        public void ThreadProc()
        {
            if (command != null)
            {
                try
                {
                    string error = null;

                    switch (command)
                    {
                        case "ON":
                            api.Open(out error);
                            if (error == null) // Port is ok for use.
                            {
                                msg = "Open pump sucessfully.";
                            }
                            else
                            {
                                msg = "Failed to open pump.";
                            }
                            break;

                        case "OFF":
                            api.Close(out error);
                            if (error == null) // Port is ok for use.
                            {
                                msg = "Close pump sucessfully.";
                            }
                            else
                            {
                                msg = "Failed to close pump.";
                            }
                            break;
                    }
                }
                catch (System.IO.IOException ex)
                {
                    // Error due to port. Reset port and api associated with it.
                    if (pumpPort != null) { pumpPort.Close(); pumpPort = null; }
                    if (api != null) api = null;

                    msg = "Pump open/close failed. " + ex.Message;
                    System.Diagnostics.Debug.WriteLine(ex.Message);
                }
                catch (Exception ex)
                {
                    msg = "Pump open/close failed. " + ex.Message;
                    System.Diagnostics.Debug.WriteLine(ex.Message);
                }
            }
        }
    }
}
