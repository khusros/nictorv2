﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ShuleHostProgram.ShuleWebService;

namespace ShuleHostProgram
{
    public class UploadNodeData
    {
        private string did;
        private double batt;
        private double temp;
        private double lqi;
        private bool isCoordinator;
        private string sensorData;
        private int networkState;
        private int networkIntervalInMin;
        private int networkWakeIntervalInSec;


       
        public UploadNodeData(string _did, double _batt, double _temp, bool _isCoordinator, int _networkState, int _networkIntervalInMin, int _networkWakeIntervalInSec, string _sensorData)
        {
            did = _did;
            batt = _batt;
            temp = _temp;
            isCoordinator = _isCoordinator;
            networkState = _networkState;
            networkIntervalInMin = _networkIntervalInMin;
            networkWakeIntervalInSec = _networkWakeIntervalInSec;
            sensorData = _sensorData;
        }

        public void ThreadProc()
        {
            try
            {
                ShuleWebService.Service webService = new Service();
                if (!isCoordinator)
                {
                    webService.WriteNodeStatus(did, batt, temp);
                    webService.InsertMeasurement(did, sensorData);
                }
                else
                {
                    webService.WriteNetworkStatus(did, batt, temp, networkState, networkIntervalInMin, networkWakeIntervalInSec);
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
            }
        }
    }
}
