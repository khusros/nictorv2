﻿namespace ShuleHostProgram
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.MessageLogPanel = new System.Windows.Forms.TextBox();
            this.button3 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button12 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.ModbusRxTbox = new System.Windows.Forms.TextBox();
            this.ModbusTxTbox = new System.Windows.Forms.TextBox();
            this.button13 = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.StopDropDownList = new System.Windows.Forms.ComboBox();
            this.ParityDropDownList = new System.Windows.Forms.ComboBox();
            this.DataDropDownList = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.BaudDropDownList = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.PortDropDownList = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.ReadTimeoutNumUpDown = new System.Windows.Forms.NumericUpDown();
            this.writeTimeoutNumUpDown = new System.Windows.Forms.NumericUpDown();
            this.retiresNumUpDown = new System.Windows.Forms.NumericUpDown();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ReadTimeoutNumUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.writeTimeoutNumUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.retiresNumUpDown)).BeginInit();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(12, 28);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 41);
            this.button1.TabIndex = 0;
            this.button1.Text = "Connect";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(174, 28);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 41);
            this.button2.TabIndex = 2;
            this.button2.Text = "Exit";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // MessageLogPanel
            // 
            this.MessageLogPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.MessageLogPanel.Location = new System.Drawing.Point(13, 150);
            this.MessageLogPanel.Multiline = true;
            this.MessageLogPanel.Name = "MessageLogPanel";
            this.MessageLogPanel.ReadOnly = true;
            this.MessageLogPanel.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.MessageLogPanel.Size = new System.Drawing.Size(845, 94);
            this.MessageLogPanel.TabIndex = 3;
            // 
            // button3
            // 
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.Location = new System.Drawing.Point(783, 120);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 4;
            this.button3.Text = "Clear log";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 126);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(127, 17);
            this.label1.TabIndex = 5;
            this.label1.Text = "Message log panel";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.button12);
            this.groupBox1.Controls.Add(this.button10);
            this.groupBox1.Controls.Add(this.button11);
            this.groupBox1.Controls.Add(this.button8);
            this.groupBox1.Controls.Add(this.button7);
            this.groupBox1.Controls.Add(this.button6);
            this.groupBox1.Controls.Add(this.button5);
            this.groupBox1.Controls.Add(this.button4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.ModbusRxTbox);
            this.groupBox1.Controls.Add(this.ModbusTxTbox);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(15, 250);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(843, 244);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Pump control panel";
            // 
            // button12
            // 
            this.button12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button12.Location = new System.Drawing.Point(101, 41);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(75, 23);
            this.button12.TabIndex = 18;
            this.button12.Text = "Fault status";
            this.button12.UseVisualStyleBackColor = true;
            this.button12.Click += new System.EventHandler(this.button12_Click);
            // 
            // button10
            // 
            this.button10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button10.Location = new System.Drawing.Point(20, 99);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(156, 23);
            this.button10.TabIndex = 16;
            this.button10.Text = "Flow Rate / Presure";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // button11
            // 
            this.button11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button11.Location = new System.Drawing.Point(182, 41);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(101, 23);
            this.button11.TabIndex = 15;
            this.button11.Text = "Low Water Level";
            this.button11.UseVisualStyleBackColor = true;
            this.button11.Click += new System.EventHandler(this.button11_Click);
            // 
            // button8
            // 
            this.button8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button8.Location = new System.Drawing.Point(101, 70);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(75, 23);
            this.button8.TabIndex = 14;
            this.button8.Text = "Close";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // button7
            // 
            this.button7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button7.Location = new System.Drawing.Point(20, 70);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(75, 23);
            this.button7.TabIndex = 13;
            this.button7.Text = "Open";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button6
            // 
            this.button6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button6.Location = new System.Drawing.Point(20, 41);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(75, 23);
            this.button6.TabIndex = 12;
            this.button6.Text = "Pump status";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button5
            // 
            this.button5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button5.Location = new System.Drawing.Point(752, 134);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(75, 23);
            this.button5.TabIndex = 11;
            this.button5.Text = "Clear";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button4
            // 
            this.button4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.Location = new System.Drawing.Point(751, 24);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 10;
            this.button4.Text = "Clear";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(404, 137);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(81, 17);
            this.label3.TabIndex = 9;
            this.label3.Text = "Modbus RX";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(404, 27);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(80, 17);
            this.label2.TabIndex = 8;
            this.label2.Text = "Modbus TX";
            // 
            // ModbusRxTbox
            // 
            this.ModbusRxTbox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.ModbusRxTbox.Location = new System.Drawing.Point(407, 163);
            this.ModbusRxTbox.Multiline = true;
            this.ModbusRxTbox.Name = "ModbusRxTbox";
            this.ModbusRxTbox.ReadOnly = true;
            this.ModbusRxTbox.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.ModbusRxTbox.Size = new System.Drawing.Size(420, 66);
            this.ModbusRxTbox.TabIndex = 7;
            // 
            // ModbusTxTbox
            // 
            this.ModbusTxTbox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.ModbusTxTbox.Location = new System.Drawing.Point(406, 53);
            this.ModbusTxTbox.Multiline = true;
            this.ModbusTxTbox.Name = "ModbusTxTbox";
            this.ModbusTxTbox.ReadOnly = true;
            this.ModbusTxTbox.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.ModbusTxTbox.Size = new System.Drawing.Size(420, 66);
            this.ModbusTxTbox.TabIndex = 0;
            // 
            // button13
            // 
            this.button13.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button13.Location = new System.Drawing.Point(93, 28);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(75, 41);
            this.button13.TabIndex = 7;
            this.button13.Text = "Help";
            this.button13.UseVisualStyleBackColor = true;
            this.button13.Click += new System.EventHandler(this.button13_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.StopDropDownList);
            this.groupBox2.Controls.Add(this.ParityDropDownList);
            this.groupBox2.Controls.Add(this.DataDropDownList);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.BaudDropDownList);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.PortDropDownList);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Location = new System.Drawing.Point(297, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(544, 57);
            this.groupBox2.TabIndex = 8;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Serial port configuration";
            // 
            // StopDropDownList
            // 
            this.StopDropDownList.AutoCompleteCustomSource.AddRange(new string[] {
            "1",
            "2"});
            this.StopDropDownList.FormattingEnabled = true;
            this.StopDropDownList.Items.AddRange(new object[] {
            "0",
            "1",
            "1.5",
            "2"});
            this.StopDropDownList.Location = new System.Drawing.Point(482, 27);
            this.StopDropDownList.Name = "StopDropDownList";
            this.StopDropDownList.Size = new System.Drawing.Size(48, 21);
            this.StopDropDownList.TabIndex = 14;
            this.StopDropDownList.SelectedIndexChanged += new System.EventHandler(this.StopDropDownList_SelectedIndexChanged);
            // 
            // ParityDropDownList
            // 
            this.ParityDropDownList.FormattingEnabled = true;
            this.ParityDropDownList.Items.AddRange(new object[] {
            "None",
            "Even",
            "Odd",
            "Mark",
            "Space"});
            this.ParityDropDownList.Location = new System.Drawing.Point(379, 27);
            this.ParityDropDownList.Name = "ParityDropDownList";
            this.ParityDropDownList.Size = new System.Drawing.Size(61, 21);
            this.ParityDropDownList.TabIndex = 13;
            this.ParityDropDownList.SelectedIndexChanged += new System.EventHandler(this.ParityDropDownList_SelectedIndexChanged);
            // 
            // DataDropDownList
            // 
            this.DataDropDownList.FormattingEnabled = true;
            this.DataDropDownList.Items.AddRange(new object[] {
            "5",
            "6",
            "7",
            "8"});
            this.DataDropDownList.Location = new System.Drawing.Point(278, 27);
            this.DataDropDownList.Name = "DataDropDownList";
            this.DataDropDownList.Size = new System.Drawing.Size(56, 21);
            this.DataDropDownList.TabIndex = 12;
            this.DataDropDownList.SelectedIndexChanged += new System.EventHandler(this.DataDropDownList_SelectedIndexChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(340, 30);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(33, 13);
            this.label8.TabIndex = 11;
            this.label8.Text = "Parity";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(242, 30);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(30, 13);
            this.label7.TabIndex = 10;
            this.label7.Text = "Data";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(447, 30);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(29, 13);
            this.label6.TabIndex = 9;
            this.label6.Text = "Stop";
            // 
            // BaudDropDownList
            // 
            this.BaudDropDownList.FormattingEnabled = true;
            this.BaudDropDownList.Items.AddRange(new object[] {
            "9600",
            "14400",
            "19200",
            "38400",
            "57600",
            "115200"});
            this.BaudDropDownList.Location = new System.Drawing.Point(159, 27);
            this.BaudDropDownList.Name = "BaudDropDownList";
            this.BaudDropDownList.Size = new System.Drawing.Size(68, 21);
            this.BaudDropDownList.TabIndex = 9;
            this.BaudDropDownList.SelectedIndexChanged += new System.EventHandler(this.BaudDropDownList_SelectedIndexChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(121, 30);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(32, 13);
            this.label5.TabIndex = 2;
            this.label5.Text = "Baud";
            // 
            // PortDropDownList
            // 
            this.PortDropDownList.FormattingEnabled = true;
            this.PortDropDownList.Location = new System.Drawing.Point(40, 27);
            this.PortDropDownList.Name = "PortDropDownList";
            this.PortDropDownList.Size = new System.Drawing.Size(73, 21);
            this.PortDropDownList.TabIndex = 1;
            this.PortDropDownList.SelectedIndexChanged += new System.EventHandler(this.PortDropDownList_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(8, 30);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(26, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Port";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.retiresNumUpDown);
            this.groupBox3.Controls.Add(this.writeTimeoutNumUpDown);
            this.groupBox3.Controls.Add(this.ReadTimeoutNumUpDown);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Location = new System.Drawing.Point(297, 75);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(476, 68);
            this.groupBox3.TabIndex = 9;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Modbus configuration";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(9, 27);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(74, 13);
            this.label9.TabIndex = 0;
            this.label9.Text = "Read Timeout";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(175, 27);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(73, 13);
            this.label10.TabIndex = 1;
            this.label10.Text = "Write Timeout";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(344, 27);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(40, 13);
            this.label11.TabIndex = 2;
            this.label11.Text = "Retries";
            // 
            // ReadTimeoutNumUpDown
            // 
            this.ReadTimeoutNumUpDown.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.ReadTimeoutNumUpDown.Location = new System.Drawing.Point(89, 25);
            this.ReadTimeoutNumUpDown.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.ReadTimeoutNumUpDown.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.ReadTimeoutNumUpDown.Name = "ReadTimeoutNumUpDown";
            this.ReadTimeoutNumUpDown.ReadOnly = true;
            this.ReadTimeoutNumUpDown.Size = new System.Drawing.Size(80, 20);
            this.ReadTimeoutNumUpDown.TabIndex = 3;
            this.ReadTimeoutNumUpDown.Value = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.ReadTimeoutNumUpDown.ValueChanged += new System.EventHandler(this.ReadTimeoutNumUpDown_ValueChanged);
            // 
            // writeTimeoutNumUpDown
            // 
            this.writeTimeoutNumUpDown.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.writeTimeoutNumUpDown.Location = new System.Drawing.Point(254, 25);
            this.writeTimeoutNumUpDown.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.writeTimeoutNumUpDown.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.writeTimeoutNumUpDown.Name = "writeTimeoutNumUpDown";
            this.writeTimeoutNumUpDown.ReadOnly = true;
            this.writeTimeoutNumUpDown.Size = new System.Drawing.Size(80, 20);
            this.writeTimeoutNumUpDown.TabIndex = 4;
            this.writeTimeoutNumUpDown.Value = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.writeTimeoutNumUpDown.ValueChanged += new System.EventHandler(this.writeTimeoutNumUpDown_ValueChanged);
            // 
            // retiresNumUpDown
            // 
            this.retiresNumUpDown.Location = new System.Drawing.Point(390, 25);
            this.retiresNumUpDown.Name = "retiresNumUpDown";
            this.retiresNumUpDown.ReadOnly = true;
            this.retiresNumUpDown.Size = new System.Drawing.Size(80, 20);
            this.retiresNumUpDown.TabIndex = 5;
            this.retiresNumUpDown.ValueChanged += new System.EventHandler(this.retiresNumUpDown_ValueChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(877, 506);
            this.ControlBox = false;
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.button13);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.MessageLogPanel);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Name = "Form1";
            this.Text = "Shule host controller";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ReadTimeoutNumUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.writeTimeoutNumUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.retiresNumUpDown)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox MessageLogPanel;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox ModbusRxTbox;
        private System.Windows.Forms.TextBox ModbusTxTbox;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox BaudDropDownList;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox PortDropDownList;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox StopDropDownList;
        private System.Windows.Forms.ComboBox ParityDropDownList;
        private System.Windows.Forms.ComboBox DataDropDownList;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.NumericUpDown retiresNumUpDown;
        private System.Windows.Forms.NumericUpDown writeTimeoutNumUpDown;
        private System.Windows.Forms.NumericUpDown ReadTimeoutNumUpDown;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
    }
}