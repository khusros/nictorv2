﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.IO.Ports;
using AgAnalyticApi;

namespace ShuleHostProgram
{
    public partial class Form1 : Form
    {
        private string NictorCoordinatorComPort = null;
        private string ShulePumpControllerComPort = null;
        private string STATE = "IDLE"; // "CONNECTING", "CONNECTED"
        private AgAnApi api = null;
        SerialPort port = null;

        public Form1()
        {
            InitializeComponent();
            Init();
        }

        private void Init()
        {
            NictorCoordinatorComPort = null;
            ShulePumpControllerComPort = null;
            STATE = "IDLE";

            string[] allSerialPortNames = SerialPort.GetPortNames();

            foreach (string portName in allSerialPortNames)
            {
                PortDropDownList.Items.Add(portName);
            }
            
            log4net.Config.XmlConfigurator.Configure();
            
            BaudDropDownList.SelectedIndex = 0;
            DataDropDownList.SelectedIndex = 3;
            ParityDropDownList.SelectedIndex = 0;
            StopDropDownList.SelectedIndex = 1;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string alertMessage = "If this program is closed, you will disable all communication with the wireless sensor network (WSN). You can restart communication with the WSN by restarting this program. \r\n\nAre you sure you want to close this program?";
            DialogResult confirm = MessageBox.Show(alertMessage, "Alert", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (confirm == DialogResult.Yes)
            {
                if (port != null)
                {
                    if (port.IsOpen) port.Close();
                    port.Dispose();
                }

                Application.Exit();
            }
        }

        private void EstablishComPortLinks()
        {
            if (this.STATE == "IDLE")
            {
                this.MessageLogPanel.Text = "Connected";//"Connecting...";
                this.STATE = "CONNECTING";
            }
            else if (this.STATE == "CONNECTING")
            {
                MessageBox.Show("This program is current attempting to connect, please wait.");
            }
            else if (this.STATE == "CONNECTED")
            {
                MessageBox.Show("This program is already connected.");
            }
            else
            {
                MessageBox.Show("The program has experienced a critical error. Please exit and restart.");
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.MessageLogPanel.Clear();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.EstablishComPortLinks();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.ModbusTxTbox.Clear();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            this.ModbusRxTbox.Clear();
        }

        private void button13_Click(object sender, EventArgs e)
        {
            string alertMessage = "Some help information.";
            DialogResult confirm = MessageBox.Show(alertMessage, "Help", MessageBoxButtons.OK);
        }

        private void button6_Click(object sender, EventArgs e)
        {
            if (api != null)
            {
                ModbusTxTbox.AppendText("Pump Status command has been sent.\r\n");

                string error;
                bool b = api.GetPumpStatus(out error);

                if (error == null)
                {
                    ModbusRxTbox.AppendText(String.Format("Pump Status is {0}.\r\n", b));
                }
                else
                {
                    ModbusRxTbox.AppendText(String.Format("Pump Status Error : {0}.\r\n", error));
                }
            }
            else
            {
                MessageBox.Show("Serial port not connected", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            if (api != null)
            {
                ModbusTxTbox.AppendText("Pump Open command has been sent.\r\n");

                string error;
                api.Open(out error);

                if (error == null)
                {
                    ModbusRxTbox.AppendText("Open successfully.\r\n");
                }
                else
                {
                    ModbusRxTbox.AppendText(String.Format("Pump Open Error : {0}.\r\n", error));
                }
            }
            else
            {
                MessageBox.Show("Serial port not connected", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button8_Click(object sender, EventArgs e)
        {
            if (api != null)
            {
                ModbusTxTbox.AppendText("Pump Close command has been sent.\r\n");
         
                string error;
                api.Close(out error);

                if (error == null)
                {
                    ModbusRxTbox.AppendText("Close successfully.\r\n");
                }
                else
                {
                    ModbusRxTbox.AppendText(String.Format("Pump Close Error : {0}.\r\n", error));
                }
            }
            else
            {
                MessageBox.Show("Serial port not connected", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button12_Click(object sender, EventArgs e)
        {
            if (api != null)
            {
                ModbusTxTbox.AppendText("Pump Fault Status command has been sent.\r\n");

                string error;
                bool b = api.GetPumpFaultStatus(out error);

                if (error == null)
                {
                    ModbusRxTbox.AppendText(String.Format("Pump Fault Status is {0}.\r\n", b));
                }
                else
                {
                    ModbusRxTbox.AppendText(String.Format("Pump Fault Status Error : {0}.\r\n", error));
                }
            }
            else
            {
                MessageBox.Show("Serial port not connected", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button10_Click(object sender, EventArgs e)
        {
            if (api != null)
            {
                ModbusTxTbox.AppendText("Get Flow Rate / Pressure command has been sent.\r\n");

                double localSetFlowRate, remoteSetFlowRate, flowrate, pressure;
                string error;

                api.GetFlowRateAndPresure(out localSetFlowRate, out remoteSetFlowRate, out flowrate, out pressure, out error);

                if (error == null)
                {
                    ModbusRxTbox.AppendText(String.Format("Local Set Flow Rate = {0}, Remote Set Flow Rate = {1}, Flow Rate = {2}, Pressure = {3}.\r\n",
                        localSetFlowRate, remoteSetFlowRate, flowrate, pressure));
                }
                else
                {
                    ModbusRxTbox.AppendText(String.Format("Get Flow Rate/Pressure Error : {0}.\r\n", error));
                }
            }
            else
            {
                MessageBox.Show("Serial port not connected", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button11_Click(object sender, EventArgs e)
        {
            if (api != null)
            {
                ModbusTxTbox.AppendText("Get Low Water Level command has been sent.\r\n");
               
                string error;
                bool b = api.GetLowWaterLevelStatus(out error);

                if (error == null)
                {
                    ModbusRxTbox.AppendText(String.Format("Low Water Level Status is {0}.\r\n", b));
                }
                else
                {
                    ModbusRxTbox.AppendText(String.Format("Low Water Level Status Error : {0}.\r\n", error));
                }
            }
            else
            {
                MessageBox.Show("Serial port not connected", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void PortDropDownList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (PortDropDownList.Items.Count > 0)
            {
                try
                {
                    // Create serial port.
                    port = new SerialPort(PortDropDownList.SelectedItem as string);
                    SetBaudRate();
                    SetDataBits();
                    SetParity();
                    SetStopBits();
                    port.Open();

                    // Create modbus Api.
                    api = new AgAnApi(port);
                    api.ReadTimeOut = (int)ReadTimeoutNumUpDown.Value;
                    api.WriteTimeOut = (int)writeTimeoutNumUpDown.Value;
                    api.Retries = (int)retiresNumUpDown.Value;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void BaudDropDownList_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetBaudRate();
        }

        private void SetBaudRate()
        {
            if (port != null)
            {
                switch (BaudDropDownList.SelectedIndex)
                {
                    case 0:
                        port.BaudRate = 9600;
                        break;
                    case 1:
                        port.BaudRate = 14400;
                        break;
                    case 2:
                        port.BaudRate = 19200;
                        break;
                    case 3:
                        port.BaudRate = 38400;
                        break;
                    case 4:
                        port.BaudRate = 57600;
                        break;
                    case 5:
                        port.BaudRate = 115200;
                        break;
                }
            }
        }

        private void DataDropDownList_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetDataBits();
        }

        private void SetDataBits()
        {
            if (port != null)
            {
                switch (DataDropDownList.SelectedIndex)
                {
                    case 0:
                        port.DataBits = 5;
                        break;
                    case 1:
                        port.DataBits = 6;
                        break;
                    case 2:
                        port.DataBits = 7;
                        break;
                    case 3:
                        port.DataBits = 8;
                        break;
                }
            }
        }

        private void ParityDropDownList_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetParity();
        }

        private void SetParity()
        {
            if (port != null)
            {
                switch (ParityDropDownList.SelectedIndex)
                {
                    case 0:
                        port.Parity = Parity.None;
                        break;
                    case 1:
                        port.Parity = Parity.Even;
                        break;
                    case 2:
                        port.Parity = Parity.Odd;
                        break;
                    case 3:
                        port.Parity = Parity.Mark;
                        break;
                    case 4:
                        port.Parity = Parity.Space;
                        break;
                }
            }
        }

        private void StopDropDownList_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetStopBits();
        }

        private void SetStopBits()
        {
            if (port != null)
            {
                switch (StopDropDownList.SelectedIndex)
                {
                    case 0:
                        port.StopBits = StopBits.None;
                        break;
                    case 1:
                        port.StopBits = StopBits.One;
                        break;
                    case 2:
                        port.StopBits = StopBits.OnePointFive;
                        break;
                    case 3:
                        port.StopBits = StopBits.Two;
                        break;
                }
            }
        }

        private void ReadTimeoutNumUpDown_ValueChanged(object sender, EventArgs e)
        {
            if (api != null)
            {
                api.ReadTimeOut = (int)ReadTimeoutNumUpDown.Value;
            }
        }

        private void writeTimeoutNumUpDown_ValueChanged(object sender, EventArgs e)
        {
            if (api != null)
            {
                api.WriteTimeOut = (int)writeTimeoutNumUpDown.Value;
            }
        }

        private void retiresNumUpDown_ValueChanged(object sender, EventArgs e)
        {
            if (api != null)
            {
                api.Retries = (int)retiresNumUpDown.Value;
            }
        }
     




    }
}
