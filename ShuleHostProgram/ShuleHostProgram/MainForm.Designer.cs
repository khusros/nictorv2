﻿namespace ShuleHostProgram
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.systemTabControl = new System.Windows.Forms.TabControl();
            this.starTabPage = new System.Windows.Forms.TabPage();
            this.disconnectButton = new System.Windows.Forms.Button();
            this.serialGroupBox = new System.Windows.Forms.GroupBox();
            this.pumpPortComboBox = new System.Windows.Forms.ComboBox();
            this.label17 = new System.Windows.Forms.Label();
            this.nictorComboBox = new System.Windows.Forms.ComboBox();
            this.label16 = new System.Windows.Forms.Label();
            this.connGroupBox = new System.Windows.Forms.GroupBox();
            this.label15 = new System.Windows.Forms.Label();
            this.webConnTextBox = new System.Windows.Forms.TextBox();
            this.nictorPortTextBox = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.pumpPortTextBox = new System.Windows.Forms.TextBox();
            this.helpButton = new System.Windows.Forms.Button();
            this.exitButton = new System.Windows.Forms.Button();
            this.connectButton = new System.Windows.Forms.Button();
            this.logsTab = new System.Windows.Forms.TabPage();
            this.clearLogBtn = new System.Windows.Forms.Button();
            this.logTextbox = new System.Windows.Forms.TextBox();
            this.pumpInfoTabPage = new System.Windows.Forms.TabPage();
            this.pumpInfoTextBox = new System.Windows.Forms.TextBox();
            this.advancedTabPage = new System.Windows.Forms.TabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.retiresNumUpDown = new System.Windows.Forms.NumericUpDown();
            this.writeTimeoutNumUpDown = new System.Windows.Forms.NumericUpDown();
            this.ReadTimeoutNumUpDown = new System.Windows.Forms.NumericUpDown();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.StopDropDownList = new System.Windows.Forms.ComboBox();
            this.ParityDropDownList = new System.Windows.Forms.ComboBox();
            this.DataDropDownList = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.BaudDropDownList = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.PortDropDownList = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.pumpFaultButton = new System.Windows.Forms.Button();
            this.FlowRateutton = new System.Windows.Forms.Button();
            this.lowWaterLevelButton = new System.Windows.Forms.Button();
            this.closeButton = new System.Windows.Forms.Button();
            this.openButton = new System.Windows.Forms.Button();
            this.pumpStatusButton = new System.Windows.Forms.Button();
            this.clearRxbutton = new System.Windows.Forms.Button();
            this.clearTxbutton = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.ModbusRxTbox = new System.Windows.Forms.TextBox();
            this.ModbusTxTbox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.clearLogButton = new System.Windows.Forms.Button();
            this.MessageLogPanel = new System.Windows.Forms.TextBox();
            this.pumpBackgroundWorker = new System.ComponentModel.BackgroundWorker();
            this.actionBackgroundWorker = new System.ComponentModel.BackgroundWorker();
            this.webBackgroundWorker = new System.ComponentModel.BackgroundWorker();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.pumpStatusBackgroundWorker = new System.ComponentModel.BackgroundWorker();
            this.systemTabControl.SuspendLayout();
            this.starTabPage.SuspendLayout();
            this.serialGroupBox.SuspendLayout();
            this.connGroupBox.SuspendLayout();
            this.logsTab.SuspendLayout();
            this.pumpInfoTabPage.SuspendLayout();
            this.advancedTabPage.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.retiresNumUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.writeTimeoutNumUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReadTimeoutNumUpDown)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // systemTabControl
            // 
            this.systemTabControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.systemTabControl.Controls.Add(this.starTabPage);
            this.systemTabControl.Controls.Add(this.logsTab);
            this.systemTabControl.Controls.Add(this.pumpInfoTabPage);
            this.systemTabControl.Controls.Add(this.advancedTabPage);
            this.systemTabControl.Location = new System.Drawing.Point(13, 13);
            this.systemTabControl.Name = "systemTabControl";
            this.systemTabControl.SelectedIndex = 0;
            this.systemTabControl.Size = new System.Drawing.Size(859, 514);
            this.systemTabControl.TabIndex = 0;
            // 
            // starTabPage
            // 
            this.starTabPage.Controls.Add(this.disconnectButton);
            this.starTabPage.Controls.Add(this.serialGroupBox);
            this.starTabPage.Controls.Add(this.connGroupBox);
            this.starTabPage.Controls.Add(this.helpButton);
            this.starTabPage.Controls.Add(this.exitButton);
            this.starTabPage.Controls.Add(this.connectButton);
            this.starTabPage.Location = new System.Drawing.Point(4, 22);
            this.starTabPage.Name = "starTabPage";
            this.starTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.starTabPage.Size = new System.Drawing.Size(851, 488);
            this.starTabPage.TabIndex = 0;
            this.starTabPage.Text = "Start";
            this.starTabPage.UseVisualStyleBackColor = true;
            // 
            // disconnectButton
            // 
            this.disconnectButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.disconnectButton.Location = new System.Drawing.Point(547, 21);
            this.disconnectButton.Name = "disconnectButton";
            this.disconnectButton.Size = new System.Drawing.Size(99, 41);
            this.disconnectButton.TabIndex = 22;
            this.disconnectButton.Text = "Disconnect";
            this.disconnectButton.UseVisualStyleBackColor = true;
            this.disconnectButton.Click += new System.EventHandler(this.disconnectButton_Click);
            // 
            // serialGroupBox
            // 
            this.serialGroupBox.Controls.Add(this.pumpPortComboBox);
            this.serialGroupBox.Controls.Add(this.label17);
            this.serialGroupBox.Controls.Add(this.nictorComboBox);
            this.serialGroupBox.Controls.Add(this.label16);
            this.serialGroupBox.Location = new System.Drawing.Point(16, 15);
            this.serialGroupBox.Name = "serialGroupBox";
            this.serialGroupBox.Size = new System.Drawing.Size(444, 76);
            this.serialGroupBox.TabIndex = 21;
            this.serialGroupBox.TabStop = false;
            this.serialGroupBox.Text = "Serial Ports";
            // 
            // pumpPortComboBox
            // 
            this.pumpPortComboBox.FormattingEnabled = true;
            this.pumpPortComboBox.Location = new System.Drawing.Point(303, 31);
            this.pumpPortComboBox.Name = "pumpPortComboBox";
            this.pumpPortComboBox.Size = new System.Drawing.Size(121, 21);
            this.pumpPortComboBox.TabIndex = 3;
            this.pumpPortComboBox.SelectedIndexChanged += new System.EventHandler(this.pumpPortComboBox_SelectedIndexChanged);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(216, 34);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(56, 13);
            this.label17.TabIndex = 2;
            this.label17.Text = "Pump Port";
            // 
            // nictorComboBox
            // 
            this.nictorComboBox.FormattingEnabled = true;
            this.nictorComboBox.Location = new System.Drawing.Point(71, 31);
            this.nictorComboBox.Name = "nictorComboBox";
            this.nictorComboBox.Size = new System.Drawing.Size(121, 21);
            this.nictorComboBox.TabIndex = 1;
            this.nictorComboBox.SelectedIndexChanged += new System.EventHandler(this.nictorComboBox_SelectedIndexChanged);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(10, 34);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(55, 13);
            this.label16.TabIndex = 0;
            this.label16.Text = "WSN Port";
            // 
            // connGroupBox
            // 
            this.connGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.connGroupBox.Controls.Add(this.label15);
            this.connGroupBox.Controls.Add(this.webConnTextBox);
            this.connGroupBox.Controls.Add(this.nictorPortTextBox);
            this.connGroupBox.Controls.Add(this.label14);
            this.connGroupBox.Controls.Add(this.label13);
            this.connGroupBox.Controls.Add(this.label12);
            this.connGroupBox.Controls.Add(this.pumpPortTextBox);
            this.connGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.connGroupBox.Location = new System.Drawing.Point(16, 107);
            this.connGroupBox.Name = "connGroupBox";
            this.connGroupBox.Size = new System.Drawing.Size(817, 204);
            this.connGroupBox.TabIndex = 20;
            this.connGroupBox.TabStop = false;
            this.connGroupBox.Text = "Connection Status";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(5, 160);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(147, 20);
            this.label15.TabIndex = 7;
            this.label15.Text = "Internet connection";
            // 
            // webConnTextBox
            // 
            this.webConnTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.webConnTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.webConnTextBox.Location = new System.Drawing.Point(219, 157);
            this.webConnTextBox.Name = "webConnTextBox";
            this.webConnTextBox.Size = new System.Drawing.Size(583, 26);
            this.webConnTextBox.TabIndex = 5;
            this.webConnTextBox.Text = "Checking for connection to internet...";
            // 
            // nictorPortTextBox
            // 
            this.nictorPortTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.nictorPortTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nictorPortTextBox.Location = new System.Drawing.Point(219, 93);
            this.nictorPortTextBox.Name = "nictorPortTextBox";
            this.nictorPortTextBox.Size = new System.Drawing.Size(583, 26);
            this.nictorPortTextBox.TabIndex = 4;
            this.nictorPortTextBox.Text = "Checking for connection with wireless sensor network...";
            // 
            // label14
            // 
            this.label14.Location = new System.Drawing.Point(0, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(100, 23);
            this.label14.TabIndex = 8;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(5, 96);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(181, 20);
            this.label13.TabIndex = 2;
            this.label13.Text = "Wireless sensor network";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(7, 31);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(50, 20);
            this.label12.TabIndex = 1;
            this.label12.Text = "Pump";
            // 
            // pumpPortTextBox
            // 
            this.pumpPortTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pumpPortTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pumpPortTextBox.Location = new System.Drawing.Point(219, 28);
            this.pumpPortTextBox.Name = "pumpPortTextBox";
            this.pumpPortTextBox.Size = new System.Drawing.Size(583, 26);
            this.pumpPortTextBox.TabIndex = 0;
            this.pumpPortTextBox.Text = "Checking for connection to pump...";
            // 
            // helpButton
            // 
            this.helpButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.helpButton.Location = new System.Drawing.Point(677, 21);
            this.helpButton.Name = "helpButton";
            this.helpButton.Size = new System.Drawing.Size(75, 41);
            this.helpButton.TabIndex = 19;
            this.helpButton.Text = "Help";
            this.helpButton.UseVisualStyleBackColor = true;
            this.helpButton.Visible = false;
            this.helpButton.Click += new System.EventHandler(this.helpButton_Click);
            // 
            // exitButton
            // 
            this.exitButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.exitButton.Location = new System.Drawing.Point(758, 21);
            this.exitButton.Name = "exitButton";
            this.exitButton.Size = new System.Drawing.Size(75, 41);
            this.exitButton.TabIndex = 18;
            this.exitButton.Text = "Exit";
            this.exitButton.UseVisualStyleBackColor = true;
            this.exitButton.Click += new System.EventHandler(this.exitButton_Click);
            // 
            // connectButton
            // 
            this.connectButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.connectButton.Location = new System.Drawing.Point(466, 21);
            this.connectButton.Name = "connectButton";
            this.connectButton.Size = new System.Drawing.Size(75, 41);
            this.connectButton.TabIndex = 17;
            this.connectButton.Text = "Connect";
            this.connectButton.UseVisualStyleBackColor = true;
            this.connectButton.Click += new System.EventHandler(this.connectButton_Click);
            // 
            // logsTab
            // 
            this.logsTab.Controls.Add(this.clearLogBtn);
            this.logsTab.Controls.Add(this.logTextbox);
            this.logsTab.Location = new System.Drawing.Point(4, 22);
            this.logsTab.Name = "logsTab";
            this.logsTab.Padding = new System.Windows.Forms.Padding(3);
            this.logsTab.Size = new System.Drawing.Size(851, 488);
            this.logsTab.TabIndex = 2;
            this.logsTab.Text = "Logs";
            this.logsTab.UseVisualStyleBackColor = true;
            // 
            // clearLogBtn
            // 
            this.clearLogBtn.Location = new System.Drawing.Point(25, 414);
            this.clearLogBtn.Name = "clearLogBtn";
            this.clearLogBtn.Size = new System.Drawing.Size(75, 23);
            this.clearLogBtn.TabIndex = 1;
            this.clearLogBtn.Text = "Clear";
            this.clearLogBtn.UseVisualStyleBackColor = true;
            this.clearLogBtn.Click += new System.EventHandler(this.clearLogBtn_Click);
            // 
            // logTextbox
            // 
            this.logTextbox.Location = new System.Drawing.Point(25, 32);
            this.logTextbox.Multiline = true;
            this.logTextbox.Name = "logTextbox";
            this.logTextbox.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.logTextbox.Size = new System.Drawing.Size(802, 375);
            this.logTextbox.TabIndex = 0;
            // 
            // pumpInfoTabPage
            // 
            this.pumpInfoTabPage.Controls.Add(this.pumpInfoTextBox);
            this.pumpInfoTabPage.Location = new System.Drawing.Point(4, 22);
            this.pumpInfoTabPage.Name = "pumpInfoTabPage";
            this.pumpInfoTabPage.Size = new System.Drawing.Size(851, 488);
            this.pumpInfoTabPage.TabIndex = 3;
            this.pumpInfoTabPage.Text = "Pump Information";
            this.pumpInfoTabPage.UseVisualStyleBackColor = true;
            // 
            // pumpInfoTextBox
            // 
            this.pumpInfoTextBox.Location = new System.Drawing.Point(19, 20);
            this.pumpInfoTextBox.Multiline = true;
            this.pumpInfoTextBox.Name = "pumpInfoTextBox";
            this.pumpInfoTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.pumpInfoTextBox.Size = new System.Drawing.Size(809, 447);
            this.pumpInfoTextBox.TabIndex = 0;
            // 
            // advancedTabPage
            // 
            this.advancedTabPage.Controls.Add(this.groupBox3);
            this.advancedTabPage.Controls.Add(this.groupBox2);
            this.advancedTabPage.Controls.Add(this.groupBox1);
            this.advancedTabPage.Controls.Add(this.label1);
            this.advancedTabPage.Controls.Add(this.clearLogButton);
            this.advancedTabPage.Controls.Add(this.MessageLogPanel);
            this.advancedTabPage.Location = new System.Drawing.Point(4, 22);
            this.advancedTabPage.Name = "advancedTabPage";
            this.advancedTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.advancedTabPage.Size = new System.Drawing.Size(851, 488);
            this.advancedTabPage.TabIndex = 1;
            this.advancedTabPage.Text = "Advanced";
            this.advancedTabPage.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.retiresNumUpDown);
            this.groupBox3.Controls.Add(this.writeTimeoutNumUpDown);
            this.groupBox3.Controls.Add(this.ReadTimeoutNumUpDown);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Location = new System.Drawing.Point(287, 77);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(476, 68);
            this.groupBox3.TabIndex = 18;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Modbus configuration";
            // 
            // retiresNumUpDown
            // 
            this.retiresNumUpDown.Location = new System.Drawing.Point(390, 25);
            this.retiresNumUpDown.Name = "retiresNumUpDown";
            this.retiresNumUpDown.ReadOnly = true;
            this.retiresNumUpDown.Size = new System.Drawing.Size(80, 20);
            this.retiresNumUpDown.TabIndex = 5;
            this.retiresNumUpDown.ValueChanged += new System.EventHandler(this.retiresNumUpDown_ValueChanged);
            // 
            // writeTimeoutNumUpDown
            // 
            this.writeTimeoutNumUpDown.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.writeTimeoutNumUpDown.Location = new System.Drawing.Point(254, 25);
            this.writeTimeoutNumUpDown.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.writeTimeoutNumUpDown.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.writeTimeoutNumUpDown.Name = "writeTimeoutNumUpDown";
            this.writeTimeoutNumUpDown.ReadOnly = true;
            this.writeTimeoutNumUpDown.Size = new System.Drawing.Size(80, 20);
            this.writeTimeoutNumUpDown.TabIndex = 4;
            this.writeTimeoutNumUpDown.Value = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.writeTimeoutNumUpDown.ValueChanged += new System.EventHandler(this.writeTimeoutNumUpDown_ValueChanged);
            // 
            // ReadTimeoutNumUpDown
            // 
            this.ReadTimeoutNumUpDown.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.ReadTimeoutNumUpDown.Location = new System.Drawing.Point(89, 25);
            this.ReadTimeoutNumUpDown.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.ReadTimeoutNumUpDown.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.ReadTimeoutNumUpDown.Name = "ReadTimeoutNumUpDown";
            this.ReadTimeoutNumUpDown.ReadOnly = true;
            this.ReadTimeoutNumUpDown.Size = new System.Drawing.Size(80, 20);
            this.ReadTimeoutNumUpDown.TabIndex = 3;
            this.ReadTimeoutNumUpDown.Value = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.ReadTimeoutNumUpDown.ValueChanged += new System.EventHandler(this.ReadTimeoutNumUpDown_ValueChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(344, 27);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(40, 13);
            this.label11.TabIndex = 2;
            this.label11.Text = "Retries";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(175, 27);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(73, 13);
            this.label10.TabIndex = 1;
            this.label10.Text = "Write Timeout";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(9, 27);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(74, 13);
            this.label9.TabIndex = 0;
            this.label9.Text = "Read Timeout";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.StopDropDownList);
            this.groupBox2.Controls.Add(this.ParityDropDownList);
            this.groupBox2.Controls.Add(this.DataDropDownList);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.BaudDropDownList);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.PortDropDownList);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Location = new System.Drawing.Point(287, 14);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(544, 57);
            this.groupBox2.TabIndex = 17;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Serial port configuration";
            // 
            // StopDropDownList
            // 
            this.StopDropDownList.AutoCompleteCustomSource.AddRange(new string[] {
            "1",
            "2"});
            this.StopDropDownList.FormattingEnabled = true;
            this.StopDropDownList.Items.AddRange(new object[] {
            "0",
            "1",
            "1.5",
            "2"});
            this.StopDropDownList.Location = new System.Drawing.Point(482, 27);
            this.StopDropDownList.Name = "StopDropDownList";
            this.StopDropDownList.Size = new System.Drawing.Size(48, 21);
            this.StopDropDownList.TabIndex = 14;
            this.StopDropDownList.SelectedIndexChanged += new System.EventHandler(this.StopDropDownList_SelectedIndexChanged);
            // 
            // ParityDropDownList
            // 
            this.ParityDropDownList.FormattingEnabled = true;
            this.ParityDropDownList.Items.AddRange(new object[] {
            "None",
            "Even",
            "Odd",
            "Mark",
            "Space"});
            this.ParityDropDownList.Location = new System.Drawing.Point(379, 27);
            this.ParityDropDownList.Name = "ParityDropDownList";
            this.ParityDropDownList.Size = new System.Drawing.Size(61, 21);
            this.ParityDropDownList.TabIndex = 13;
            this.ParityDropDownList.SelectedIndexChanged += new System.EventHandler(this.ParityDropDownList_SelectedIndexChanged);
            // 
            // DataDropDownList
            // 
            this.DataDropDownList.FormattingEnabled = true;
            this.DataDropDownList.Items.AddRange(new object[] {
            "5",
            "6",
            "7",
            "8"});
            this.DataDropDownList.Location = new System.Drawing.Point(278, 27);
            this.DataDropDownList.Name = "DataDropDownList";
            this.DataDropDownList.Size = new System.Drawing.Size(56, 21);
            this.DataDropDownList.TabIndex = 12;
            this.DataDropDownList.SelectedIndexChanged += new System.EventHandler(this.DataDropDownList_SelectedIndexChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(340, 30);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(33, 13);
            this.label8.TabIndex = 11;
            this.label8.Text = "Parity";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(242, 30);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(30, 13);
            this.label7.TabIndex = 10;
            this.label7.Text = "Data";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(447, 30);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(29, 13);
            this.label6.TabIndex = 9;
            this.label6.Text = "Stop";
            // 
            // BaudDropDownList
            // 
            this.BaudDropDownList.FormattingEnabled = true;
            this.BaudDropDownList.Items.AddRange(new object[] {
            "9600",
            "14400",
            "19200",
            "38400",
            "57600",
            "115200"});
            this.BaudDropDownList.Location = new System.Drawing.Point(159, 27);
            this.BaudDropDownList.Name = "BaudDropDownList";
            this.BaudDropDownList.Size = new System.Drawing.Size(68, 21);
            this.BaudDropDownList.TabIndex = 9;
            this.BaudDropDownList.SelectedIndexChanged += new System.EventHandler(this.BaudDropDownList_SelectedIndexChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(121, 30);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(32, 13);
            this.label5.TabIndex = 2;
            this.label5.Text = "Baud";
            // 
            // PortDropDownList
            // 
            this.PortDropDownList.FormattingEnabled = true;
            this.PortDropDownList.Location = new System.Drawing.Point(40, 27);
            this.PortDropDownList.Name = "PortDropDownList";
            this.PortDropDownList.Size = new System.Drawing.Size(73, 21);
            this.PortDropDownList.TabIndex = 1;
            this.PortDropDownList.SelectedIndexChanged += new System.EventHandler(this.PortDropDownList_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(8, 30);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(26, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Port";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.pumpFaultButton);
            this.groupBox1.Controls.Add(this.FlowRateutton);
            this.groupBox1.Controls.Add(this.lowWaterLevelButton);
            this.groupBox1.Controls.Add(this.closeButton);
            this.groupBox1.Controls.Add(this.openButton);
            this.groupBox1.Controls.Add(this.pumpStatusButton);
            this.groupBox1.Controls.Add(this.clearRxbutton);
            this.groupBox1.Controls.Add(this.clearTxbutton);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.ModbusRxTbox);
            this.groupBox1.Controls.Add(this.ModbusTxTbox);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(5, 252);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(843, 244);
            this.groupBox1.TabIndex = 15;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Pump control panel";
            // 
            // pumpFaultButton
            // 
            this.pumpFaultButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pumpFaultButton.Location = new System.Drawing.Point(101, 41);
            this.pumpFaultButton.Name = "pumpFaultButton";
            this.pumpFaultButton.Size = new System.Drawing.Size(75, 23);
            this.pumpFaultButton.TabIndex = 18;
            this.pumpFaultButton.Text = "Fault status";
            this.pumpFaultButton.UseVisualStyleBackColor = true;
            this.pumpFaultButton.Click += new System.EventHandler(this.pumpFaultButton_Click);
            // 
            // FlowRateutton
            // 
            this.FlowRateutton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FlowRateutton.Location = new System.Drawing.Point(20, 99);
            this.FlowRateutton.Name = "FlowRateutton";
            this.FlowRateutton.Size = new System.Drawing.Size(156, 23);
            this.FlowRateutton.TabIndex = 16;
            this.FlowRateutton.Text = "Flow Rate / Presure";
            this.FlowRateutton.UseVisualStyleBackColor = true;
            this.FlowRateutton.Click += new System.EventHandler(this.FlowRateutton_Click);
            // 
            // lowWaterLevelButton
            // 
            this.lowWaterLevelButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lowWaterLevelButton.Location = new System.Drawing.Point(182, 41);
            this.lowWaterLevelButton.Name = "lowWaterLevelButton";
            this.lowWaterLevelButton.Size = new System.Drawing.Size(101, 23);
            this.lowWaterLevelButton.TabIndex = 15;
            this.lowWaterLevelButton.Text = "Low Water Level";
            this.lowWaterLevelButton.UseVisualStyleBackColor = true;
            this.lowWaterLevelButton.Click += new System.EventHandler(this.lowWaterLevelButton_Click);
            // 
            // closeButton
            // 
            this.closeButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.closeButton.Location = new System.Drawing.Point(101, 70);
            this.closeButton.Name = "closeButton";
            this.closeButton.Size = new System.Drawing.Size(75, 23);
            this.closeButton.TabIndex = 14;
            this.closeButton.Text = "Close";
            this.closeButton.UseVisualStyleBackColor = true;
            this.closeButton.Click += new System.EventHandler(this.closeButton_Click);
            // 
            // openButton
            // 
            this.openButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.openButton.Location = new System.Drawing.Point(20, 70);
            this.openButton.Name = "openButton";
            this.openButton.Size = new System.Drawing.Size(75, 23);
            this.openButton.TabIndex = 13;
            this.openButton.Text = "Open";
            this.openButton.UseVisualStyleBackColor = true;
            this.openButton.Click += new System.EventHandler(this.openButton_Click);
            // 
            // pumpStatusButton
            // 
            this.pumpStatusButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pumpStatusButton.Location = new System.Drawing.Point(20, 41);
            this.pumpStatusButton.Name = "pumpStatusButton";
            this.pumpStatusButton.Size = new System.Drawing.Size(75, 23);
            this.pumpStatusButton.TabIndex = 12;
            this.pumpStatusButton.Text = "Pump status";
            this.pumpStatusButton.UseVisualStyleBackColor = true;
            this.pumpStatusButton.Click += new System.EventHandler(this.pumpStatusButton_Click);
            // 
            // clearRxbutton
            // 
            this.clearRxbutton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clearRxbutton.Location = new System.Drawing.Point(752, 134);
            this.clearRxbutton.Name = "clearRxbutton";
            this.clearRxbutton.Size = new System.Drawing.Size(75, 23);
            this.clearRxbutton.TabIndex = 11;
            this.clearRxbutton.Text = "Clear";
            this.clearRxbutton.UseVisualStyleBackColor = true;
            this.clearRxbutton.Click += new System.EventHandler(this.clearRxbutton_Click);
            // 
            // clearTxbutton
            // 
            this.clearTxbutton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clearTxbutton.Location = new System.Drawing.Point(751, 24);
            this.clearTxbutton.Name = "clearTxbutton";
            this.clearTxbutton.Size = new System.Drawing.Size(75, 23);
            this.clearTxbutton.TabIndex = 10;
            this.clearTxbutton.Text = "Clear";
            this.clearTxbutton.UseVisualStyleBackColor = true;
            this.clearTxbutton.Click += new System.EventHandler(this.clearTxbutton_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(404, 137);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(81, 17);
            this.label3.TabIndex = 9;
            this.label3.Text = "Modbus RX";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(404, 27);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(80, 17);
            this.label2.TabIndex = 8;
            this.label2.Text = "Modbus TX";
            // 
            // ModbusRxTbox
            // 
            this.ModbusRxTbox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.ModbusRxTbox.Location = new System.Drawing.Point(407, 163);
            this.ModbusRxTbox.Multiline = true;
            this.ModbusRxTbox.Name = "ModbusRxTbox";
            this.ModbusRxTbox.ReadOnly = true;
            this.ModbusRxTbox.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.ModbusRxTbox.Size = new System.Drawing.Size(420, 66);
            this.ModbusRxTbox.TabIndex = 7;
            // 
            // ModbusTxTbox
            // 
            this.ModbusTxTbox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.ModbusTxTbox.Location = new System.Drawing.Point(406, 53);
            this.ModbusTxTbox.Multiline = true;
            this.ModbusTxTbox.Name = "ModbusTxTbox";
            this.ModbusTxTbox.ReadOnly = true;
            this.ModbusTxTbox.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.ModbusTxTbox.Size = new System.Drawing.Size(420, 66);
            this.ModbusTxTbox.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(2, 128);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(127, 17);
            this.label1.TabIndex = 14;
            this.label1.Text = "Message log panel";
            // 
            // clearLogButton
            // 
            this.clearLogButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clearLogButton.Location = new System.Drawing.Point(773, 122);
            this.clearLogButton.Name = "clearLogButton";
            this.clearLogButton.Size = new System.Drawing.Size(75, 23);
            this.clearLogButton.TabIndex = 13;
            this.clearLogButton.Text = "Clear log";
            this.clearLogButton.UseVisualStyleBackColor = true;
            this.clearLogButton.Click += new System.EventHandler(this.clearLogButton_Click);
            // 
            // MessageLogPanel
            // 
            this.MessageLogPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.MessageLogPanel.Location = new System.Drawing.Point(3, 152);
            this.MessageLogPanel.Multiline = true;
            this.MessageLogPanel.Name = "MessageLogPanel";
            this.MessageLogPanel.ReadOnly = true;
            this.MessageLogPanel.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.MessageLogPanel.Size = new System.Drawing.Size(845, 94);
            this.MessageLogPanel.TabIndex = 12;
            // 
            // pumpBackgroundWorker
            // 
            this.pumpBackgroundWorker.WorkerReportsProgress = true;
            this.pumpBackgroundWorker.WorkerSupportsCancellation = true;
            this.pumpBackgroundWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.pumpBackgroundWorker_DoWork);
            this.pumpBackgroundWorker.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.pumpBackgroundWorker_ProgressChanged);
            this.pumpBackgroundWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.pumpBackgroundWorker_RunWorkerCompleted);
            // 
            // actionBackgroundWorker
            // 
            this.actionBackgroundWorker.WorkerReportsProgress = true;
            this.actionBackgroundWorker.WorkerSupportsCancellation = true;
            this.actionBackgroundWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.actionBackgroundWorker_DoWork);
            this.actionBackgroundWorker.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.actionBackgroundWorker_ProgressChanged);
            this.actionBackgroundWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.actionBackgroundWorker_RunWorkerCompleted);
            // 
            // webBackgroundWorker
            // 
            this.webBackgroundWorker.WorkerReportsProgress = true;
            this.webBackgroundWorker.WorkerSupportsCancellation = true;
            this.webBackgroundWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.webBackgroundWorker_DoWork);
            this.webBackgroundWorker.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.webBackgroundWorker_ProgressChanged);
            this.webBackgroundWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.webBackgroundWorker_RunWorkerCompleted);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Location = new System.Drawing.Point(0, 540);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(884, 22);
            this.statusStrip1.TabIndex = 1;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // pumpStatusBackgroundWorker
            // 
            this.pumpStatusBackgroundWorker.WorkerReportsProgress = true;
            this.pumpStatusBackgroundWorker.WorkerSupportsCancellation = true;
            this.pumpStatusBackgroundWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.pumpStatusBackgroundWorker_DoWork);
            this.pumpStatusBackgroundWorker.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.pumpStatusBackgroundWorker_ProgressChanged);
            this.pumpStatusBackgroundWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.pumpStatusBackgroundWorker_RunWorkerCompleted);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(884, 562);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.systemTabControl);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainForm";
            this.Text = "Ag Analytics - Wireless sensor network host PC control panel";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.systemTabControl.ResumeLayout(false);
            this.starTabPage.ResumeLayout(false);
            this.serialGroupBox.ResumeLayout(false);
            this.serialGroupBox.PerformLayout();
            this.connGroupBox.ResumeLayout(false);
            this.connGroupBox.PerformLayout();
            this.logsTab.ResumeLayout(false);
            this.logsTab.PerformLayout();
            this.pumpInfoTabPage.ResumeLayout(false);
            this.pumpInfoTabPage.PerformLayout();
            this.advancedTabPage.ResumeLayout(false);
            this.advancedTabPage.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.retiresNumUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.writeTimeoutNumUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReadTimeoutNumUpDown)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl systemTabControl;
        private System.Windows.Forms.TabPage starTabPage;
        private System.Windows.Forms.TabPage advancedTabPage;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.NumericUpDown retiresNumUpDown;
        private System.Windows.Forms.NumericUpDown writeTimeoutNumUpDown;
        private System.Windows.Forms.NumericUpDown ReadTimeoutNumUpDown;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox StopDropDownList;
        private System.Windows.Forms.ComboBox ParityDropDownList;
        private System.Windows.Forms.ComboBox DataDropDownList;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox BaudDropDownList;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox PortDropDownList;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button pumpFaultButton;
        private System.Windows.Forms.Button FlowRateutton;
        private System.Windows.Forms.Button lowWaterLevelButton;
        private System.Windows.Forms.Button closeButton;
        private System.Windows.Forms.Button openButton;
        private System.Windows.Forms.Button pumpStatusButton;
        private System.Windows.Forms.Button clearRxbutton;
        private System.Windows.Forms.Button clearTxbutton;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox ModbusRxTbox;
        private System.Windows.Forms.TextBox ModbusTxTbox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button clearLogButton;
        private System.Windows.Forms.TextBox MessageLogPanel;
        private System.Windows.Forms.Button helpButton;
        private System.Windows.Forms.Button exitButton;
        private System.Windows.Forms.Button connectButton;
        private System.Windows.Forms.GroupBox connGroupBox;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox pumpPortTextBox;
        private System.Windows.Forms.TextBox webConnTextBox;
        private System.Windows.Forms.TextBox nictorPortTextBox;
        private System.ComponentModel.BackgroundWorker pumpBackgroundWorker;
        private System.ComponentModel.BackgroundWorker actionBackgroundWorker;
        private System.ComponentModel.BackgroundWorker webBackgroundWorker;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TabPage logsTab;
        private System.Windows.Forms.Button clearLogBtn;
        private System.Windows.Forms.TextBox logTextbox;
        private System.Windows.Forms.GroupBox serialGroupBox;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ComboBox pumpPortComboBox;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.ComboBox nictorComboBox;
        private System.Windows.Forms.Button disconnectButton;
        private System.ComponentModel.BackgroundWorker pumpStatusBackgroundWorker;
        private System.Windows.Forms.TabPage pumpInfoTabPage;
        private System.Windows.Forms.TextBox pumpInfoTextBox;
    }
}