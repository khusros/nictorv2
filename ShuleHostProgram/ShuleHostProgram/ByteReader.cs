﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;

namespace ShuleHostProgram
{
    public class ByteReader
    {
        private int cmd_currentState;
        private bool cmd_isPrevRxByteDLE;
        private int cmd_numTotalRxByte;
        public int cmd_numPayloadRxByte;

        private const byte DLE = 0x10;                        // escape char
        private const byte SOF = 0x12;                        // start of frame delimiter
        private const byte EOF = 0x13;                        // end of frame delimiter
        private const byte PKT_IDLE = 5;                           // search for SOF in this state
        private const byte PKT_ASMBL = 6;                           // search for EOF in this state and assemble pkt

        private const byte MAX_PAYLOAD_FIELD_BYTES = 253;
        private const byte NICTOR_SERIAL_INTERFACE_RX_BUFFER_SIZE = (MAX_PAYLOAD_FIELD_BYTES + 2); // Add to for the CRC bytes.
        private const byte INDEX_OF_CMD_TYPE = 1;
        private const byte INDEX_OF_CMD_ID = 2;
        private const byte INDEX_OF_PAYLOAD = 3;
        private const byte INDEX_OF_ERROR_CODE = 3;

        public byte[] cmd_pRxPayload = new byte[1000];

        // Constructor
        public ByteReader()
        {
            cmd_pRxPayload = new byte[NICTOR_SERIAL_INTERFACE_RX_BUFFER_SIZE];
            CMD_initByteProcessing();
        }

        static ushort crc_xmodem_update(ushort crc, byte[] data, int pos)
        {
            ushort temp = data[0];
            temp = (ushort)(temp * 256);
            crc = (ushort)(crc ^ temp);

            int i;
            for (i = 0; i < 8; i++)
            {
                if ((crc & (ushort)0x8000) != 0)
                    crc = (ushort)((crc << 1) ^ 0x1021);
                else
                    crc <<= 1;
            }

            return crc;
        }

        public void CMD_initByteProcessing()
        {
            cmd_currentState = PKT_IDLE;
            cmd_isPrevRxByteDLE = false;
            cmd_numTotalRxByte = 0;
            cmd_numPayloadRxByte = 0;
        }

        public bool processRxBytes(int rxByte)
        {
            bool isPacketFound = false;

            if (cmd_currentState == PKT_IDLE)
            {
                if (rxByte == DLE)
                {
                    cmd_isPrevRxByteDLE = true;
                    isPacketFound = false;
                }
                else
                {
                    if (cmd_isPrevRxByteDLE)
                    {
                        if (rxByte == SOF)
                        {
                            cmd_currentState = PKT_ASMBL;
                            cmd_numTotalRxByte = 0;
                            cmd_numPayloadRxByte = 0;
                        }
                    }
                    cmd_isPrevRxByteDLE = false;
                    isPacketFound = false;
                }
            }
            else
            {
                if (rxByte == DLE)
                {
                    if (cmd_isPrevRxByteDLE)
                    {
                        // Should not happen
                    }
                    else
                    {
                        cmd_isPrevRxByteDLE = true;
                        isPacketFound = false;
                    }
                }
                else
                {
                    if (cmd_isPrevRxByteDLE)
                    {
                        if (rxByte == SOF)
                        {
                            // DLE/SOF
                            cmd_numTotalRxByte = 0;
                            cmd_numPayloadRxByte = 0;
                            cmd_isPrevRxByteDLE = false;
                            isPacketFound = false;
                        }
                        else if (rxByte == EOF)
                        {
                            cmd_isPrevRxByteDLE = false;
                            if (cmd_numTotalRxByte < 3)
                            {
                                CMD_initByteProcessing();
                                isPacketFound = false;
                            }
                            else
                            {
                                // DLE/EOF
                                cmd_currentState = PKT_IDLE;
                                cmd_numPayloadRxByte = cmd_numTotalRxByte - 2; // Take off num CRC bytes
                                isPacketFound = true;
                            }
                        }
                        else
                        {
                            // DLE/X !
                            cmd_isPrevRxByteDLE = false;
                            if (cmd_numTotalRxByte == NICTOR_SERIAL_INTERFACE_RX_BUFFER_SIZE)
                            {
                                CMD_initByteProcessing();
                            }
                            else
                            {
                                cmd_pRxPayload[cmd_numTotalRxByte + 1] = (byte)rxByte;// We add on a/c first byte will be used to store cmd_numPayloadRxByte for FIFO
                                ++cmd_numTotalRxByte;
                            }
                            isPacketFound = false;
                        }
                    }
                    else
                    {
                        // NOT DLE/X
                        cmd_isPrevRxByteDLE = false;
                        if (cmd_numTotalRxByte == NICTOR_SERIAL_INTERFACE_RX_BUFFER_SIZE)
                        {
                            CMD_initByteProcessing();
                        }
                        else
                        {
                            cmd_pRxPayload[cmd_numTotalRxByte + 1] = (byte)rxByte; // We add on a/c first byte will be used to store cmd_numPayloadRxByte for FIFO
                            ++cmd_numTotalRxByte;
                        }
                        isPacketFound = false;
                    }
                }
            }

            return isPacketFound;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cmdType"></param>
        /// <param name="cmdID"></param>
        /// <param name="numBytes">Number of bytes in pPayload</param>
        /// <param name="pPayload">The content</param>
        /// <returns></returns>
        public bool CMD_buildSendMessageOld(ushort numBytes, byte[] pPayload, NetworkStream clientStream)
        {
            byte[] sendMessage = new byte[numBytes + 2];
            ushort theCRC = 0;
            for (int k = 0; k < numBytes; k++)
            {
                byte[] tempByteArray3 = { pPayload[k] };
                theCRC = crc_xmodem_update(theCRC, tempByteArray3, k);
            }
            for (int k = 0; k < numBytes; k++)
            {
                sendMessage[k] = pPayload[k];
            }
            sendMessage[numBytes] = (byte)((theCRC >> 8) & 0xFF);
            sendMessage[numBytes + 1] = (byte)((theCRC) & 0xFF);

            clientStream.WriteByte(DLE);
            clientStream.WriteByte((byte)(SOF));

            for (int k = 0; k < numBytes + 2; k++)
            {
                if (sendMessage[k] == DLE)
                {
                    clientStream.WriteByte(DLE);
                    clientStream.WriteByte((byte)(sendMessage[k]));
                }
                else
                {
                    clientStream.WriteByte(sendMessage[k]);
                }
            }
            clientStream.WriteByte(DLE);
            clientStream.WriteByte((byte)(EOF));

            return true;
        }


        public bool CMD_buildSendMessage(ushort numBytes, byte[] pPayload, System.IO.Ports.SerialPort serialPort)
        {
            byte[] sendMessage = new byte[numBytes + 2];
            byte[] tempByteStorage = new byte[1];
            ushort theCRC = 0;
            for (int k = 0; k < numBytes; k++)
            {
                byte[] tempByteArray3 = { pPayload[k] };
                theCRC = crc_xmodem_update(theCRC, tempByteArray3, k);
            }
            for (int k = 0; k < numBytes; k++)
            {
                sendMessage[k] = pPayload[k];
            }
            sendMessage[numBytes] = (byte)((theCRC >> 8) & 0xFF);
            sendMessage[numBytes + 1] = (byte)((theCRC) & 0xFF);

            tempByteStorage[0] = DLE;
            serialPort.Write(tempByteStorage, 0, 1);
            tempByteStorage[0] = SOF;
            serialPort.Write(tempByteStorage, 0, 1);

            for (int k = 0; k < numBytes + 2; k++)
            {
                if (sendMessage[k] == DLE)
                {
                    tempByteStorage[0] = DLE;
                    serialPort.Write(tempByteStorage, 0, 1);
                    tempByteStorage[0] = sendMessage[k];
                    serialPort.Write(tempByteStorage, 0, 1);
                }
                else
                {
                    tempByteStorage[0] = sendMessage[k];
                    serialPort.Write(tempByteStorage, 0, 1);
                }
            }
            tempByteStorage[0] = DLE;
            serialPort.Write(tempByteStorage, 0, 1);
            tempByteStorage[0] = EOF;
            serialPort.Write(tempByteStorage, 0, 1);

            return true;
        }
    }
}
