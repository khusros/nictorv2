﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace NictorWebClient
{
    /// <summary>
    /// Summary description for WebService
    /// </summary>

    /// A class just for testing.
    public class Person
    {
        public string firstName { get; set; }
        public string lastName { get; set; }
    }

    [System.Web.Script.Services.ScriptService]
    public class Service : System.Web.Services.WebService
    {
        #region FOR_TESTING_ONLY
        /// <summary>
        /// A test web service method with no input parameters.
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public string HelloWorld()
        {
            return "Hello World";
        }

        /// <summary>
        /// A test web service method with input string parameters and object output.
        /// </summary>
        /// <param name="firstName"></param>
        /// <param name="lastName"></param>
        /// <returns></returns>
        [WebMethod]
        public Person GetName(string firstName, string lastName)
        {
            Person person = new Person();
            person.firstName = firstName;
            person.lastName = lastName;
            return person;
        }

        /// <summary>
        /// A test web service method with input double parameters and double output.
        /// </summary>
        /// <param name="firstName"></param>
        /// <param name="lastName"></param>
        /// <returns></returns>
        [WebMethod]
        public double Sum(double a, double b)
        {
            return a + b;
        }
        #endregion

        #region WEB_SERVICE_INTERFACE


        /// <summary>
        /// Get the oldest pump command
        /// </summary>
        /// <param name="ieeeAddress"></param>
        /// <returns></returns>
        [WebMethod]
        public string GetNextPumpCommand(string ieeeAddress)
        {
            string error;
            DatabaseInterface dbif = new DatabaseInterface();
            bool isAnyValveOpen = dbif.IsAnyValveOpen(out error);
            if (error != "")
            {
                isAnyValveOpen = false;
            }

            if (isAnyValveOpen)
                return "ON";
            else
                return "OFF";
        }

        [WebMethod]
        public void SavePumpFlowRate(int flowRate)
        {

        }

        [WebMethod]
        public void SavePumpPressure(int pressure)
        {

        }

        [WebMethod]
        public void SavePumpLocalSetFlowRate(int localSetFlowRate)
        {

        }

        [WebMethod]
        public void SavePumpRemoteSetFlowRate(int remoteSetFlowRate)
        {

        }

        [WebMethod]
        public void SavePumpState(string state)
        {

        }


        /// <summary>
        /// Get time series of node status information (e.g. battery, internal temp, etc.)
        /// </summary>
        /// <param name="ieeeAddress"></param>
        /// <returns></returns>
        [WebMethod]
        public GetNodeStatusResp GetNodeStatus(string ieeeAddress)
        {
            string error;
            DatabaseInterface dbif = new DatabaseInterface();
            List<NodeStatusItem> items = dbif.GetNodeStatus(ieeeAddress, out error);
            GetNodeStatusResp resp = new GetNodeStatusResp();
            resp.listOfItems = items;
            resp.error = error;
            return resp;
        }


        /// <summary>
        /// Get time series of network status information (e.g. battery, internal temp, etc.)
        /// </summary>
        /// <param name="ieeeAddress"></param>
        /// <returns></returns>
        [WebMethod]
        public GetNetworkStatusResp GetNetworkStatus(string ieeeAddress)
        {
            string error;
            DatabaseInterface dbif = new DatabaseInterface();
            List<NetworkStatusItem> items = dbif.GetNetworkStatus(ieeeAddress, out error);
            GetNetworkStatusResp resp = new GetNetworkStatusResp();
            resp.listOfItems = items;
            resp.error = error;
            return resp;
        }
        
        /// <summary>
        /// Insert a new node status record.
        /// </summary>
        /// <param name="ieeeAddress"></param>
        /// <param name="battery"></param>
        /// <param name="temperature"></param>
        [WebMethod]
        public void WriteNodeStatus(string ieeeAddress, double battery, double temperature)
        {
            string error;
            DatabaseInterface dbif = new DatabaseInterface();
            dbif.WriteNodeStatus(ieeeAddress, battery, temperature, out error);
        }

        [WebMethod]
        public void WriteNetworkStatus(string coordIeeeAddress, double battery, double temperature, int networkState, int networkIntervalInMin, int networkWakeIntervalInSec)
        {
            string error;
            DatabaseInterface dbif = new DatabaseInterface();
            dbif.WriteNetworkStatus(coordIeeeAddress, battery, temperature, networkState, networkIntervalInMin, networkWakeIntervalInSec, out error);
        }

        /// <summary>
        /// Update network properties
        /// </summary>
        /// <param name="description"></param>
        /// <param name="markerColor"></param>
        [WebMethod]
        public UpdateNetworkPropertiesResp UpdateNetworkProperties(string description, string markerColor, 
                                                                    string ieeeAddress)
        {
            string error;
            DatabaseInterface dbif = new DatabaseInterface();
            UpdateNetworkPropertiesResp resp = new UpdateNetworkPropertiesResp();
            Network nwk = new Network();
            nwk.description = description;
            nwk.ieeeAddress = ieeeAddress;
            nwk.markerColor = markerColor;
            dbif.UpdateNetworkProperties(nwk, out error);
            resp.error = error;
            if (error == "")
            {
                resp.nwk = dbif.GetNetwork(nwk.ieeeAddress, out error);
                resp.error = error;
            }
            return resp;
        }


        /// <summary>
        /// Update a network location
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        [WebMethod]
        public UpdateNetworkLocationResp UpdateNetworkLocation(double lat, double lng, string ieeeAddress)
        {
            string error;
            DatabaseInterface dbif = new DatabaseInterface();
            UpdateNetworkLocationResp resp = new UpdateNetworkLocationResp();
            Network nwk = new Network();
            nwk.lat = lat;
            nwk.lng = lng;
            nwk.ieeeAddress = ieeeAddress;
            dbif.UpdateNetworkLocation(nwk, out error);
            resp.error = error;
            if (error == "")
            {
                resp.nwk = dbif.GetNetwork(nwk.ieeeAddress, out error);
                resp.error = error;
            }

            return resp;
        }


        /// <summary>
        /// Update a node's location
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        [WebMethod]
        public UpdateNodeLocationResp UpdateNodeLocation(double lat, double lng, string ieeeAddress)
        {
            string error;
            DatabaseInterface dbif = new DatabaseInterface();
            UpdateNodeLocationResp resp = new UpdateNodeLocationResp();
            Node node = new Node();
            node.lat = lat;
            node.lng = lng;
            node.ieeeAddress = ieeeAddress;
            dbif.UpdateNodeLocation(node, out error);
            resp.error = error;
            if (error == "")
            {
                resp.node = dbif.GetNode(node.ieeeAddress, out error);
                resp.error = error;
            }

            return resp;
        }


        /// <summary>
        /// Get list of all networks for this user in database.
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public GetNetworkListResp GetNetworkList(int userId)
        {
            string error;
            DatabaseInterface dbif = new DatabaseInterface();
            List<Network> nwks = dbif.GetNetworkListByUserId(userId, out error);
            GetNetworkListResp resp = new GetNetworkListResp();
            resp.listOfNetworks = nwks;
            resp.error = error;
            return resp;
        }


        [WebMethod]
        public NictorCommandResp UpdateSleepParameters(string coordIeeeAddress, int beaconInterval, int wakeInterval)
        {
            string error;
            DatabaseInterface dbif = new DatabaseInterface();
            dbif.UpdateSleepParameters(coordIeeeAddress, beaconInterval, wakeInterval, out error);
            NictorCommandResp resp = new NictorCommandResp();
            resp.commandId = NictorCommandIds.NICTOR_SLEEP_UPDATE_CMD_ID;
            resp.ieeeAddress = coordIeeeAddress;
            resp.error = error;
            return resp;
        }

        [WebMethod]
        public string StartNetwork(string coordIeeeAddress)
        {
            DatabaseInterface dbif = new DatabaseInterface();
            string error1;
            string error2;
            dbif.StartNetwork(coordIeeeAddress, out error1);
            dbif.UpdateSleepParameters(coordIeeeAddress, 20, 30, out error2); 
            if ((error1 == "") && (error2 == "")) { return "Network starting. Please wait..."; }
            return "Sorry, something went wrong, network was not started. Please try again.";
        }

        [WebMethod]
        public string StopNetwork(string coordIeeeAddress)
        {
            DatabaseInterface dbif = new DatabaseInterface();
            string error;
            dbif.StopNetwork(coordIeeeAddress, out error);
            if (error == "")
            {
                return "Network stopping. Please wait...";
            }
            return "Sorry, something went wrong, network was not stopped. Please try again.";            
        }

        /// <summary>
        /// Open a valve.
        /// </summary>
        /// <param name="ieeeAddress"></param>
        /// <returns></returns>
        [WebMethod]
        public NictorCommandResp OpenValve(string coordIeeeAddress, string nodeIeeeAddress)
        {
            string error;
            DatabaseInterface dbif = new DatabaseInterface();
            dbif.OpenValve(coordIeeeAddress, nodeIeeeAddress, out error);
            NictorCommandResp resp = new NictorCommandResp();
            resp.commandId = NictorActuationCommands.OPEN_VALVE;
            resp.ieeeAddress = nodeIeeeAddress;
            resp.error = error;
            return resp;
        }

        [WebMethod]
        public IsValveOpenResp IsValveOpen(string ieeeAddress)
        {
            string error;
            DatabaseInterface dbif = new DatabaseInterface();
            bool isOpen = dbif.IsValveOpen(ieeeAddress, out error);
            IsValveOpenResp resp = new IsValveOpenResp();
            resp.isOpen = isOpen;
            resp.error = error;
            return resp;
        }

        /// <summary>
        /// Close a valve.
        /// </summary>
        /// <param name="ieeeAddress"></param>
        /// <returns></returns>
        [WebMethod]
        public NictorCommandResp CloseValve(string coordIeeeAddress, string nodeIeeeAddress)
        {
            string error;
            DatabaseInterface dbif = new DatabaseInterface();
            dbif.CloseValve(coordIeeeAddress, nodeIeeeAddress, out error);
            NictorCommandResp resp = new NictorCommandResp();
            resp.commandId = NictorActuationCommands.CLOSE_VALVE;
            resp.ieeeAddress = nodeIeeeAddress;
            resp.error = error;
            return resp;
        }


        /// <summary>
        /// Get a list of all commands in the database.
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        [WebMethod]
        public GetCommandsListResp GetCommandsList(string userId)
        {
            string error;
            DatabaseInterface dbif = new DatabaseInterface();
            List<NictorCommand> commands = dbif.GetCommandList(out error);
            GetCommandsListResp resp = new GetCommandsListResp();
            resp.listOfItems = commands;
            resp.error = error;
            return resp;
        }


        /// <summary>
        /// Get the next commands for this node (returns the oldest command that has not
        /// been actioned, e.g. FIFO behavior).
        /// </summary>
        /// <param name="coordIeeeAddress"></param>
        /// <returns></returns>
        [WebMethod]
        public string GetNextCommand(string coordIeeeAddress)
        {
            string error;
            DatabaseInterface dbif = new DatabaseInterface();
            string resp = dbif.GetNextCommand(coordIeeeAddress, out error);
            return resp;
        }

        [WebMethod]
        public string GetNextCommandAlt(string coordIeeeAddress)
        {
            string error;
            DatabaseInterface dbif = new DatabaseInterface();
            string resp = dbif.GetNextCommandAlt(coordIeeeAddress, out error);
            return resp;
        }

        
        [WebMethod]
        public void InsertMeasurement(string ieeeAddress, string data)
        {
            string error;
            DatabaseInterface dbif = new DatabaseInterface();
            dbif.InsertMeasurement(ieeeAddress, data, out error);
        }

        [WebMethod]
        public GetMeasurementListResp GetMeasurementList(string ieeeAddress, string startDate, string endDate)
        {
            string error;
            DatabaseInterface dbif = new DatabaseInterface();
            string measType;
            List<Measurement> data = dbif.SelectMeasurement(ieeeAddress, startDate, endDate, out measType, out error);
            GetMeasurementListResp resp = new GetMeasurementListResp();
            resp.listOfMeas = data;
            resp.error = error;
            resp.measType = measType;
            return resp;
        }

        [WebMethod]
        public GetNetworkMeasurementListResp GetNetworkMeasurementList(string coordIeeeAddress, string startDate, string endDate)
        {
            string error;
            DatabaseInterface dbif = new DatabaseInterface();
            List<List<Measurement>> data = dbif.SelectNetworkMeasurement(coordIeeeAddress, startDate, endDate, out error);
            GetNetworkMeasurementListResp resp = new GetNetworkMeasurementListResp();
            resp.listOfMeas = data;
            resp.error = error;
            return resp;
        }


        [WebMethod]
        public GetMarkerColorsResp GetMarkerColors()
        {
            string error;
            DatabaseInterface dbif = new DatabaseInterface();
            GetMarkerColorsResp resp = new GetMarkerColorsResp();
            resp.colors = dbif.GetMarkerColors(out error);
            resp.error = error;
            return resp;
        }


        [WebMethod]
        public string HelpQuery(string email, string name, string query)
        {
            string result = "ok";
            try
            {
                System.Net.Mail.MailMessage message = new System.Net.Mail.MailMessage();
                message.From = new System.Net.Mail.MailAddress(email);
                message.To.Add(new System.Net.Mail.MailAddress("support@aganalytics.com.au"));
                message.Subject = "Query from " + name;
                message.Body = query;
                System.Net.Mail.SmtpClient client = new System.Net.Mail.SmtpClient("localhost", 25);
                client.Send(message);
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }
            return result;
        }


        [WebMethod]
        public DynamicIrrigationScheduleResp GetDynamicIrrigationSchedule(string actuatorIeeeAddress)
        {
            DynamicIrrigationScheduleResp resp = new DynamicIrrigationScheduleResp();
            DatabaseInterface dbif = new DatabaseInterface();
            string error;
            DynamicIrrigationSchedule dis = dbif.GetDynamicIrrigationSchedule(actuatorIeeeAddress, out error);
            resp.dynamicIrrigationSchedule = dis;
            resp.error = error;
            return resp;
        }


        public DynamicIrrigationScheduleResp UpdateDynamicIrrigationSchedule(DynamicIrrigationSchedule dynamicIrrigationSchedule)
        {
            DynamicIrrigationScheduleResp resp = new DynamicIrrigationScheduleResp();
            string error;
            DatabaseInterface dbif = new DatabaseInterface();
            dbif.UpdateDynamicIrrigationSchedule(dynamicIrrigationSchedule, out error);
            resp.error = error;
            return resp;
        }


        public DynamicIrrigationScheduleResp InsertDynamicIrrigationSchedule(DynamicIrrigationSchedule dynamicIrrigationSchedule)
        {
            DynamicIrrigationScheduleResp resp = new DynamicIrrigationScheduleResp();
            string error;
            DatabaseInterface dbif = new DatabaseInterface();
            dbif.InsertDynamicIrrigationSchedule(dynamicIrrigationSchedule, out error);
            resp.error = error;
            return resp;
        }


        #endregion

    }
}
