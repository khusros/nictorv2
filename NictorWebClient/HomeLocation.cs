﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NictorWebClient
{
    public class HomeLocation
    {
        public double Lat { get; set; }
        public double Lng { get; set; }
    }
}