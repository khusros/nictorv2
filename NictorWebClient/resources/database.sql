﻿USE master
GO

IF EXISTS(SELECT * from sys.databases WHERE name='nictordb')
BEGIN
	ALTER DATABASE nictordb SET SINGLE_USER WITH ROLLBACK IMMEDIATE
END
GO

										
IF EXISTS(SELECT * from sys.databases WHERE name='nictordb')
BEGIN
	DROP DATABASE nictordb
END
GO


CREATE DATABASE nictordb
GO


USE nictordb
GO


CREATE TABLE dbo.Membership (
	Id					INT IDENTITY(1,1) PRIMARY KEY NOT NULL,
	UserGuid			UNIQUEIDENTIFIER NOT NULL DEFAULT newid(),
	Email				VARCHAR(256) UNIQUE,
	PasswordHash		VARCHAR(32),
	Confirmed           BIT,
	ConfirmToken		VARCHAR(32),
	ConfirmTokenDate	DATETIME,
	PasswordResetToken	VARCHAR(32),
	PasswordResetTokenDate DATETIME,		
	DateCreated			DATETIME,
	Role				VARCHAR(32),
	LastLogin			DATETIME,
	Status				VARCHAR,
	HomeLat				FLOAT DEFAULT -37.8136,
	HomeLng				FLOAT DEFAULT 144.9631		
)
GO
INSERT INTO dbo.Membership (Email,PasswordHash,Confirmed) VALUES ('echidna.admin@aganalytics.com.au','0c01cc9d3be76dbb38eaa5f8597eb18c','true');
GO


CREATE TABLE dbo.Nid (
	Id					INT IDENTITY(1,1) PRIMARY KEY NOT NULL,
	Nid					VARCHAR(8) UNIQUE,
	UserId				INT,
	FOREIGN KEY (UserId) REFERENCES dbo.Membership(Id)	
)
GO

CREATE TABLE dbo.MarkerColor (
	Id				int identity(1,1) PRIMARY KEY NOT NULL,
	Color			varchar(32),
	Code			varchar(7)
)
GO
INSERT INTO dbo.MarkerColor (Color,Code) VALUES ('blue','#6b98ff');
INSERT INTO dbo.MarkerColor (Color,Code) VALUES ('red','#ff776b');
INSERT INTO dbo.MarkerColor (Color,Code) VALUES ('green','#97ec7d');
INSERT INTO dbo.MarkerColor (Color,Code) VALUES ('yellow', '#ffed5c');
INSERT INTO dbo.MarkerColor (Color,Code) VALUES ('orange','#fd8d08');
INSERT INTO dbo.MarkerColor (Color,Code) VALUES ('pink','#fdabff');
INSERT INTO dbo.MarkerColor (Color,Code) VALUES ('darkgreen','#01bf00');
INSERT INTO dbo.MarkerColor (Color,Code) VALUES ('paleblue','#bce3ff');
INSERT INTO dbo.MarkerColor (Color,Code) VALUES ('purple','#c89bff');
GO

CREATE TABLE dbo.Network (
	Id				int IDENTITY(1,1) PRIMARY KEY NOT NULL,
	Lat				FLOAT,
	Lng				FLOAT,
	Description     varchar(1024),
	CoordIeeeAddress varchar(16) UNIQUE,
	MarkerColor		varchar(32) DEFAULT 'blue'
)
GO

CREATE TABLE dbo.NetworkStatus (
	Id				INT IDENTITY(1,1) PRIMARY KEY NOT NULL,
	NetworkId		INT,
	RxTimeStamp		DATETIMEOFFSET DEFAULT SYSDATETIMEOFFSET(),
	Battery			FLOAT,
	Temperature		FLOAT,
	NetworkState	INT,  -- 0: IDLE, 1: SLEEP
	NetworkInterval  INT,  -- minutes
	WakeInterval    INT,  -- seconds
	FOREIGN KEY (NetworkId) REFERENCES dbo.Network(Id)	
)
GO


CREATE TABLE dbo.NetworkUser (
	Id				int IDENTITY(1,1) PRIMARY KEY NOT NULL,
	NetworkId		int,
	UserId			int,
	FOREIGN KEY (NetworkId) REFERENCES dbo.Network(Id),
	FOREIGN KEY (UserId) REFERENCES dbo.Membership(Id)
)
GO


CREATE TABLE dbo.Node (
	Id				int IDENTITY(1,1) PRIMARY KEY NOT NULL,
	Lat				FLOAT DEFAULT -9999,
	Lng				FLOAT DEFAULT -9999,
	IeeeAddress		varchar(16) UNIQUE,
	Description		varchar(256),	
	NetworkId		int,
	NodeType		varchar(32), -- shule_flow,shule_ec20
	RelayIsOpen     BIT,
	FOREIGN KEY (NetworkId) REFERENCES dbo.Network(Id)	
)
GO


CREATE TABLE dbo.NodeStatus (
	Id				INT IDENTITY(1,1) PRIMARY KEY NOT NULL,
	NodeId			INT,
	RxTimeStamp		DATETIMEOFFSET DEFAULT SYSDATETIMEOFFSET(),
	Battery			FLOAT,
	Temperature		FLOAT,
	FOREIGN KEY (NodeId) REFERENCES dbo.Node(Id),	
)
GO


CREATE TABLE dbo.Measurement (
	Id				INT IDENTITY(1,1) PRIMARY KEY NOT NULL,
	NodeID			INT,
	RxTimeStamp		DATETIMEOFFSET DEFAULT SYSDATETIMEOFFSET(),
	Data			VARCHAR(64),
	FOREIGN KEY (NodeId) REFERENCES dbo.Node(Id)	
) 
GO

CREATE TABLE dbo.CommandAlt (
	Id					INT IDENTITY(1,1) PRIMARY KEY NOT NULL,
	Timestamp			DATETIMEOFFSET DEFAULT SYSDATETIMEOFFSET(),
	Txd					INT,
	Ackd				INT,
	CommandId			INT,
	DestinationAddress varchar(16),
	CommandPayload		VARCHAR(12) DEFAULT ''
)
GO	

  
CREATE TABLE dbo.Command (
	Id					INT IDENTITY(1,1) PRIMARY KEY NOT NULL,
	Timestamp			DATETIMEOFFSET DEFAULT SYSDATETIMEOFFSET(),
	Txd					INT,
	Ackd				INT,
	CommandId			INT,
	DestinationNodeId	INT,
	FOREIGN KEY (DestinationNodeId) REFERENCES Node(Id)
)
GO			

CREATE TABLE dbo.Automation (
	Id						INT IDENTITY(1,1) PRIMARY KEY NOT NULL,
	ActuatorId				INT,
	IsOperating				BIT,
	SensorId			INT,
	StopIrrigationLevel			FLOAT,
	StartIrrigationLevel			FLOAT,
	FOREIGN KEY (ActuatorId) REFERENCES Node(Id),
	FOREIGN KEY (SensorId) REFERENCES Node(Id)
)
GO

