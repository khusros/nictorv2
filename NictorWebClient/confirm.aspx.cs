﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NictorWebClient.NictorDataSetTableAdapters;

namespace NictorWebClient
{
    public partial class confirm : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Label.Text = "";

            try
            {
                if (Request.QueryString.HasKeys())
                {
                    string token = Request.QueryString["t"];

                    NictorDataSetTableAdapters.MembershipTableAdapter ta = new MembershipTableAdapter();
                    ta.Connection.ConnectionString = System.Configuration.ConfigurationManager.AppSettings["SqlServerConnectionString"];
                    int? days = (int?)ta.GetConfirmMembershipTokenAgeInDays(token);
                    if (days == null)
                    {
                        Response.Redirect("http://www.aganalytics.com.au/Echidna/index.htm");
                    }
                    // TODO: Check for confirm token expiry e.g. if (days <= 7)
                    if (ta.ConfirmMembership(token) == 1)
                    {
                        this.Label.Text = Server.HtmlDecode("Your new aganalytics account is ready. <br />Click on this link <a href=\"http://www.aganalytics.com.au/Echidna/signin.aspx\" style=\"text-decoration:underline;\">here</a> to sign in");
                    }
                    else
                    {
                        Response.Redirect("http://www.aganalytics.com.au/Echidna/index.htm");
                    }
                }
                else
                {
                    Response.Redirect("http://www.aganalytics.com.au/Echidna/index.htm");
                }

            }
            catch (Exception ex)
            {
                this.Label.Text = Server.HtmlDecode("ERROR: " + ex.Message);
            }
        }
    }
}