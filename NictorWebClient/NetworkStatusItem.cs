﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NictorWebClient
{
    public class NetworkStatusItem
    {
        public string timestamp { get; set; }
        public double batteryVoltage { get; set; }
        public double temperature { get; set; }
        public bool isNetworkActive { get; set; }
    }
}