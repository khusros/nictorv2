﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NictorWebClient
{
    public class GetMeasurementListResp
    {
        public List<Measurement> listOfMeas { get; set; }
        public string measType { get; set; }
        public string error { get; set; }
    }
}