﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="signin.aspx.cs" Inherits="NictorWebClient.signin" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Ag Analytics</title>
    <link rel="shortcut icon" href="./resources/images/logoheader.png"/>
    <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400' rel='stylesheet' type='text/css' />
    <link href="StyleSheet.css" type="text/css" rel="Stylesheet" />
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js" ></script>
    <script type="text/javascript">
        $(document).ready(function () {
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div style="margin-top:50px;">
        <table>
            <tr>
                <td>
                <a href="index.htm" class="homeLogoLink"></a>
                </td>
                <td>
                    <table>
                        <tr><td colspan="2"><h1>Sign in</h1></td></tr>
                        <tr><td>Email</td><td><asp:TextBox ID="email" runat="server" style="width:320px;" CssClass="tbox" /></td></tr>
                        <tr><td>Password</td><td><asp:TextBox ID="password" runat="server" CssClass="tbox" style="width:320px;" TextMode="Password" /></td></tr>
                        <tr><td></td><td><asp:Button ID="submit" runat="server" Text="Submit" CssClass="btn" OnClick="SubmitClick" /><a href="recover.aspx" style="padding-left:20px;">Can't sign in</a></td></tr>
                        <tr><td></td><td><asp:Label ID="label" runat="server" style="color:Red; font-size:0.8em;" /></td></tr>
                    </table>
                </td>
            </tr>
        </table>    
    </div>
    </form>
</body>
</html>
