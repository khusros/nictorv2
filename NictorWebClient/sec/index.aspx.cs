﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Newtonsoft.Json;
using System.Web.Security;

namespace NictorWebClient.sec
{

    public partial class index : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    // User identify information may change asynchronously so this information should not be updated after intial page load
                    NictorDataSetTableAdapters.MembershipTableAdapter mta = new NictorDataSetTableAdapters.MembershipTableAdapter();
                    mta.Connection.ConnectionString = System.Configuration.ConfigurationManager.AppSettings["SqlServerConnectionString"];
                    string userEmail = ((FormsIdentity)User.Identity).Name;
                    NictorDataSet.MembershipDataTable dt = mta.GetUserDetailsByEmail(userEmail);
                    this.hfUserId.Value = dt[0].Id.ToString();
                    this.hfUserEmail.Value = dt[0].Email;
                    this.UserLabel.Text = this.hfUserEmail.Value;
                    this.hfHomeLat.Value = dt[0].HomeLat.ToString();
                    this.hfHomeLng.Value = dt[0].HomeLng.ToString();
                }
                catch (Exception ex)
                {
                    Response.Redirect("http://www.aganalytics.com.au/Echinda/error.html");
                }
            }
        }

        protected void SignoutClick(object sender, EventArgs e)
        {
            try
            {
                DatabaseInterface dbif = new DatabaseInterface();
                string error;
                dbif.UpdateHomeLocation(int.Parse(this.hfUserId.Value), Double.Parse(this.hfHomeLat.Value), Double.Parse(this.hfHomeLng.Value), out error);
            }
            catch { }
            finally
            {
                FormsAuthentication.SignOut();
                Response.Redirect("http://www.aganalytics.com.au/Echidna/index.htm");
            }
        }

    }
}