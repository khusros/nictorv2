﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="index.aspx.cs" Inherits="NictorWebClient.sec.index" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Ag Analytics</title>
    <link rel="shortcut icon" href="../resources/images/logoheader.png"/>
    <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400' rel='stylesheet' type='text/css' />
    <link href='http://fonts.googleapis.com/css?family=Droid+Sans+Mono' rel='stylesheet' type='text/css' />
    <link href="../StyleSheet.css" type="text/css" rel="Stylesheet" />
    <link rel="Stylesheet" href="../resources/jqueryui_css/custom-theme/jquery-ui-1.8.23.custom.css" type="text/css" />
    <link rel="Stylesheet" type="text/css" href="../javascript/jquery.timepicker.css" />
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js" ></script>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jqueryui/1.8.21/jquery-ui.min.js"></script>
    <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyCrVlZYUPxQj5RFy36_3-bWie9S9nHSb8A&sensor=false&libraries=drawing"></script>
    <script type="text/javascript" src="../javascript/highcharts/highcharts.js"></script>
    <script type="text/javascript" src="../javascript/jquery.timepicker.min.js"></script>
    <script type="text/javascript" src="../javascript/json2.js"></script>
    <script type="text/javascript">
        var GGeocoder;
        var GMapHomeLocationCoords;
        var GMap;
        var GMarkerColors;
        var GNodes = new Array();
        var GCoordinators = new Array();
        var GNetworks = null;
        var GMyUserId;
        var GMyUserEmail = "";
        var GHomeLat;
        var GHomeLng;

        function initializeMap() {
            GMapHomeLocationCoords = new google.maps.LatLng(GHomeLat, GHomeLng);
            var myOptions = {
                center: GMapHomeLocationCoords,
                zoom: 17,
                mapTypeId: google.maps.MapTypeId.SATELLITE,
                streetViewControl: false,
                panControl: false,
                mapTypeControlOptions: {
                    position: google.maps.ControlPosition.TOP_LEFT
                },
                zoomControlOptions: {
                    style: google.maps.ZoomControlStyle.SMALL,
                    position: google.maps.ControlPosition.TOP_RIGHT
                },
                scaleControl: true,
                scaleControlOptions: {
                    position: google.maps.ControlPosition.BOTTOM_LEFT
                }
            };
            GMap = new google.maps.Map(document.getElementById("mapCanvas"), myOptions);
            GGeocoder = new google.maps.Geocoder();
            google.maps.event.addListener(GMap, 'mousemove', function (event) {
                $('#latCoord').html(event.latLng.lat());
                $('#lngCoord').html(event.latLng.lng());  
            });
            //google.maps.event.addListener(GMap, 'maptypeid_changed', function () { MapTypeChanged(); });
            google.maps.event.addListener(GMap, 'center_changed', function () { $('#hfHomeLat').val(GMap.getCenter().lat()); $('#hfHomeLng').val(GMap.getCenter().lng()); });
        }

        function GetNodeListAndDrawMarkers() {
            var DTO = { 'userId': GMyUserId };
            $.ajax({
                type: "POST",
                url: 'http://www.khusrosaleem.com/Echidna/Service.asmx/GetNetworkList',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify(DTO),
                success: function (data) {
                    if (data != null) {
                        GNodes.length = 0;

                        var resp = data.d;
                        var networkList = resp.listOfNetworks;

                        var error = resp.error;
                        if (error == "") {
                            $('#sideView').empty();
                            if (networkList.length == 0) {
                                $('#sideView').empty().html('<span style="font-size:1.4em;margin-left:10px;">You don\'t have any sensor networks.</span><br/><br/><span style="margin-left:10px;">Not what you\'re expecting? <button id="contact_" class="linkButton">Contact us</button></span>.');
                            }
                            else {
                                GNetworks = networkList;
                                for (var k = 0; k < GNetworks.length; k++) {
                                    var markerColor = GNetworks[k].markerColor;

                                    var coord = new Object();
                                    coord.id = GNetworks[k].id;
                                    coord.ieeeAddress = GNetworks[k].ieeeAddress;
                                    coord.lat = GNetworks[k].lat;
                                    coord.lng = GNetworks[k].lng;
                                    coord.description = GNetworks[k].description;
                                    coord.latestBattery = GNetworks[k].latestBattery;
                                    coord.latestTimestamp = GNetworks[k].latestRxTimestamp;
                                    coord.marker = null;
                                    coord.markerColor = markerColor;
                                    coord.isNetworkActive = GNetworks[k].isNetworkActive;

                                    if (GNetworks[k].nodes.length == 0) {
                                        $('#sideView').append('<ul id="nwk_' + k + '" style="background-color:#E8E8E8;border-top:1px solid gray;"></ul>');
                                        $("#nwk_" + k).append('<li style="height:26px;width:100%;background-color:#D0D0D0;">' +
                                                          '<span style="height:26px;float:left;width:360px;white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">' + coord.description + '</span>' +
                                                          '<span style="height:26px;float:right;"><button id="refreshNwkBtn_x">refresh</button></span></li>');
                                        var coordStatusInfo = (coord.latestBattery == -1) ? "No status information" : (coord.latestBattery + ' V ' + coord.latestTimestamp);
                                        $("#nwk_" + k).append('<li style="height:26px;width:478px;padding-bottom:10px;">' +
                                                        '<span style="height:26px;float:left;">' +
                                                        '<button id="ec_' + coord.ieeeAddress + '" class="nodeBtn">C</button><button class="linkButton" style="color:blue;" id="vc_' + coord.ieeeAddress + '">' + coord.ieeeAddress + '</button></span>' +
                                                        '<span style="height:26px;float:right;">' + coordStatusInfo + '</span></li>');
                                    }
                                    else {
                                        $('#sideView').append('<ul id="nwk_' + k + '" style="background-color:#E8E8E8;border-top:1px solid gray;"></ul>');
                                        $("#nwk_" + k).append('<li style="height:26px;width:100%;background-color:#D0D0D0;">' +
                                                          '<span style="height:26px;float:left;width:360px;white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">' + coord.description + '</span>' +
                                                          '<span style="height:26px;float:right;"><button id="refreshNwkBtn_x">refresh</button></span></li>');
                                        var coordStatusInfo = (coord.latestBattery == -1) ? "No status information" : (coord.latestBattery + ' V ' + coord.latestTimestamp);
                                        $("#nwk_" + k).append('<li style="height:26px;width:478px;">' +
                                                        '<span style="height:26px;float:left;">' +
                                                        '<button id="ec_' + coord.ieeeAddress + '" class="nodeBtn">C</button><button class="linkButton" style="color:blue;" id="vc_' + coord.ieeeAddress + '">' + coord.ieeeAddress + '</button></span>' +
                                                        '<span style="height:26px;float:right;">' + coordStatusInfo + '</span></li>');
                                    }
                                    var children = GNetworks[k].nodes;
                                    for (var n = 0; n < children.length; n++) {
                                        children[n].marker = null;
                                        children[n].markerColor = markerColor;
                                        children[n].coordIeeeAddress = GNetworks[k].ieeeAddress;
                                        var nodeStatusInfo = (children[n].latestBattery == -1) ? "No status information" : (children[n].latestBattery + ' V ' + children[n].latestRxTimestamp);
                                        if (n < children.length - 1) {
                                            // If this is an irrigation node (actuation + flow sensor)
                                            if (children[n].nodeType == "shule_flow") {
                                                $("#nwk_" + k).append('<li style="height:26px;width:478px;">' +
                                                        '<span style="height:26px;float:left;">' +
                                                        '<button id="eact_' + children[n].ieeeAddress + '" class="nodeBtn">F</button><button class="linkButton" id="v_' + children[n].ieeeAddress + '">' + children[n].ieeeAddress + '</button></span>' +
                                                        '<span style="height:26px;float:right;">' + nodeStatusInfo + '</span></li>');
                                            }
                                            else if (children[n].nodeType == "shule_ec20") {
                                                $("#nwk_" + k).append('<li style="height:26px;width:478px;">' +
                                                        '<span style="height:26px;float:left;">' +
                                                        '<button id="esen_' + children[n].ieeeAddress + '" class="nodeBtn">E</button><button class="linkButton" id="v_' + children[n].ieeeAddress + '">' + children[n].ieeeAddress + '</button></span>' +
                                                        '<span style="height:26px;float:right;">' + nodeStatusInfo + '</span></li>');
                                            }
                                            else {
                                                $("#nwk_" + k).append('<li style="height:26px;width:478px;">' +
                                                        '<span style="height:26px;float:left;">' +
                                                        '<button id="esen_' + children[n].ieeeAddress + '" class="nodeBtn">?</button><button class="linkButton" id="v_' + children[n].ieeeAddress + '">' + children[n].ieeeAddress + '</button></span>' +
                                                        '<span style="height:26px;float:right;">' + nodeStatusInfo + '</span></li>');
                                            }
                                        }
                                        else {
                                            // If this is an irrigation node (actuation + flow sensor)
                                            if (children[n].nodeType == "shule_flow") {
                                                $("#nwk_" + k).append('<li style="height:26px;width:478px;padding-bottom:10px;">' +
                                                        '<span style="height:26px;float:left;">' +
                                                        '<button id="eact_' + children[n].ieeeAddress + '" class="nodeBtn">F</button><button class="linkButton" id="v_' + children[n].ieeeAddress + '">' + children[n].ieeeAddress + '</button></span>' +
                                                        '<span style="height:26px;float:right;">' + nodeStatusInfo + '</span></li>');
                                            }
                                            else if (children[n].nodeType == "shule_ec20") {
                                                $("#nwk_" + k).append('<li style="height:26px;width:478px;padding-bottom:10px;">' +
                                                        '<span style="height:26px;float:left;">' +
                                                        '<button id="esen_' + children[n].ieeeAddress + '" class="nodeBtn">E</button><button class="linkButton" id="v_' + children[n].ieeeAddress + '">' + children[n].ieeeAddress + '</button></span>' +
                                                        '<span style="height:26px;float:right;">' + nodeStatusInfo + '</span></li>');
                                            }
                                            else {
                                                $("#nwk_" + k).append('<li style="height:26px;width:478px;padding-bottom:10px;">' +
                                                        '<span style="height:26px;float:left;">' +
                                                        '<button id="esen_' + children[n].ieeeAddress + '" class="nodeBtn">?</button><button class="linkButton" id="v_' + children[n].ieeeAddress + '">' + children[n].ieeeAddress + '</button></span>' +
                                                        '<span style="height:26px;float:right;">' + nodeStatusInfo + '</span></li>');
                                            }
                                        }
                                    }
                                    coord.children = children;
                                    GCoordinators.push(coord);
                                }
                                DrawAllNodesOnMap();
                            }
                        }
                        else {
                            $('#sideView').empty().html('Oops... there was error.');
                        }
                    }
                    else {
                        $('#sideView').empty().html('Oops... no response.');
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log('GetNodeList error: ' + errorThrown);
                }
            });
        }

        function GetNetworkStatus(ieeeAddress) {
            var DTO = { 'ieeeAddress': ieeeAddress };
            $.ajax({
                type: "POST",
                url: 'http://www.khusrosaleem.com/Echidna/Service.asmx/GetNetworkStatus',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify(DTO),
                success: function (data) {
                    if (data != null) {
                        var resp = data.d;
                        var nodeData = resp.listOfItems;
                        var error = resp.error;

                        $('#BatteryDiv').empty().html("Loading information for this node.");
                        if (error == "") {
                            if (nodeData.length == 0) {
                                $('#BatteryDiv').empty().html('There is no information for this node.');
                            }
                            else {
                                var dates = new Array();
                                var batteryVoltage = new Array();
                                var temperature = new Array();
                                for (var k = 0; k < nodeData.length; k++) {
                                    batteryVoltage.push(nodeData[k].batteryVoltage);
                                    temperature.push(nodeData[k].temperature);
                                    dates.push(nodeData[k].timestamp);
                                }                                
                                if (nodeData[nodeData.length-1].isNetworkActive) { $('#networkStopBtn').show(); $('#networkStartBtn').hide(); }
                                else { $('#networkStopBtn').hide(); $('#networkStartBtn').show(); }
                                $('#BatteryDiv').empty();
                            }
                        }
                        else {
                            $('#BatteryDiv').empty().html('Oops... there was error.');
                        }
                    }
                    else {
                        $('#BatteryDiv').empty().html('There is no data for this node.');
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log('GetNodeStatus error: ' + errorThrown);
                }
            });
        }
       
        function GetNodeStatus(ieeeAddress) {
            var DTO = { 'ieeeAddress': ieeeAddress };
            $.ajax({
                type: "POST",
                url: 'http://www.khusrosaleem.com/Echidna/Service.asmx/GetNodeStatus',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify(DTO),
                success: function (data) {
                    if (data != null) {
                        var resp = data.d;
                        var nodeData = resp.listOfItems;
                        var error = resp.error;

                        $('#BatteryDiv').empty('Loading battery info...');
                        if (error == "") {
                            if (nodeData.length == 0) {
                                $('#BatteryDiv').empty().html('There is no data for this node.');
                            }
                            else {
                                var dates = new Array();
                                var batteryVoltage = new Array();
                                var temperature = new Array();
                                for (var k = 0; k < nodeData.length; k++) {
                                    batteryVoltage.push(nodeData[k].batteryVoltage);
                                    temperature.push(nodeData[k].temperature);
                                    dates.push(nodeData[k].timestamp);
                                }
                                PlotStatusData(dates, batteryVoltage, temperature);
                            }
                        }
                        else {
                            $('#BatteryDiv').empty().html('Oops... there was error.');
                        }
                    }
                    else {
                        $('#BatteryDiv').empty().html('There is no data for this node.');
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log('GetNodeStatus error: ' + errorThrown);
                }
            });
        }

        function UpdateNetworkLocation(lat, lng, ieeeAddress) {

            // Save to server
            var DTO = { 'lat': lat, 'lng': lng, 'ieeeAddress': ieeeAddress };
            $.ajax({
                type: "POST",
                url: 'http://www.khusrosaleem.com/Echidna/Service.asmx/UpdateNetworkLocation',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify(DTO),
                success: function (data) {
                    if (data.d.error == "") {
                        for (var k = 0; k < GCoordinators.length; k++) {
                            if (GCoordinators[k].ieeeAddress == data.d.nwk.ieeeAddress) {
                                GCoordinators[k].lat = data.d.nwk.lat;
                                GCoordinators[k].lng = data.d.nwk.lng;
                                console.log('UpdateNetworkLocation: Your new coords are: ' + data.d.nwk.lat + ', ' + data.d.nwk.lng);
                                break;
                            }
                        }
                    }
                    else {
                        console.log("Could not update Coord location.");
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log('UpdateNetworkLocation error: ' + errorThrown);
                }
            });
        }

        function UpdateNodeLocation(lat, lng, ieeeAddress) {
            // Save to server
            var DTO = { 'lat': lat, 'lng': lng, 'ieeeAddress': ieeeAddress };
            //console.log(DTO);
            $.ajax({
                type: "POST",
                url: 'http://www.khusrosaleem.com/Echidna/Service.asmx/UpdateNodeLocation',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify(DTO),
                success: function (data) {
                    if (data.d.error == "") {
                        for (var k = 0; k < GCoordinators.length; k++) {
                            var coord = GCoordinators[k];
                            for (var n = 0; n < coord.children.length; n++) {
                                var child = coord.children[n];
                                if (child.ieeeAddress == data.d.node.ieeeAddress) {
                                    GCoordinators[k].children[n].lat = data.d.node.lat;
                                    GCoordinators[k].children[n].lng = data.d.node.lng;
                                    console.log("Node location updated");
                                }
                            }
                        }
                    }
                    else {
                        console.log("Could not update node location.");
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log('UpdateNodeLocation error: ' + errorThrown);
                }
            });         
        }

        function SetNetworkLocation(lat, lng, ieeeAddress) {
            // Save to server
            var DTO = { 'lat': lat, 'lng': lng, 'ieeeAddress': ieeeAddress };
            var URL = 'http://www.khusrosaleem.com/Echidna/Service.asmx/UpdateNetworkLocation';
            //console.log(DTO);
            $.ajax({
                type: "POST",
                url: URL,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify(DTO),
                success: function (data) {
                    if (data.d.error == "") {
                        for (var k = 0; k < GCoordinators.length; k++) {
                            if (GCoordinators[k].ieeeAddress == data.d.nwk.ieeeAddress) {

                                var marker = new google.maps.Marker({
                                    map: GMap,
                                    position: new google.maps.LatLng(data.d.nwk.lat, data.d.nwk.lng),
                                    title: data.d.nwk.ieeeAddress,
                                    animation: google.maps.Animation.DROP,
                                    icon: '../resources/images/map_markers/' + GCoordinators[k].markerColor + '_MarkerC.png',
                                    draggable: true
                                });

                                GCoordinators[k].marker = marker;
                                GCoordinators[k].lat = data.d.nwk.lat;
                                GCoordinators[k].lng = data.d.nwk.lng;
                                attachListenersToCoord(marker, marker.getTitle());
                            }
                        }
                    }
                    else {
                        alert("Could not update node location.");
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log('UpdateNodeLocation error: ' + errorThrown);
                }
            });   
        }

        function SetNodeLocation(lat, lng, ieeeAddress) {
            // Save to server
            var DTO = { 'lat': lat, 'lng': lng, 'ieeeAddress': ieeeAddress };
            var URL = 'http://www.khusrosaleem.com/Echidna/Service.asmx/UpdateNodeLocation';
            $.ajax({
                type: "POST",
                url: URL,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify(DTO),
                success: function (data) {
                    if (data.d.error == "") {
                        for (var k = 0; k < GCoordinators.length; k++) {
                            var coord = GCoordinators[k];
                            for (var n = 0; n < coord.children.length; n++) {
                                var child = coord.children[n];
                                if (child.ieeeAddress == data.d.node.ieeeAddress) {
                                    var marker;
                                    if (child.nodeType == "shule_ec20") {                                        
                                        marker = new google.maps.Marker({
                                            map: GMap,
                                            position: new google.maps.LatLng(data.d.node.lat, data.d.node.lng),
                                            title: data.d.node.ieeeAddress,
                                            animation: google.maps.Animation.DROP,
                                            icon: '../resources/images/map_markers/' + GCoordinators[k].markerColor + '_MarkerE.png',
                                            draggable: true
                                        });
                                    }
                                    else if (child.nodeType == "shule_flow") {
                                        marker = new google.maps.Marker({
                                            map: GMap,
                                            position: new google.maps.LatLng(data.d.node.lat, data.d.node.lng),
                                            title: data.d.node.ieeeAddress,
                                            animation: google.maps.Animation.DROP,
                                            icon: '../resources/images/map_markers/' + GCoordinators[k].markerColor + '_MarkerF.png',
                                            draggable: true
                                        });
                                    }
                                    else {
                                        marker = new google.maps.Marker({
                                            map: GMap,
                                            position: new google.maps.LatLng(data.d.node.lat, data.d.node.lng),
                                            title: data.d.node.ieeeAddress,
                                            animation: google.maps.Animation.DROP,
                                            icon: '../resources/images/map_markers/' + GCoordinators[k].markerColor + '_Marker.png',
                                            draggable: true
                                        });
                                    }


                                    GCoordinators[k].children[n].marker = marker;
                                    GCoordinators[k].children[n].lat = data.d.node.lat;
                                    GCoordinators[k].children[n].lng = data.d.node.lng;
                                    attachListenersToNode(marker, marker.getTitle());                                                                
                                }
                            }
                        }
                    }
                    else {
                        alert("Could not update node location.");
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log('UpdateNodeLocation error: ' + errorThrown);
                }
            });        
        }

        function DeleteNodeLocation(lat, lng, ieeeAddress) {
            // Save to server
            var DTO = { 'lat': lat, 'lng': lng, 'ieeeAddress': ieeeAddress };
            $.ajax({
                type: "POST",
                url: 'http://www.khusrosaleem.com/Echidna/Service.asmx/UpdateNodeLocation',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify(DTO),
                success: function (data) {

                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log('UpdateNodeLocation error: ' + errorThrown);
                }
            });
        }

        function DeleteNetworkLocation(lat, lng, ieeeAddress) {
            // Save to server
            var DTO = { 'lat': lat, 'lng': lng, 'ieeeAddress': ieeeAddress };
            $.ajax({
                type: "POST",
                url: 'http://www.khusrosaleem.com/Echidna/Service.asmx/UpdateNetworkLocation',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify(DTO),
                success: function (data) {

                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log('UpdateNodeLocation error: ' + errorThrown);
                }
            });
        }

        function DrawAllNodesOnMap() {
            for (var k = 0; k < GCoordinators.length; k++) {
                var lat = GCoordinators[k].lat;
                var lng = GCoordinators[k].lng;
                if ((lat != -9999) && (lng != -9999)) {
                    
                    var marker = new google.maps.Marker({
                        map: GMap,
                        position: new google.maps.LatLng(lat, lng),
                        title: GCoordinators[k].ieeeAddress,
                        animation: google.maps.Animation.DROP,
                        icon: '../resources/images/map_markers/' + GCoordinators[k].markerColor + '_MarkerC.png',
                        draggable: true
                    });
                    GCoordinators[k].marker = marker;

                    attachListenersToCoord(GCoordinators[k].marker, marker.getTitle());
                }

                var children = GCoordinators[k].children;
                
                for (var n = 0; n < children.length; n++) {
                    var lat = children[n].lat;
                    var lng = children[n].lng;
                    if ((lat != -9999) && (lng != -9999)) {
                        var marker;
                        if (children[n].nodeType == "shule_ec20") {
                            marker = new google.maps.Marker({
                                map: GMap,
                                position: new google.maps.LatLng(lat, lng),
                                title: children[n].ieeeAddress,
                                animation: google.maps.Animation.DROP,
                                icon: '../resources/images/map_markers/' + GCoordinators[k].markerColor + '_MarkerE.png',
                                draggable: true
                            });
                        }
                        else if (children[n].nodeType == "shule_flow") {
                            marker = new google.maps.Marker({
                                map: GMap,
                                position: new google.maps.LatLng(lat, lng),
                                title: children[n].ieeeAddress,
                                animation: google.maps.Animation.DROP,
                                icon: '../resources/images/map_markers/' + GCoordinators[k].markerColor + '_MarkerF.png',
                                draggable: true
                            });
                        }
                        else {
                            marker = new google.maps.Marker({
                                map: GMap,
                                position: new google.maps.LatLng(lat, lng),
                                title: children[n].ieeeAddress,
                                animation: google.maps.Animation.DROP,
                                icon: '../resources/images/map_markers/' + GCoordinators[k].markerColor + '_Marker.png',
                                draggable: true
                            });
                        }
                        GCoordinators[k].children[n].marker = marker;

                        attachListenersToNode(GCoordinators[k].children[n].marker, marker.getTitle());
                    }
                }                            
            }           
        }

        function attachListenersToCoord(marker, title) {
            google.maps.event.addListener(marker, 'click', function () {
                //alert('You clicked on ' + marker.getTitle());
            });
            google.maps.event.addListener(marker, 'dragend', function (event) {
                for (var k = 0; k < GCoordinators.length; k++) {
                    if (GCoordinators[k].ieeeAddress == title) {
                        UpdateNetworkLocation(event.latLng.lat(), event.latLng.lng(), GCoordinators[k].ieeeAddress);
                    }
                }
            });
        }

        function attachListenersToNode(marker, title) {
            
            google.maps.event.addListener(marker, 'click', function () {
                //alert('You clicked on ' + marker.getTitle());
            });
            google.maps.event.addListener(marker, 'dragend', function (event) {
                for (var k = 0; k < GCoordinators.length; k++) {
                    for (var n = 0; n < GCoordinators[k].children.length; n++) {
                        if (GCoordinators[k].children[n].ieeeAddress == title) {
                            UpdateNodeLocation(event.latLng.lat(), event.latLng.lng(), GCoordinators[k].children[n].ieeeAddress);
                        }
                    }
                }
            });
        }

        function GetMarkerColors() {
            $.ajax({
                type: "POST",
                url: 'http://www.khusrosaleem.com/Echidna/Service.asmx/GetMarkerColors',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    GMarkerColors = data.d.colors;
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log('GetMarkerColors error: ' + errorThrown);
                }
            });
        }

        $(document).ready(function () {

            GMyUserId = $('#hfUserId').val();
            GMyUserEmail = $('#hfUserEmail').val();
            GHomeLat = $('#hfHomeLat').val();
            GHomeLng = $('#hfHomeLng').val();

            initializeMap();
            GetNodeListAndDrawMarkers();
            GetMarkerColors();

            $("#sideView").on("click", "button", function (event) {
                var m = $(this).attr("id").split('_')[0];
                var addr = $(this).attr("id").split('_')[1];
                if (m == 'refreshNwkBtn') {
                    return false;
                }
                if (m == 'contact') {
                    $('#QueryDialog').dialog('open');
                }
                else if (m == 'ec') {

                    $('#propertiesViewTitle').html('Network properties');
                    $('#hfPropertiesViewAddress').html(addr);
                    $('#propertiesViewAddr').html(addr);
                    $('#ViewMeasDiv').hide(); $('#ViewNetworkMeasDiv').hide(); $('#colordiv').show(); $('#ColorSelect').empty();
                    $('#BatteryDiv').empty().html('Loading information for this node...');

                    

                    $('#propertiesView').fadeIn('fast', function () {
                        var ieeeAddress = addr;     
                        var idx = -1;
                        for (var k = 0; k < GCoordinators.length; k++) {
                            if (GCoordinators[k].ieeeAddress == ieeeAddress) {
                                idx = k;
                            }
                        }
                        if (idx == -1) { alert('An unknown error has occured, referent 0402.') }
                        else {
                            $('#propertiesViewDescription').html(GCoordinators[idx].description);
                            for (var k = 0; k < GMarkerColors.length; k++) {
                                if (GMarkerColors[k].color == GCoordinators[idx].markerColor) {
                                    $('#ColorSelect').append('<input type="radio" checked name="colorRadio" value="' + GMarkerColors[k].color + '"><span style="width:30px;height:20px;background-color:' + GMarkerColors[k].code + ';padding:0 8px 0 8px;margin-right:5px;border-radius:2px;"></span></input>');
                                }
                                else {
                                    $('#ColorSelect').append('<input type="radio" name="colorRadio" value="' + GMarkerColors[k].color + '"><span style="width:30px;height:20px;background-color:' + GMarkerColors[k].code + ';padding:0 8px 0 8px;margin-right:5px;border-radius:2px;"></span></input>');
                                }
                            }
                            GetNetworkStatus(ieeeAddress);
                            if ((GCoordinators[idx].lat != -9999) && (GCoordinators[idx].lng != -9999)) {
                                GMap.panTo(new google.maps.LatLng(GCoordinators[idx].lat, GCoordinators[idx].lng));
                            }
                        }
                    });
                }
                else if (m == 'esen') {
                    var myCoordinatorAddress = null;
                    $('#networkStartBtn').hide();
                    $('#networkStopBtn').hide();
                    $('#propertiesViewTitle').html('Sensor properties');
                    $('#hfPropertiesViewAddress').html(addr);
                    $('#hfCoordinatorAddress').html('');
                    $('#propertiesViewAddr').html(addr);
                    $('#ViewNetworkMeasDiv').hide(); $('#ViewMeasDiv').show(); $('#colordiv').hide(); $('#ColorSelect').empty();
                    $('#BatteryDiv').empty().html('Loading battery info...');
                    $('#AutomationBtn').hide();
                    $('#propertiesView').fadeIn('fast', function () {

                        var ieeeAddress = addr;
                        var child = null;
                        for (var k = 0; k < GCoordinators.length; k++) {
                            for (var n = 0; n < GCoordinators[k].children.length; n++) {
                                if (GCoordinators[k].children[n].ieeeAddress == ieeeAddress) {
                                    child = GCoordinators[k].children[n];
                                    myCoordinatorAddress = GCoordinators[k].ieeeAddress;
                                    $('#hfCoordinatorAddress').html(myCoordinatorAddress);
                                }
                            }
                        }
                        if (child == null) {
                            alert('An unknown error has occured, reference 0544.')
                        }
                        else {
                            $('#propertiesViewDescription').html(child.description);
                            GetNodeStatus(ieeeAddress);
                            if ((child.lat != -9999) && (child.lng != -9999)) {
                                GMap.panTo(new google.maps.LatLng(child.lat, child.lng));
                            }
                        }
                        //$('#DataDialog').data('myData', { myAddress: $('#hfPropertiesViewAddress').html(), coordAddress: myCoordinatorAddress }).dialog('open');
                    });
                    
                }
                else if (m == 'eact') {
                    $('#networkStartBtn').hide();
                    $('#networkStopBtn').hide();
                    $('#propertiesViewTitle').html('Actuator properties');
                    $('#hfPropertiesViewAddress').html(addr);
                    $('#hfCoordinatorAddress').html('');
                    $('#propertiesViewAddr').html(addr);
                    $('#ViewNetworkMeasDiv').hide(); $('#ViewMeasDiv').show(); $('#colordiv').hide(); $('#ColorSelect').empty();
                    $('#BatteryDiv').empty().html('Loading battery info...');
                    $('#AutomationBtn').show();
                    $('#propertiesView').fadeIn('fast', function () {

                        var ieeeAddress = addr;
                        var child = null;
                        for (var k = 0; k < GCoordinators.length; k++) {
                            for (var n = 0; n < GCoordinators[k].children.length; n++) {
                                if (GCoordinators[k].children[n].ieeeAddress == ieeeAddress) {
                                    child = GCoordinators[k].children[n];
                                    $('#hfCoordinatorAddress').html(GCoordinators[k].ieeeAddress);
                                }
                            }
                        }
                        if (child == null) {
                            alert('An uknown error has occured, reference 3442.')
                        }
                        else {
                            $('#propertiesViewDescription').html(child.description);
                            GetNodeStatus(ieeeAddress);
                            if ((child.lat != -9999) && (child.lng != -9999)) {
                                GMap.panTo(new google.maps.LatLng(child.lat, child.lng));
                            }
                        }
                    });
                }
                else if (m == 'vnc') {
                    for (var k = 0; k < GCoordinators.length; k++) {
                        if ((GCoordinators[k].ieeeAddress == addr) && (GCoordinators[k].marker != null)) {
                            GCoordinators[k].marker.setVisible(true);
                            for (var n = 0; n < GCoordinators[k].children.length; n++) {
                                if (GCoordinators[k].children[n].marker != null) {
                                    GCoordinators[k].children[n].marker.setVisible(true);
                                }
                            }
                        }
                    }
                }
                else if (m == 'hnc') {
                    for (var k = 0; k < GCoordinators.length; k++) {
                        if ((GCoordinators[k].ieeeAddress == addr) && (GCoordinators[k].marker != null)) {
                            GCoordinators[k].marker.setVisible(false);
                            for (var n = 0; n < GCoordinators[k].children.length; n++) {
                                if (GCoordinators[k].children[n].marker != null) {
                                    GCoordinators[k].children[n].marker.setVisible(false);
                                }
                            }
                        }
                    }
                }
                else if (m == 'v') {
                    for (var k = 0; k < GCoordinators.length; k++) {
                        var coor = GCoordinators[k];
                        for (var n = 0; n < coor.children.length; n++) {
                            var child = coor.children[n];
                            if ((child.ieeeAddress == addr) && (child.lat != -9999) && (child.lng != -9999)) {
                                GMap.panTo(new google.maps.LatLng(child.lat, child.lng));
                                return false;
                            }
                        }
                    }
                    alert("You have not positioned this sensor/actuator node on the map.");
                }
                else if (m == 'vc') {
                    for (var k = 0; k < GCoordinators.length; k++) {
                        if (addr == GCoordinators[k].ieeeAddress) {
                            if ((GCoordinators[k].lat != -9999) && (GCoordinators[k].lng != -9999)) {
                                GMap.panTo(new google.maps.LatLng(GCoordinators[k].lat, GCoordinators[k].lng));
                                return false;
                            }
                        }
                    }
                    alert("You have not positioned this coordinator node on the map.");
                }
                return false;
            });

            $('#DataDialog').dialog({
                autoOpen: false,
                modal: true,
                title: 'Sensor data and settings',
                width: 1100,
                height: 650,
                resizable: false,
                closeOnEscape: false,
                open: function (event, ui) {
                    var dialogData = $(this).data('myData');
                    $('#nodeAddress').html(dialogData.myAddress + ', ' + dialogData.coordAddress);
                    $('#DataDialogTabs').tabs();
                },
                close: function (event, ui) {

                }
            });

            $("#IrrigationDialog").dialog({
                autoOpen: false,
                modal: true,
                title: "Automation - Irrigation scheduling",
                width: 1000,
                height: 600,
                resizable: false,
                closeOnEscape: false,
                open: function (event, ui) {
                    $('#irrigationDialogCurrentValveState').html('');
                    $('#openValveBtn').hide();
                    $('#closeValveBtn').hide();

                    $('#irrigationDialogNodeAddress').html($(this).data('ieeeAddress'));

                    var DTO = { 'ieeeAddress': $(this).data('ieeeAddress') };
                    $.ajax({
                        type: "POST",
                        url: 'http://www.khusrosaleem.com/Echidna/Service.asmx/IsValveOpen',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        data: JSON.stringify(DTO),
                        success: function (data) {
                            if (data.d.error != "") {
                                alert("An unknown error has occured, reference 8827.");
                            }
                            else {
                                if (data.d.isOpen) {
                                    $('#irrigationDialogCurrentValveState').html('The valve is currently OPEN');
                                    $('#openValveBtn').hide();
                                    $('#closeValveBtn').show();
                                }
                                else {
                                    $('#irrigationDialogCurrentValveState').html('The valve is currently CLOSED');
                                    $('#openValveBtn').show();
                                    $('#closeValveBtn').hide();
                                }
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            console.log('IsValveOpen error: ' + errorThrown);
                        }
                    });

                    /*
                    for (var c = 0; c < GNetworks.length; c++) {
                        var children = GNetworks[c].nodes;
                        for (n = 0; n < children.length; n++) {
                            $('#dynamicSensorSelect').append('<option>' + children[n].ieeeAddress + '</option>');
                        }
                    }
                    */
                },
                close: function (event, ui) {

                }
            });



            $('#refreshValveState').click(function () {
                var DTO = { 'ieeeAddress': $('#irrigationDialogNodeAddress').html() };
                $.ajax({
                    type: "POST",
                    url: 'http://www.khusrosaleem.com/Echidna/Service.asmx/IsValveOpen',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify(DTO),
                    success: function (data) {
                        if (data.d.error != "") {
                            alert("An unknown error has occured, reference 2894.");
                        }
                        else {
                            if (data.d.isOpen) {
                                $('#irrigationDialogCurrentValveState').html('The valve is currently OPEN');
                                $('#openValveBtn').hide();
                                $('#closeValveBtn').show();
                            }
                            else {
                                $('#irrigationDialogCurrentValveState').html('The valve is currently CLOSED');
                                $('#openValveBtn').show();
                                $('#closeValveBtn').hide();
                            }
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log('IsValveOpen error: ' + errorThrown);
                    }
                });
                return false;
            });

            $("#QueryDialog").dialog({
                autoOpen: false,
                modal: false,
                title: "Contact us",
                width: 480,
                height: 420,
                resizable: false,
                closeOnEscape: false,
                open: function (event, ui) {
                    $('#QuerySendWait').hide();
                    console.log($(this).data('id'));
                },
                close: function (event, ui) {
                }
            });
            $('#searchBtn').click(function () {
                var address = $('#searchTbox').val();
                if (address == '') return false;
                GGeocoder.geocode({ 'address': address }, function (geocodeResult, geocodeStatus) {
                    if (geocodeStatus == google.maps.GeocoderStatus.OK) {
                        GMap.setCenter(geocodeResult[0].geometry.location);
                        $('#searchTbox').val('');
                    } else {
                        alert('Could not find location. Is it spelt correctly?');
                    }
                });
                return false;
            });

            $('#closePropView').click(function () {
                $('#propertiesView').fadeOut();
                return false;
            });

            $('#propertiesViewSaveBtn').click(function () {
                var DTO = { 'description': $('#propertiesViewDescription').val(),
                    'markerColor': $('input[name=colorRadio]:checked').val(),
                    'ieeeAddress': $('#hfPropertiesViewAddress').html()
                };
                $.ajax({
                    type: "POST",
                    url: 'http://www.khusrosaleem.com/Echidna/Service.asmx/UpdateNetworkProperties',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify(DTO),
                    success: function (data) {
                        var resp = data.d;
                        for (var k = 0; k < GCoordinators.length; k++) {
                            if (GCoordinators[k].ieeeAddress == resp.nwk.ieeeAddress) {
                                GCoordinators[k].markerColor = resp.nwk.markerColor;
                                GCoordinators[k].description = resp.nwk.description;
                                GCoordinators[k].marker.setIcon('../resources/images/map_markers/' + GCoordinators[k].markerColor + '_MarkerC.png');
                                for (var n = 0; n < GCoordinators[k].children.length; n++) {
                                    if (GCoordinators[k].children[n].nodeType == "shule_ec20") {
                                        GCoordinators[k].children[n].marker.setIcon('../resources/images/map_markers/' + GCoordinators[k].markerColor + '_MarkerE.png');
                                    }
                                    else if (GCoordinators[k].children[n].nodeType == "shule_flow") {
                                        GCoordinators[k].children[n].marker.setIcon('../resources/images/map_markers/' + GCoordinators[k].markerColor + '_MarkerF.png');
                                    }
                                    else {
                                        GCoordinators[k].children[n].marker.setIcon('../resources/images/map_markers/' + GCoordinators[k].markerColor + '_Marker.png');
                                    }
                                    
                                }
                            }
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log('UpdateNetworkProperties error: ' + errorThrown);
                    }
                });
                return false;
            });

            $('#addToMapBtn').click(function () {
                var addr = $('#hfPropertiesViewAddress').html();
                for (var k = 0; k < GCoordinators.length; k++) {
                    if (addr == GCoordinators[k].ieeeAddress) {
                        console.log("addr: " + addr + ", .ieeeAddr: " + GCoordinators[k].ieeeAddress);
                        if (GCoordinators[k].marker == null) {
                            SetNetworkLocation(GMap.getCenter().lat() + 0.0001 * Math.random(),
                                                    GMap.getCenter().lng() + 0.0001 * Math.random(),
                                                    addr);
                            return false;
                        }
                        else {
                            alert('This coordinator is already on the map');
                            GMap.panTo(new google.maps.LatLng(GCoordinators[k].lat, GCoordinators[k].lng));
                        }
                        return false;
                    }
                    var coor = GCoordinators[k];
                    for (var n = 0; n < coor.children.length; n++) {
                        var child = coor.children[n];
                        if ((addr == child.ieeeAddress) && (child.marker == null)) {
                            SetNodeLocation(GMap.getCenter().lat() + 0.0001 * Math.random(),
                                                    GMap.getCenter().lng() + 0.0001 * Math.random(),
                                                    addr);
                            return false;
                        }
                        else if ((addr == child.ieeeAddress) && (child.marker != null)) {
                            alert('This sensor/actuator node is already on the map');
                            GMap.panTo(new google.maps.LatLng(child.lat, child.lng));
                            return false;
                        }
                    }
                }
                return false;
            });


            $('#remFromMapBtn').click(function () {
                var addr = $('#hfPropertiesViewAddress').html();
                for (var k = 0; k < GCoordinators.length; k++) {
                    if ((addr == GCoordinators[k].ieeeAddress) && (GCoordinators[k].marker != null)) {
                        GCoordinators[k].marker.setMap(null);
                        GCoordinators[k].marker = null;
                        GCoordinators[k].lat = -9999;
                        GCoordinators[k].lng = -9999;
                        DeleteNetworkLocation(-9999, -9999, addr);
                        //$('#ac_' + addr).show();
                        //$('#rc9_' + addr).hide();
                        return false;
                    }
                    var coor = GCoordinators[k];
                    for (var n = 0; n < coor.children.length; n++) {
                        var child = coor.children[n];
                        if ((addr == child.ieeeAddress) && (child.marker != null)) {
                            GCoordinators[k].children[n].marker.setMap(null);
                            GCoordinators[k].children[n].marker = null;
                            GCoordinators[k].children[n].lat = -9999;
                            GCoordinators[k].children[n].lng = -9999;
                            DeleteNodeLocation(-9999, -9999, addr);
                            //$('#a_' + addr).show();
                            //$('#r_' + addr).hide();
                        }
                    }
                }
                return false;
            });


            $('#ViewNetworkMeasBtn').click(function () {
                $('#measPlotDiv').empty();
                $('#measView').show();
                var DTO = { 'coordIeeeAddress': $('#hfPropertiesViewAddress').html(),
                    'startDate': "",
                    'endDate': ""
                };

                $.ajax({
                    type: "POST",
                    url: 'http://www.khusrosaleem.com/Echidna/Service.asmx/GetNetworkMeasurementList',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify(DTO),
                    success: function (data) {
                        var resp = data.d.listOfMeas;
                        $('#measPlotDiv').empty();
                        for (var n = 0; n < resp.length; n++) {
                            var soilMoisture = new Array();
                            for (var m = 0; m < resp[n].length; m++) {
                                //soilMoisture.push(JSON.parse(resp[n][m].data).soilMoisture);
                                soilMoisture.push(JSON.parse(resp[n][m].data));
                            }
                            var container = $('<div style="margin-bottom:70px;"></div>').appendTo($('#measPlotDiv'));
                            //PlotMeasurementData(container, "", soilMoisture, "");
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log('GetNetworkMeasurementList error: ' + errorThrown);
                    }
                });

                console.log($('#hfPropertiesViewAddress').html());
                return false;
            });


            $('#ViewMeasBtn').click(function () {
                $('#measPlotDiv').empty();
                $('#measView').show();
                var DTO = { 'ieeeAddress': $('#hfPropertiesViewAddress').html(),
                    'startDate': "",
                    'endDate': ""
                };
                $.ajax({
                    type: "POST",
                    url: 'http://www.khusrosaleem.com/Echidna/Service.asmx/GetMeasurementList',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify(DTO),
                    success: function (data) {

                        var resp = data.d;
                        var measurements = new Array();
                        for (var k = 0; k < resp.listOfMeas.length; k++) {
                            //soilMoisture.push(JSON.parse(resp.listOfMeas[k].data).soilMoisture);
                            //measurements.push(resp.listOfMeas[k].data);
                            measurements.push(resp.listOfMeas[k]);
                        }
                        var container = $('#measPlotDiv');
                        if (resp.measType == "soil_moisture") {
                            PlotMeasurementData(container, measurements, "Soil moisture");                            
                        }
                        else if (resp.measType == "flow") {
                            PlotMeasurementData(container, measurements, "Flow");
                        }
                        else {
                            alert("Unknown measurement type, cannot plot data");
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log('UpdateNetworkProperties error: ' + errorThrown);
                    }
                });

                //$('#TimeSeriesDialog').data('id', $('#hfPropertiesViewAddress').html()).dialog('open');
                return false;
            });
            $('#measViewClose').click(function () {
                $('#measView').fadeOut();
                return false;
            });
            $('#contactBtn').click(function () {
                $('#QueryDialog').dialog('open');
                return false;
            });
            $('#QuerySendBtn').click(function () {
                $('#QuerySendWait').show();
                var DTO = { 'email': $('#queryEmail').val(),
                    'name': $('#queryName').val(),
                    'query': $('#queryQuery').val()
                };
                $.ajax({
                    type: "POST",
                    url: 'http://www.khusrosaleem.com/Echidna/Service.asmx/HelpQuery',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify(DTO),
                    success: function (data) {
                        if (data.d == "ok") {
                            alert('Your query has been received. We will get back to you within 24 hours.');
                            $('#QueryDialog').dialog('close');
                        }
                        else {
                            alert('Error: ' + data.d);
                        }
                        $('#QuerySendWait').hide();
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        $('#QuerySendWait').hide();
                        console.log('HelpQuery error: ' + errorThrown);
                    }
                });
                return false;
            });

            $('#networkStopBtn').click(function () {
                var DTO = {
                    'coordIeeeAddress': $('#hfPropertiesViewAddress').html()
                };
                $.ajax({
                    type: "POST",
                    url: 'http://www.khusrosaleem.com/Echidna/Service.asmx/StopNetwork',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify(DTO),
                    success: function (data) {
                        alert(data.d);
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log('StopNetwork error: ' + errorThrown);
                    }
                });
                return false;
            });

            $('#networkStartBtn').click(function () {
                var DTO = {
                    'coordIeeeAddress': $('#hfPropertiesViewAddress').html()
                };
                $.ajax({
                    type: "POST",
                    url: 'http://www.khusrosaleem.com/Echidna/Service.asmx/StartNetwork',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify(DTO),
                    success: function (data) {
                        alert(data.d);
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log('StartNetwork error: ' + errorThrown);
                    }
                });
                return false;
            });

            $('#openValveBtn').click(function () {
                var DTO = {
                    'coordIeeeAddress': $('#hfCoordinatorAddress').html(),
                    'nodeIeeeAddress': $('#hfPropertiesViewAddress').html()
                };
                $.ajax({
                    type: "POST",
                    url: 'http://www.khusrosaleem.com/Echidna/Service.asmx/OpenValve',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify(DTO),
                    success: function (data) {
                        if (data.d.error == "") {
                            alert('Open valve command has been send, it may take a while to happen, depending on the network state');
                        }
                        else {
                            alert('Your open valve request was unsuccessful, please try again');
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log('OpenValve error: ' + errorThrown);
                    }
                });
                return false;
            });

            $('#closeValveBtn').click(function () {
                var DTO = {
                    'coordIeeeAddress': $('#hfCoordinatorAddress').html(),
                    'nodeIeeeAddress': $('#hfPropertiesViewAddress').html()
                };
                $.ajax({
                    type: "POST",
                    url: 'http://www.khusrosaleem.com/Echidna/Service.asmx/CloseValve',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify(DTO),
                    success: function (data) {
                        if (data.d.error == "") {
                            alert('Close valve command has been send, it may take a while to happen, depending on the network state');
                        }
                        else {
                            alert('Your close valve request was unsuccessful, please try again');
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log('CloseValve error: ' + errorThrown);
                    }
                });
                return false;
            });

            $('#AutomationBtn').click(function () {
                $('#localDate').datepicker({ dateFormat: "yy-mm-dd" });
                $('#fixedStartDate').datepicker({ dateFormat: "yy-mm-dd" });
                $('#fixedEndDate').datepicker({ dateFormat: "yy-mm-dd" });
                var ieeeAddress = $('#hfPropertiesViewAddress').html();
                $('#IrrigationDialog').data('ieeeAddress', ieeeAddress).dialog('open');
                return false;
            });

            $('#manIrriStatusBtn').click(function () {
                var startDate = $('#fixedStartDate').datepicker("getDate");
                var endDate = $('#fixedEndDate').datepicker("getDate");
                var startDatetime = new Date(startDate.getFullYear(), startDate.getMonth(),
                        startDate.getDate(), document.getElementById("fixedStartHour").selectedIndex,
                        document.getElementById("fixedStartMinute").selectedIndex, 0, 0);
                var endDatetime = new Date(endDate.getFullYear(), endDate.getMonth(),
                        endDate.getDate(), document.getElementById("fixedEndHour").selectedIndex,
                        document.getElementById("fixedEndMinute").selectedIndex, 0, 0);
                console.log(startDatetime);
                console.log(endDatetime);
                if (endDatetime <= startDatetime) {
                    alert('end must be greater than start');
                    return;
                }
                /*
                var DTO = { 'localDatetime': $('#queryEmail').val(),
                'startDatetime': $('#queryName').val(),
                'endDatetime': ,
                'repeat': $('#queryQuery').val(),
                'localMonth': ,
                'localYear': ,
                'startMinute
                };
                */
                var DTO = {};
                $.ajax({
                    type: "POST",
                    url: 'http://www.khusrosaleem.com/Echidna/Service.asmx/HelpQuery',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify(DTO),
                    success: function (data) {
                        if (data.d == "ok") {
                            alert('Your query has been received. We will get back to you within 24 hours.');
                            $('#QueryDialog').dialog('close');
                        }
                        else {
                            alert('Error: ' + data.d);
                        }
                        $('#QuerySendWait').hide();
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        $('#QuerySendWait').hide();
                        console.log('HelpQuery error: ' + errorThrown);
                    }
                });

            });
        });
    </script>    
</head>
<body>
    
    <form id="form1" runat="server">
    <asp:HiddenField ID="hfUserId" runat="server" />
    <asp:HiddenField ID="hfUserEmail" runat="server" />
    <asp:HiddenField ID="hfHomeLat" runat="server" />
    <asp:HiddenField ID="hfHomeLng" runat="server" />
    <asp:HiddenField ID="hfErrorMessageFromServer" runat="server" />
    <div>
        <div id="DataDialog" style="display:none;">
            <p>Node address <span id="nodeAddress"></span></p>
            <div id="DataDialogTabs">
              <ul>
                <li><a href="#DataDialogTabs1">Sensor data</a></li>
                <li><a href="#DataDialogTabs2">Settings</a></li>
              </ul>
              <div id="DataDialogTabs1">
                <p>Proin elit arcu, rutrum commodo, vehicula tempus, commodo a, risus. Curabitur nec arcu. Donec sollicitudin mi sit amet mauris. Nam elementum quam ullamcorper ante. Etiam aliquet massa et lorem. Mauris dapibus lacus auctor risus. Aenean tempor ullamcorper leo. Vivamus sed magna quis ligula eleifend adipiscing. Duis orci. Aliquam sodales tortor vitae ipsum. Aliquam nulla. Duis aliquam molestie erat. Ut et mauris vel pede varius sollicitudin. Sed ut dolor nec orci tincidunt interdum. Phasellus ipsum. Nunc tristique tempus lectus.</p>
              </div>
              <div id="DataDialogTabs2">
                <p>Morbi tincidunt, dui sit amet facilisis feugiat, odio metus gravida ante, ut pharetra massa metus id nunc. Duis scelerisque molestie turpis. Sed fringilla, massa eget luctus malesuada, metus eros molestie lectus, ut tempus eros massa ut dolor. Aenean aliquet fringilla sem. Suspendisse sed ligula in ligula suscipit aliquam. Praesent in eros vestibulum mi adipiscing adipiscing. Morbi facilisis. Curabitur ornare consequat nunc. Aenean vel metus. Ut posuere viverra nulla. Aliquam erat volutpat. Pellentesque convallis. Maecenas feugiat, tellus pellentesque pretium posuere, felis lorem euismod felis, eu ornare leo nisi vel felis. Mauris consectetur tortor et purus.</p>
              </div>
            </div>
        </div>
        <div id="IrrigationDialog" style="display:none;">  
            <div style="padding:10px;">
                <div>
                    <p>The automation component is currently disabled. We will initially conduct soil moisture
                        data collection experiments to calibrate the automation algorithms.<br /> After this initial phase 
                        has been completed, you will be able to configure the system to automatically irrigate your fields.
                        <br />Thank you for your patience.
                    </p>
                    <p>In the meantime, you can manually control irrigation using these controls.</p>
                    <table>
                        <tr><td>Node Address</td><td><span id="irrigationDialogNodeAddress" style="padding-left:20px;"></span></td></tr>
                        <tr><td>Current valve state</td><td><span id="irrigationDialogCurrentValveState" style="padding-left:20px;padding-right:20px;"></span><button id="refreshValveState">Refresh</button></td></tr>
                        <tr><td colspan="2"></td></tr>
                        <tr><td colspan="2"><button id="openValveBtn">Open valve</button><button id="closeValveBtn">Close valve</button></td></tr>
                    </table>
                </div>
                <div style="display:none;">
                <div style="width:100%;height:30px;">
                    <div style="float:left;height:30px;">Dynamic irrigation</div>
                    <div style="float:right;height:30px;"><button id="dynIrriStatusBtn">START</button></div>
                </div>
                <div>
                    <input type="text" style="width: 0; height: 0; top: -100px; position: absolute;"/>
                    <p>Enter your current local time 
                        <select id="localHour">
                            <option>00</option><option>01</option><option>02</option><option>03</option>
                            <option>04</option><option>05</option><option>06</option><option>07</option>
                            <option>08</option><option>09</option><option>10</option><option>11</option>
                            <option>12</option><option>13</option><option>14</option><option>15</option>
                            <option>16</option><option>17</option><option>18</option><option>19</option>
                            <option>20</option><option>21</option><option>22</option><option>23</option>
                        </select>:
                        <select id="localMinute">
                            <option>00</option><option>01</option><option>02</option><option>03</option><option>04</option><option>05</option>
                            <option>06</option><option>07</option><option>08</option><option>09</option><option>10</option><option>11</option>
                            <option>12</option><option>13</option><option>14</option><option>15</option><option>16</option><option>17</option>
                            <option>18</option><option>19</option><option>20</option><option>21</option><option>22</option><option>23</option>
                            <option>24</option><option>25</option><option>26</option><option>27</option><option>28</option><option>29</option>
                            <option>30</option><option>31</option><option>32</option><option>33</option><option>34</option><option>35</option>
                            <option>36</option><option>37</option><option>38</option><option>39</option><option>40</option><option>41</option>
                            <option>42</option><option>43</option><option>44</option><option>45</option><option>46</option><option>47</option>
                            <option>48</option><option>49</option><option>50</option><option>51</option><option>52</option><option>53</option>
                            <option>54</option><option>55</option><option>56</option><option>57</option><option>58</option><option>59</option>
                        </select>
                        and date <input type="text" id="localDate" />
                    </p>
                </div>
                <div>
                    Select a sensor <select id="dynamicSensorSelect"></select>
                </div>
                </div>
            </div>          
        </div>
        <div id="QueryDialog" style="display:none;">
            <table>
                <tr><td>Your name</td></tr>
                <tr><td><input type="text" id="queryName" style="width:425px;" /></td></tr>
                <tr><td>Your email</td></tr>
                <tr><td><input type="text" id="queryEmail" style="width:425px;" /></td></tr>
                <tr><td>Query</td></tr>
                <tr><td><textarea id="queryQuery" rows="5" cols="45" style="resize:none;" ></textarea></td></tr>
                <tr><td><button id="QuerySendBtn" style="margin-right:20px;">Send</button><span id="QuerySendWait" style="display:none;">Please wait...</span></td></tr>
            </table>
        </div>
        <div id="header" style="display:block;height:72px;background-color:#F8F8F8;">
            <div id="headerLeft" style="height:72px;float:left;">
                <table>
                    <tr><td><img src="../resources/images/logoheader.png" alt=""/></td><td><span style="margin-left:20px;font-size:1.5em;">Echidna data portal</span></td></tr>
                </table>
            </div>
            <div id="headerRight" style="height:72px;float:right;">
                <table style="height:72px;"><tr>
                    <td><span style="margin-right:10px;"><asp:Label ID="UserLabel" runat="server" /></span></td>
                    <td><asp:Button ID="Signout" runat="server" Text="Sign out" CssClass="btn" OnClick="SignoutClick" /></td></tr>
                </table>
            </div>
        </div>
        <div id="content" style="display:block;height:570px;padding:20px 10px;background-color:#F8F8F8;position:relative;">
            <div id="mapView" style="position:absolute;top:10px;left:0;">       
                <div style="width:600px;height:50px;">
                    <input type="text" id="searchTbox" class="tbox" style="margin-left:2px;width:200px;margin-right:10px;padding:3px 2px;"/>
                    <button id="searchBtn" class="btn" style="width:90px;">Search</button>                     
                </div>
                <div id="mapCanvas" style="width:600px;height:500px;"></div>
                <div style="display:none;"><span id="latCoord"></span><span id="lngCoord" style="margin-left:20px;"></span></div>
            </div>
            <div id="measView" style="opacity:0.97;position:absolute;margin-top:50px;top:10px;left:0;height:500px;width:600px;display:none;z-index:100;background-color:White;">
                <div style="width:600px;height:30px;background-color:#99ccff;margin-bottom:5px;">
                    <div style="float:left;height:30px;">        
                        Measurements                    
                    </div>
                    <div style="float:right;height:30px;">
                        <button id="measViewClose">Close</button>
                    </div>
                </div>
                <div>
                    <div id="measPlotDiv" style="margin-top:20px;height:450px;overflow-x:hidden;overflow-y:hidden;"></div>
                </div>
            </div>        
            <div style="height:500px;margin-top:50px;position:absolute;top:10px;left:604px;width:515px;">
                <div id="sideView" style="width:495px;height:500px;padding-right:0px;position:absolute;top:0;left:0;overflow:hidden;">
                    Loading, please wait ...
                </div>
                <div id="propertiesView" style="background-color:white;opacity:0.95;height:500px;padding-right:0px;position:absolute;top:0;left:0;display:none;">
                    <span id="hfPropertiesViewAddress" style="display:none;"></span>
                    <span id="hfCoordinatorAddress" style="display:none;"></span>
                    <div style="width:495px;height:30px;background-color:#99ccff;margin-bottom:5px;">
                        <div style="float:left;height:30px;" id="propertiesViewTitle">                            
                        </div>
                        <div style="float:right;height:30px;">
                            <button id="propertiesViewSaveBtn" style="margin-right:5px;">Save changes</button><button id="closePropView">Close</button>
                        </div>
                    </div>
                    <div style="height:470px;width:478px;overflow-x:hidden;overflow-y:hidden;padding:5px;">
                        <div><span id="propertiesViewAddr" style="font-size:1.2em;"></span></div>                        
                        <div style="margin-bottom:20px;">                         
                            <br />Description <br />
                            <textarea id="propertiesViewDescription" rows="2" cols="45" style="resize:none;margin-bottom:20px;" ></textarea>
                            <button id="networkStartBtn" style="display:none;">Start network</button>
                            <button id="networkStopBtn" style="display:none;">Stop network</button>
                            <div id="colordiv" style="margin-top:20px;">
                            Color<br />
                            <span id="ColorSelect" ></span>
                            </div>
                        </div>

                        <div style="margin-bottom:20px;"><button id="addToMapBtn">Add to map</button><button id="remFromMapBtn">Remove from map</button></div>

                        <div id="BatteryDiv" style="width:380px;margin-left:10px;margin-bottom:10px;"></div>

                        <div id="ViewMeasDiv" style="margin-top:20px; display:none;">
                            <button id="ViewMeasBtn" class="btn" style="padding:5px;">View measurements</button>
                            <button id="AutomationBtn" class="btn" style="padding:5px; display:none;">Automation</button>
                        </div>
                        <div id="ViewNetworkMeasDiv" style="margin-top:20px; display:none;">
                            <button id="ViewNetworkMeasBtn" class="btn" style="padding:5px;">View all network measurements</button>
                        </div>  
                    </div>                    
                </div>
            </div>
        </div>   

    </div>
    </form>
    <script type="text/javascript">

        function PlotMeasurementData(div, measurements, title) {
            datapoints = new Array();
            timestamp = new Array();
            yaxisTitle = "";

            yaxis_max = 0;
            yaxis_min = 100000;

            for (var k = 0; k < measurements.length; k++) {
                if (measurements[k].data > yaxis_max) { yaxis_max = measurements[k].data; }
                if (measurements[k].data < yaxis_min) { yaxis_min = measurements[k].data; }
                datapoints.push([Date.parse(measurements[k].timestamp), Number(Math.round(measurements[k].data + 'e2') + 'e-2')]);
                timestamp.push(measurements[k].timestamp);
            }
            if (title == "Soil moisture") {
                yaxisTitle = "Soil moisture (rel)";
            }
            else if (title == "Flow") {
                yaxisTitle = "Flow (ON/OFF)";
            }
            else {
                alert("Unknown measurement type, cannot plot plot data."); return;
            }

            Highcharts.setOptions({               
                global: {
                    useUTC: false
                }
            });
            var myChart = new Highcharts.Chart({
                chart: {
                    renderTo: div[0],
                    type: 'spline',
                    height: 200,
                    backgroundColor: '#FCFFC5',
                    borderRadius: 0
                },
                title: {
                    text: null
                },
                xAxis: {
                    type: 'datetime',
                    dateTimeLabelFormats: { // don't display the dummy year
                        day: "%A, %b %e, %Y",
                        month: '%e. %b',
                        year: '%b'
                    },
                    title: {
                        text: 'Date'
                    }
                },
                plotOptions: {
                    series: {
                        marker: {
                            enabled: false
                        }
                    }
                },
                yAxis: {
                    lineWidth: 1,
                    showLastLabel: true,
                    gridLineWidth: 0,
                    title: {
                        text: yaxisTitle
                    },
                    plotLines: [{
                        value: 0,
                        width: 1,
                        color: '#808080'
                    }],
                    min: yaxis_min,
                    max: yaxis_max
                },
                legend: { enabled: false },
                series: [{
                    data: datapoints
                }]
            });               
        }
        
        function PlotStatusData(_dates, _voltage, _temperature) {

            datapoints = new Array();
            yaxisTitle = "";

            yaxis_max = 0;
            yaxis_min = 100000;

            for (var k = _voltage.length - 1; k >=0 ; k--) {
                if (_voltage[k] > yaxis_max) { yaxis_max = _voltage[k]; }
                if (_voltage[k] < yaxis_min) { yaxis_min = _voltage[k]; }
                datapoints.push([Date.parse(_dates[k]), _voltage[k]]);
            }
            yaxisTitle = "Volts";

            Highcharts.setOptions({                                            // This is for all plots, change Date axis to local timezone
                global: {
                    useUTC: false
                }
            });
            $('#BatteryDiv').highcharts({
                chart: {
                    type: 'spline',
                    height: 150,
                    backgroundColor: 'white',
                    borderRadius: 0
                },
                title: {
                    text: null
                },
                xAxis: {
                    type: 'datetime',
                    dateTimeLabelFormats: { // don't display the dummy year
                        millisecond: "%A, %b %e, %H:%M:%S.%L",
                        second: "%A, %b %e, %H:%M:%S",
                        minute: "%A, %b %e, %H:%M",
                        hour: "%A, %b %e, %H:%M",
                        day: "%A, %b %e, %Y",
                        week: "Week from %A, %b %e, %Y",
                        month: "%B %Y",
                        year: "%Y"
                    },
                    title: {
                        text: 'Date'
                    }
                },
                plotOptions: {
                    series: {
                        marker: {
                            enabled: false
                        }
                    }
                },
                yAxis: {
                    lineWidth: 1,
                    showLastLabel: true,
                    gridLineWidth: 0,
                    title: {
                        text: 'Voltage (Volts)'
                    },
                    plotLines: [{
                        value: 0,
                        width: 1,
                        color: '#808080'
                    }]
                },
                legend: { enabled: false },
                tooltip: {
                    enabled: true
                },
                series: [{
                    data: datapoints
                }]
            });
        }
         
    </script>
</body>
</html>
