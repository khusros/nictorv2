﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Accounts.aspx.cs" Inherits="NictorWebClient.sec.admin.Accounts" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Admin - Accounts</title>
    <link rel="shortcut icon" href="../../resources/images/logoheader.png"/>
    <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400' rel='stylesheet' type='text/css' />
    <link href="../../StyleSheet.css" type="text/css" rel="Stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        <div id="header" style="display:block;height:76px;background-color:#eeeeee;">
            <div id="headerLeft" style="height:76px;float:left;">
            <table><tr><td><img src="../../resources/images/logoheader.png" alt=""/></td>
            <td><span style="font-size:2em;font-weight:300;">Admin</span></td></tr></table>                      
            </div>
            <div id="headerRight" style="height:72px;float:right;">
                <asp:Button ID="Signout" runat="server" Text="Sign out" CssClass="btn" OnClick="SignoutClick" style="margin-top:20px;margin-right:10px;"/>
            </div>
        </div>

    <div id="content" style="display:block;height:570px;padding:20px 10px;">
        <div style="display:block; height:70px;">
            <div style="float:left; height:70px;"><h2>All accounts</h2></div>
            <div style="float:right; height:70px;"><a href="AdminClient.aspx">Admin web client</a></div>
        </div>
        <asp:Button ID="CreateBtn" OnClick="CreateBtnClick" runat="server" Text="New account" CssClass="btn" />
        <hr />
        <asp:GridView ID="AccountsGridView" runat="server"  AutoGenerateColumns="false" DataKeyNames="Id" 
                        EmptyDataText="There are no accounts." GridLines="None" BorderStyle="None" 
                        CssClass="gridView" ShowHeader="true" HeaderStyle-CssClass="gridViewHeader">
            <AlternatingRowStyle CssClass="gridViewAltRow" />
            <Columns>                
                <asp:TemplateField Visible="False">  
                    <ItemTemplate>
                    <asp:Label ID="AccountId" runat="server" Text='<%# Eval("id") %>' /> 
                    </ItemTemplate> 
                </asp:TemplateField>                                                
                <asp:TemplateField ItemStyle-Width="" HeaderStyle-HorizontalAlign="Left" >
                    <ItemTemplate>
                        <asp:Button ID="EditAccountBtn" runat="server" AutoPostBack="true" 
                            OnClick="EditAccountBtnClick" ToolTip="Edit" Text="Edit"
                            CssClass="btn" style="margin-right:10px;width:100px;"/>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Email" ItemStyle-Width="250" >
                    <ItemTemplate>
                        <div style="overflow:hidden;text-overflow:ellipsis;white-space:nowrap;">
                            <asp:Label ID="AccountEmail" runat="server" Text='<%# Eval("Email") %>' />
                        </div>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Cnf" ItemStyle-Width="50" >
                    <ItemTemplate>
                        <div style="overflow:hidden;text-overflow:ellipsis;white-space:nowrap;">
                            <asp:Label ID="Label_co" runat="server" Text='<%# Eval("Confirmed") %>' />
                        </div>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Created" ItemStyle-Width="120" >
                    <ItemTemplate>
                        <div style="overflow:hidden;text-overflow:ellipsis;white-space:nowrap;">
                            <asp:Label ID="Label_cr" runat="server" Text='<%# Eval("DateCreated","{0:dd/MM/yyyy}") %>' />
                        </div>
                    </ItemTemplate>
                </asp:TemplateField>                                                  
                <asp:TemplateField HeaderText="Role" ItemStyle-Width="70" >
                    <ItemTemplate>
                        <div style="overflow:hidden;text-overflow:ellipsis;white-space:nowrap;">
                            <asp:Label ID="Label_r" runat="server" Text='<%# Eval("Role") %>' />
                        </div>
                    </ItemTemplate>
                </asp:TemplateField>               
                <asp:TemplateField HeaderText="Last login" ItemStyle-Width="120" >
                    <ItemTemplate>
                        <div style="overflow:hidden;text-overflow:ellipsis;white-space:nowrap;">
                            <asp:Label ID="Label_l" runat="server" Text='<%# Eval("LastLogin","{0:dd/MM/yyyy}") %>' />
                        </div>
                    </ItemTemplate>
                </asp:TemplateField>                                                                                                     
                <asp:TemplateField HeaderText="Status" ItemStyle-Width="200" >
                    <ItemTemplate>
                        <div style="overflow:hidden;text-overflow:ellipsis;white-space:nowrap;">
                            <asp:Label ID="Label_s" runat="server" Text='<%# Eval("Status") %>' />
                        </div>
                    </ItemTemplate>
                </asp:TemplateField>                                                                                                     
            </Columns>
        </asp:GridView> 
    </div>

    </form>
</body>
</html>
