﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NictorWebClient.NictorDataSetTableAdapters;

namespace NictorWebClient.sec.admin
{
    public partial class EditNetwork : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                this.networkId.Value = this.Page.Request.QueryString["id"];
                this.accountEmail.Value = this.Page.Request.QueryString["email"];
                NodeTableAdapter ta = new NodeTableAdapter();
                ta.Connection.ConnectionString = System.Configuration.ConfigurationManager.AppSettings["SqlServerConnectionString"];
                int networkId = int.Parse(this.networkId.Value);
                this.NodeGridView.DataSource = ta.GetDataByNetworkId((int?)networkId);
                this.NodeGridView.DataBind();
                this.UserEmail.Text = this.accountEmail.Value;
                NetworkTableAdapter nTa = new NetworkTableAdapter();
                nTa.Connection.ConnectionString = System.Configuration.ConfigurationManager.AppSettings["SqlServerConnectionString"];
                this.DescTbx.Text = (string)nTa.GetNetworkDescriptionByNetworkId(networkId);
                this.IeeeCoordinatorAddress.Text = (string)nTa.GetCoordIeeeAddress(networkId);
                
            }
        }


        protected void SaveNewClick(object sender, EventArgs e)
        {
            try
            {
                string nodeIeeeAddress = this.NodeIeee.Text.ToUpper();
                NodeTableAdapter ta = new NodeTableAdapter();
                ta.Connection.ConnectionString = System.Configuration.ConfigurationManager.AppSettings["SqlServerConnectionString"];
                int networkId = int.Parse(this.networkId.Value);
                ta.Insert(-9999, -9999, nodeIeeeAddress, this.Description.Text, (int?)networkId, this.NodeType.Text, false);
                this.NodeGridView.DataSource = ta.GetDataByNetworkId((int?)networkId);
                this.NodeGridView.DataBind();
                this.NodeIeee.Text = "";
                this.Description.Text = "";
                this.NodeType.Text = "";
            }
            catch (Exception ex)
            {
            }
        }

        protected void UpdBtnCk(object s, EventArgs e)
        {
            NetworkTableAdapter ta = new NetworkTableAdapter();
            ta.Connection.ConnectionString = System.Configuration.ConfigurationManager.AppSettings["SqlServerConnectionString"];
            ta.UpdateDescription(this.DescTbx.Text, int.Parse(this.networkId.Value));
        }


        protected void DeleteNodeBtnClick(object s, EventArgs e)
        {
            int? id = null;

            try
            {
                Button button = (Button)s;
                GridViewRow row = (GridViewRow)button.NamingContainer;
                id = Convert.ToInt32(((Label)row.FindControl("NodeId")).Text);
                NodeTableAdapter ta = new NodeTableAdapter();
                ta.Connection.ConnectionString = System.Configuration.ConfigurationManager.AppSettings["SqlServerConnectionString"];

                ta.Delete((int)id);
                int networkId = int.Parse(this.networkId.Value);
                this.NodeGridView.DataSource = ta.GetDataByNetworkId((int?)networkId);
                this.NodeGridView.DataBind();
            }
            catch { }


        }

        protected void SignoutClick(object sender, EventArgs e)
        {
            Response.Redirect("http://www.aganalytics.com.au/Echidna/index.htm");
        }
    }
}