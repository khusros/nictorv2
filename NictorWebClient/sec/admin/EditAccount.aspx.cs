﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NictorWebClient.NictorDataSetTableAdapters;

namespace NictorWebClient.sec.admin
{
    public partial class EditAccount : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                this.AccountId.Value = this.Page.Request.QueryString["id"];
                this.AccountEmail.Value = this.Page.Request.QueryString["email"];
                this.UserEmail.Text = this.AccountEmail.Value;

                NetworkUserTableAdapter nuTa = new NetworkUserTableAdapter();
                nuTa.Connection.ConnectionString = System.Configuration.ConfigurationManager.AppSettings["SqlServerConnectionString"];
                int userId = int.Parse(this.AccountId.Value);
                this.NetworkGridView.DataSource = nuTa.GetNetworksByUserId((int?)userId);
                this.NetworkGridView.DataBind();
                
            }
        }

        protected void SaveNewNetworkClick(object sender, EventArgs e)
        {
            try
            {
                string coordIeeeAddress = this.CoordIeee.Text.ToUpper();
                NetworkTableAdapter ta = new NetworkTableAdapter();
                ta.Connection.ConnectionString = System.Configuration.ConfigurationManager.AppSettings["SqlServerConnectionString"];
                int networkId = (int)ta.InsertNetwork(-9999, -9999, this.Description.Text, coordIeeeAddress);

                NetworkUserTableAdapter nuTa = new NetworkUserTableAdapter();
                nuTa.Connection.ConnectionString = System.Configuration.ConfigurationManager.AppSettings["SqlServerConnectionString"];
                nuTa.Insert((int?)networkId, (int?)int.Parse(this.AccountId.Value));

                int userId = int.Parse(this.AccountId.Value);
                this.NetworkGridView.DataSource = nuTa.GetNetworksByUserId((int?)userId);
                this.NetworkGridView.DataBind();

                this.CoordIeee.Text = "";
                this.Description.Text = "";
            }
            catch { }
        }

        protected void EditNetworkBtnClick(object sender, EventArgs e)
        {
            int? id = null;
            bool redirect = false;

            try
            {
                Button button = (Button)sender;
                GridViewRow row = (GridViewRow)button.NamingContainer;
                id = Convert.ToInt32(((Label)row.FindControl("NetworkId")).Text);
                redirect = true;
            }
            catch { }

            if (redirect)
            {
                Response.Redirect("http://www.aganalytics.com.au/Echidna/sec/admin/EditNetwork.aspx?id=" + id + 
                                        "&email=" + this.AccountEmail.Value);
            }            
        }


        protected void SignoutClick(object sender, EventArgs e)
        {
            Response.Redirect("http://www.aganalytics.com.au/Echidna/index.htm");
        }
    }
}