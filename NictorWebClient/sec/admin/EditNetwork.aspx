﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EditNetwork.aspx.cs" Inherits="NictorWebClient.sec.admin.EditNetwork" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Admin - Edit network</title>
    <link rel="shortcut icon" href="../../resources/images/logoheader.png"/>
    <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,500,600' rel='stylesheet' type='text/css' />
    <link href="../../StyleSheet.css" type="text/css" rel="Stylesheet" />
    <link rel="Stylesheet" href="../../resources/jqueryui_css/custom-theme/jquery-ui-1.8.23.custom.css" type="text/css" />
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js" ></script>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jqueryui/1.8.21/jquery-ui.min.js"></script>
    <script type="text/javascript">
    
    </script>
</head>
<body>
    <form id="form1" runat="server">
    
    <asp:HiddenField ID="networkId" runat="server" />
    <asp:HiddenField ID="accountEmail" runat="server" />
    <div id="header" style="display:block;height:76px;background-color:#eeeeee;">
        <div id="headerLeft" style="height:76px;float:left;">
        <table><tr><td><img src="../../resources/images/logoheader.png" alt=""/></td>
        <td><span style="font-size:2em;font-weight:300;">Admin</span></td></tr></table>                      
        </div>
        <div id="headerRight" style="height:72px;float:right;">
            <asp:Button ID="Signout" runat="server" Text="Sign out" CssClass="btn" OnClick="SignoutClick" style="margin-top:20px;margin-right:10px;"/>
        </div>
    </div>

    <div id="content" style="display:block;height:570px;padding:20px 10px;">
        
        <h2>Edit network</h2>
        <p><a href="Accounts.aspx">Back to accounts</a></p>
        <hr />

        <div style="background-color:#E6E6E6;padding:5px;border-radius:3px;border: 1px solid #C0C0C0;">
            <table>
                <tr><td style="width:200px;">Administrator email</td><td><asp:Label ID="UserEmail" runat="server" /></td></tr>
                <tr><td style="width:200px;">Coordinator address</td><td><asp:Label ID="IeeeCoordinatorAddress" runat="server"/></td></tr>
                <tr><td style="width:200px;">Network name</td><td><asp:Textbox ID="DescTbx" runat="server" CssClass="tbox"  style="width:400px;"/></td></tr>
                <tr><td style="width:200px;"></td><td><asp:Button ID="UpdBtn" runat="server" OnClick="UpdBtnCk" Text="Update" CssClass="btn" /></td></tr>
            </table>
        </div>

        <div style="background-color:#E6E6E6;margin-top:20px;padding:5px;border-radius:3px;border: 1px solid #C0C0C0;">
            <div style="margin-bottom:10px;font-size:1.4em;color:#1A3380;">Add a new node to this network</div>
            <div>
                <table>
                        <tr><td style="width:200px;">Node IEEE address</td><td><asp:Textbox ID="NodeIeee" runat="server" CssClass="tbox"  style="width:400px;"></asp:Textbox></td></tr>
                        <tr><td style="width:200px;">Description</td><td><asp:Textbox ID="Description" runat="server" TextMode="MultiLine" CssClass="tbox"  style="width:398px;"></asp:Textbox></td></tr>
                        <tr><td style="width:200px;">Node type</td><td><asp:TextBox ID="NodeType" runat="server" CssClass="tbox"  style="width:200px; margin-right:10px;" />(e.g. shule_ec20, shule_flow)</td></tr>
                        <tr><td style="width:200px;"></td><td><asp:Button ID="SaveNew" runat="server" Text="Save" OnClick="SaveNewClick" CssClass="btn" /></td></tr>        
                </table>
            </div>
         </div>
        <div style="background-color:#E6E6E6;margin-top:20px;padding:5px;border-radius:3px;border: 1px solid #C0C0C0;margin-bottom:20px;">
            <div style="margin-bottom:10px;font-size:1.4em;color:#1A3380;">List of nodes in this network</div> 

                <asp:GridView ID="NodeGridView" runat="server"  AutoGenerateColumns="false" DataKeyNames="Id" 
                                EmptyDataText="There are no nodes." GridLines="None" BorderStyle="None" 
                                CssClass="gridView" ShowHeader="true" HeaderStyle-CssClass="gridViewHeader">
                    <AlternatingRowStyle CssClass="gridViewAltRow" />
                    <Columns>                
                        <asp:TemplateField Visible="False">  
                            <ItemTemplate>
                            <asp:Label ID="NodeId" runat="server" Text='<%# Eval("id") %>' /> 
                            </ItemTemplate> 
                        </asp:TemplateField>                                                
                        <asp:TemplateField ItemStyle-Width="" HeaderStyle-HorizontalAlign="Left" >
                            <ItemTemplate>
                                <asp:Button ID="DeleteNodeBtn" runat="server" AutoPostBack="true" 
                                    OnClick="DeleteNodeBtnClick" ToolTip="Delete" Text="Delete"
                                    CssClass="btn" style="margin-right:10px;width:100px;"/>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Description" ItemStyle-Width="250" >
                            <ItemTemplate>
                                <div style="overflow:hidden;text-overflow:ellipsis;white-space:nowrap;">
                                    <asp:Label ID="Label_d" runat="server" Text='<%# Eval("Description") %>' />
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Node type" ItemStyle-Width="250" >
                            <ItemTemplate>
                                <div style="overflow:hidden;text-overflow:ellipsis;white-space:nowrap;">
                                    <asp:Label ID="Label_t" runat="server" Text='<%# Eval("NodeType") %>' />
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="IEEE address" ItemStyle-Width="250" >
                            <ItemTemplate>
                                <div style="overflow:hidden;text-overflow:ellipsis;white-space:nowrap;">
                                    <asp:Label ID="Label_addr" runat="server" Text='<%# Eval("IeeeAddress") %>' />
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>                                                                                                 
                    </Columns>
                </asp:GridView>  
        </div>
        
    </div>

    </form>
</body>
</html>
