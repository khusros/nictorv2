﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NictorWebClient.NictorDataSetTableAdapters;

namespace NictorWebClient.sec.admin
{
    public partial class Accounts : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                MembershipTableAdapter ta = new MembershipTableAdapter();                
                ta.Connection.ConnectionString = System.Configuration.ConfigurationManager.AppSettings["SqlServerConnectionString"];
                this.AccountsGridView.DataSource = ta.GetData();
                this.AccountsGridView.DataBind();
            }
        }

        protected void CreateBtnClick(object sender, EventArgs e)
        {
            Response.Redirect("NewAccount.aspx");
        }

        protected void EditAccountBtnClick(object sender, EventArgs e)
        {
            int? id = null;
            string email = null;
            bool redirect = false;

            try
            {
                Button button = (Button)sender;
                GridViewRow row = (GridViewRow)button.NamingContainer;
                id = Convert.ToInt32(((Label)row.FindControl("AccountId")).Text);
                email = ((Label)row.FindControl("AccountEmail")).Text;
                redirect = true;
            }
            catch {}

            if (redirect) {
                Response.Redirect("EditAccount.aspx?id=" + id + "&email=" + email); 
            }
        }

        protected void SignoutClick(object sender, EventArgs e)
        {
            Response.Redirect("http://www.aganalytics.com.au/Echidna/index.htm");
        }
    }
}