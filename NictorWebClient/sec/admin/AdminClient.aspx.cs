﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Security.Cryptography;

namespace NictorWebClient.sec.admin
{
    public partial class AdminClient : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            this.ErrorLbl.Text = "";
            this.NodeListError.Text = "";
            this.NodeStatusLbl.Text = "";
            this.CommandLbl.Text = "";
            this.CommandHistoryLbl.Text = "";
            if (!IsPostBack)
            {
                DatabaseInterface dbif = new DatabaseInterface();
                List<User> listOfUsers = dbif.GetUsers();
                this.UserList.DataValueField = "Id";
                this.UserList.DataTextField = "Email";
                this.UserList.DataSource = listOfUsers;
                this.UserList.DataBind();
            }
        }

        protected void SelectUserBtnClick(object sender, EventArgs e)
        {
            DatabaseInterface dbif = new DatabaseInterface();
            string error;
            string selectUserId = this.UserList.SelectedValue;            
            this.hfUserId.Value = selectUserId;
            List<Network> listOfNetworks = dbif.GetNetworkListByUserId(int.Parse(this.hfUserId.Value), out error);
            if (error != "") this.ErrorLbl.Text = error;
            this.NetworkList.DataValueField = "ieeeAddress";
            this.NetworkList.DataTextField = "ieeeAddress";
            this.NetworkList.DataSource = listOfNetworks;
            this.NetworkList.DataBind();
            if (listOfNetworks.Count == 0)
            {
                this.NodeListError.Text = "There are no networks.";
            }
        }

        protected void testBtnClick(object sender, EventArgs e)
        {
            Utils util = new Utils();
            this.testLbl.Text = util.GetMd5Hash(MD5.Create(), testTbox.Text);
        }

        protected void GoBtnClick(object sender, EventArgs e)
        {
            string selectedIeeeAddress = this.NetworkList.SelectedValue;
            DatabaseInterface dbif = new DatabaseInterface();
            string error;
            List<Network> listOfNetworks = dbif.GetNetworkListByUserId(int.Parse(this.hfUserId.Value), out error);
            if (error != "") this.ErrorLbl.Text = error;
            foreach (Network n in listOfNetworks)
            {
                if (n.ieeeAddress == selectedIeeeAddress)
                {
                    this.NodeList.DataValueField = "ieeeAddress";
                    this.NodeList.DataTextField = "ieeeAddress";
                    this.NodeList.DataSource = n.nodes;
                    this.NodeList.DataBind();
                    if (n.nodes.Count == 0)
                    {
                        this.NodeListError.Text = "There are no nodes for this network";
                    }
                    break;
                }
            }
        }

        protected void GoNodeBtnClick(object sender, EventArgs e)
        {
            string selectedIeeeAddress = this.NodeList.SelectedValue;
            DatabaseInterface dbif = new DatabaseInterface();
            string error;
            List<NodeStatusItem> status = dbif.GetNodeStatus(selectedIeeeAddress, out error);
            this.NodeStatusGridView.DataSource = status;
            this.NodeStatusGridView.DataBind();
            if (status.Count == 0)
            {
                this.NodeStatusLbl.Text = "No status data for this node.";
            }

            string measType;
            List<Measurement> measurements = dbif.SelectMeasurement(selectedIeeeAddress, "", "", out measType, out error);
            this.NodeMeasGridView.DataSource = measurements;
            this.NodeMeasGridView.DataBind();
            if (measurements.Count == 0)
            {
                this.NodeStatusLbl.Text = "No sensor data for this node.";
            }
        }

        protected void StartBtnClick(object sender, EventArgs e)
        {
            DatabaseInterface dbif = new DatabaseInterface();
            string error1;
            string error2 = "";
            dbif.StartNetwork(this.NetworkList.SelectedValue, out error1);
            //dbif.UpdateSleepParameters(this.NetworkList.SelectedValue, 10, 30, out error2);
            this.ErrorLbl.Text = error1 + " , " + error2;
            if (this.ErrorLbl.Text != "")
            {
                this.CommandLbl.Text = this.ErrorLbl.Text;
            }
            else
            {
                this.CommandLbl.Text = "Ok.";
                List<NictorCommand> cmds = dbif.GetCommandList(out error1);
                this.CommandListGridView.DataSource = cmds;
                this.CommandListGridView.DataBind();
            }

        }

        protected void StopBtnClick(object sender, EventArgs e)
        {
            DatabaseInterface dbif = new DatabaseInterface();
            string error;
            dbif.StopNetwork(this.NetworkList.SelectedValue, out error);
            if (error != "")
            {
                this.CommandLbl.Text = error;
            }
            else
            {
                this.CommandLbl.Text = "Ok.";
                List<NictorCommand> cmds = dbif.GetCommandList(out error);
                this.CommandListGridView.DataSource = cmds;
                this.CommandListGridView.DataBind();
            }
        }

        protected void OpenValveBtnClick(object sender, EventArgs e)
        {
            DatabaseInterface dbif = new DatabaseInterface();
            string error;
            string nodeAddress = this.IeeeAddressTbox1.Text;
            if (nodeAddress == "")
            {
                this.ErrorLbl.Text = "Enter a node address";
                return;
            }
            string coordAddress = this.NetworkList.SelectedValue;
            dbif.OpenValve(coordAddress, nodeAddress, out error);
            this.ErrorLbl.Text = error;
        }

        protected void CloseValveBtnClick(object sender, EventArgs e)
        {
            DatabaseInterface dbif = new DatabaseInterface();
            string error;
            string nodeAddress = this.IeeeAddressTbox1.Text;
            if (nodeAddress == "")
            {
                this.ErrorLbl.Text = "Enter a node address";
                return;
            }
            string coordAddress = this.NetworkList.SelectedValue;
            dbif.CloseValve(coordAddress, nodeAddress, out error);
            this.ErrorLbl.Text = error;
        }

        protected void SleepParamBtnClick(object sender, EventArgs e)
        {
            try
            {
                DatabaseInterface dbif = new DatabaseInterface();
                string error;
                string coordAddress = this.NetworkList.SelectedValue;
                if ((this.BeaconSecTbox.Text == "") || (this.WakeSecTbox.Text == ""))
                {
                    this.ErrorLbl.Text = "Enter valid network sleep parameters.";
                    return;
                }
                dbif.UpdateSleepParameters(coordAddress,
                        int.Parse(this.BeaconSecTbox.Text),
                        int.Parse(this.WakeSecTbox.Text), out error);
                this.ErrorLbl.Text = error;
            }
            catch (Exception ex)
            {
                this.ErrorLbl.Text = ex.Message;
            }
        }

        protected void RefreshCmdListBtnClick(object sender, EventArgs e)
        {
            DatabaseInterface dbif = new DatabaseInterface();
            string error;
            List<NictorCommand> cmds = dbif.GetCommandList(out error);
            this.CommandListGridView.DataSource = cmds;
            this.CommandListGridView.DataBind();
            if (cmds.Count == 0)
            {
                this.CommandHistoryLbl.Text = "There are no commands." + error;
            }
        }

        protected void GetNextCommandBtnClick(object s, EventArgs e)
        {
            this.NextCommandLbl.Text = "";
            DatabaseInterface dbif = new DatabaseInterface();
            string error;
            string nextCommand = dbif.GetNextCommandAlt(this.NetworkList.SelectedValue, out error);
            if (error == "")
                this.NextCommandLbl.Text = nextCommand;
            else
                this.NextCommandLbl.Text = error;
        }
    }
}