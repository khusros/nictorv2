﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AdminClient.aspx.cs" Inherits="NictorWebClient.sec.admin.AdminClient" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Nictor web client</title>

    <link rel="shortcut icon" href="../../resources/images/logoheader.png"/>
    <link href="../../StyleSheet.css" type="text/css" rel="Stylesheet"/>
</head>
<body>
    <form id="form1" runat="server">
    <asp:HiddenField ID="hfUserId" runat="server" />
    <div>
        <div id="header" style="display:block;height:72px;">
            <div id="headerLeft" style="height:72px;float:left;"><img src="../../resources/images/logoheader.png" alt=""/>
                
            </div>
            <div id="headerRight" style="height:72px;float:right;">
                <a href="Accounts.aspx" style="margin-right:20px;">Back to accounts</a><span style="font-size:2em;">Nictor test client</span>
            </div>
        </div>

        <p>Error or status: <asp:Label ID="ErrorLbl" runat="server" /></p>
        <div>
            input <asp:Textbox ID="testTbox" runat="server" /><asp:Button ID="testBtn" runat="server" OnClick="testBtnClick" Text="ok" />
            <asp:Label ID="testLbl" runat="server" />
        </div>
        <div style="margin-bottom:10px;">
            <div style="width:100%;height:220px;">
                <div style="width:100%;height:50px;border:2px solid black; border-radius:4px;padding:10px;">
                    Users: 
                    <asp:DropDownList ID="UserList" runat="server" Width="300" /> <asp:Button ID="SelectUserBtn" runat="server" Text="Select user" OnClick="SelectUserBtnClick" />
                </div>
                <div style="width:47%;height:200px;float:left;border: 2px solid black; border-radius:4px;padding:10px;">
                    <span style="font-size:1.3em;display:block;padding-bottom:5px;">Networks (coordinator addresses)</span>
                    <asp:DropDownList ID="NetworkList" runat="server" Width="300" /> <asp:Button ID="GoBtn" runat="server" Text="Get nodes" OnClick="GoBtnClick" /><br />
                    <asp:Label ID="NodeListError" runat="server" />
                </div>
                <div style="width:47%;height:200px;float:right;border: 2px solid black; border-radius:4px;padding:10px;">
                    <span style="font-size:1.3em;display:block;padding-bottom:5px;">Network commands</span>
                    <div style="font-size:0.8em;">                   
                        <div style="padding-bottom:5px;">
                            <asp:Button ID="StopBtn" runat="server" Text="Stop network" OnClick="StopBtnClick" />
                            <asp:Button ID="StartBtn" runat="server" Text="Start network" OnClick="StartBtnClick" />       
                        </div>
                        <table>
                            <tr><td>Beacon interval (min, &lt; 60)</td><td><asp:TextBox ID="BeaconSecTbox" runat="server" /></td></tr>
                            <tr><td>Wake inteval (sec)</td><td><asp:TextBox ID="WakeSecTbox" runat="server" /></td></tr>
                            <tr><td colspan="2"><asp:Button ID="SleepParamBtn" runat="server" Text="Update sleep params" OnClick="SleepParamBtnClick" /></td></tr>
                        </table>
                        <asp:Label ID="CommandLbl" runat="server" />
                    </div>
                </div>
            </div>
        </div>
        
        <div style="margin-bottom:10px;">
            <div style="width:100%;height:190px;">
                <div style="width:47%;height:170px;float:left;border: 2px solid black; border-radius:4px;padding:10px;">
                    <span style="font-size:1.3em;display:block;">Node list</span>
                    <asp:DropDownList ID="NodeList" runat="server" Width="300" /> <asp:Button ID="GoNodeBtn" runat="server" Text="View data" OnClick="GoNodeBtnClick" /><br />
                    <asp:Label ID="NodeStatusLbl" runat="server" />
                </div>
                <div style="width:47%;height:170px;float:right;border: 2px solid black; border-radius:4px;padding:10px;">
                    <span style="font-size:1.3em;display:block;">Node commands</span>
                    <table>
                        <tr><td>Node address</td><td><asp:TextBox ID="IeeeAddressTbox1" runat="server" /></td></tr>
                        <tr><td><asp:Button ID="OpenValveBtn" runat="server" Text="Open valve" OnClick="OpenValveBtnClick" /></td>
                            <td><asp:Button ID="CloseValveBtn" runat="server" Text="Close valve" OnClick="CloseValveBtnClick" /></td></tr>
                    </table>    
                </div>
            </div>
        </div>

        <div style="width:100%;height:520px;margin-bottom:10px;">
            <div style="width:47%;float:left;height:500px;padding:10px;border: 2px solid black; border-radius:4px;overflow-y:scroll;">
                <span style="font-size:1.3em;display:block;margin-bottom:10px;">Node status</span>
                <div style="font-size:0.8em;">

                <asp:GridView ID="NodeStatusGridView" runat="server" />
                </div>
            </div>
            <div style="width:47%;float:right;height:500px;padding:10px;border: 2px solid black; border-radius:4px;">
                <span style="font-size:1.3em;display:block;margin-bottom:10px;">Sensor data</span>
                <div style="font-size:0.8em;">
                <asp:GridView ID="NodeMeasGridView" runat="server" />
                </div>
            </div>
        </div>

        <div style="width:80%;height:520px;padding:10px;border: 2px solid black; border-radius:4px; overflow-y:scroll;">
            <span style="font-size:1.3em;display:block;margin-bottom:10px;">Command history</span>
            <div style="padding-bottom:10px;">
            <asp:Button ID="GetNextCommandBtn" runat="server" Text="Get next command" OnClick="GetNextCommandBtnClick" /><br />
            <asp:Label ID="NextCommandLbl" runat="server" /><br />
            </div>

            <asp:Button ID="RefreshCmdListBtn" runat="server" Text="Refresh" OnClick="RefreshCmdListBtnClick" /><br />
            <asp:Label ID="CommandHistoryLbl" runat="server" />
            <div style="font-size:0.8em;padding-top:10px;">
                <asp:GridView ID="CommandListGridView" runat="server" />    
            </div>
        </div>

    </div>
    </form>
</body>
</html>
