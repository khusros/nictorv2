﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NictorWebClient.NictorDataSetTableAdapters;
using System.Security.Cryptography;
using System.Net.Mail;

namespace NictorWebClient.sec.admin
{
    public partial class NewAccount : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Label.Text = "";
        }

        protected void SubmitBtnClick(object sender, EventArgs e)
        {
            if ((this.EmailTbox.Text == "") || (this.PwdTbox.Text == "") || (this.CnfPwdTbox.Text == ""))
            {
                this.Label.Text = "There are some empty fields.";
                return; 
            }
            if (this.PwdTbox.Text != this.CnfPwdTbox.Text) 
            { 
                this.Label.Text = "Passwords do not match."; 
                return; 
            }

            Utils utils = new Utils();
            MembershipTableAdapter ta = new MembershipTableAdapter();
            ta.Connection.ConnectionString = System.Configuration.ConfigurationManager.AppSettings["SqlServerConnectionString"];
            string passwordHash = utils.GetMd5Hash(MD5.Create(), this.PwdTbox.Text);
            string confirmToken = utils.GenerateRandomString32();
            if ((int)ta.IsEmailAlreadyUsed(this.EmailTbox.Text) == 1)
            {
                this.Label.Text = "The email is not free.";
                return;
            }
            ta.InsertMembership(this.EmailTbox.Text, passwordHash, false, confirmToken, DateTime.Now, DateTime.Now, "administrator");
            if (System.Configuration.ConfigurationManager.AppSettings["IsDebugMode"] == "false")
            {
                // Send email
                MailMessage message = new MailMessage();
                message.From = new MailAddress("support@aganalytics.com.au");
                message.To.Add(new MailAddress(this.EmailTbox.Text));
                message.Subject = "New account";
                message.Body = "Follow this link to finalize your sign up:\n" +
                    "http://www.aganalytics.com.au/Echidna/confirm.aspx?t=" + confirmToken + "\n" +
                    "Thank You,\n" +
                    "Ag Analytics support";

                SmtpClient client = new SmtpClient("localhost", 25);
                client.Send(message);

                this.Label.Text = "The account has been created and an email has been sent";
            }
            else
            {
                this.Label.Text = "Navigate to the following link using your browser:<br/>" + "http://www.aganalytics.com.au/Echidna/confirm.aspx?t=" + confirmToken;
            }
            
        }

        protected void SignoutClick(object sender, EventArgs e)
        {
            Response.Redirect("http://www.aganalytics.com.au/Echidna/index.htm");
        }
    }
}