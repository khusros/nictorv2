﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;

namespace NictorWebClient
{
    public partial class signin : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.label.Text = "";
        }

        protected void SubmitClick(object sender, EventArgs e)
        {
            DatabaseInterface dbif = new DatabaseInterface();
            string error;
            User user = dbif.SignIn(this.email.Text, this.password.Text, out error);
            if (error != "")
            {
                this.label.Text = "Could not sign you in, try again.";
                return;
            }
            if ((user.Id == -1) || (user.Email == ""))
            {
                this.label.Text = "Check your email and/or password.";
                return;
            }
            FormsAuthentication.SetAuthCookie(user.Email, false);
            if (this.email.Text == "echidna.admin@aganalytics.com.au")
            {
                Response.Redirect("http://www.aganalytics.com.au/Echidna/sec/admin/Accounts.aspx");
            }
            else 
            {
                Response.Redirect("http://www.aganalytics.com.au/Echidna/sec/index.aspx");
            }
        }
    }
}