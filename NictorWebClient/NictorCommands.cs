﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NictorWebClient
{
    public static class NictorCommandIds
    {
        public static int NICTOR_SLEEP_UPDATE_CMD_ID = 0x01;
        public static int NICTOR_ACTIVE_MODE_CMD_ID = 0x02;
        public static int NICTOR_IDLE_MODE_CMD_ID = 0x03;
        public static int NICTOR_ACTUATION_CMD_ID = 0x04;

        public static int NODE_NO_MORE_DATA_CMD_ID = 0xA1;
        public static int NODE_INFO_CMD_ID = 0xA0;
        public static int COORD_INFO_CMD_ID = 0xA2;
        public static int ECHO_DATA_CMD_ID = 0xFE;
        public static int FLOW_DATA_CMD_ID = 0xFD;
    }

    public static class NictorActuationCommands
    {
        public static int OPEN_VALVE = 0xA0;
        public static int CLOSE_VALVE = 0xA1;
    }
}