﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using System.Globalization;

namespace NictorWebClient
{

    public class DatabaseInterface
    {

        public string connectionString = System.Configuration.ConfigurationManager.AppSettings["SqlServerConnectionString"];

        public DatabaseInterface() { }

        public User SignIn(string email, string clearTextPassword, out string error)
        {
            error = "";
            User user = new User(-1, "");

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                try
                {
                    Utils utils = new Utils();
                    string passwordHash = utils.GetMd5Hash(MD5.Create(), clearTextPassword);
                    connection.Open();
                    SqlCommand command = connection.CreateCommand();
                    command.Connection = connection;
                    command.CommandText = "SELECT * FROM nictordb.dbo.Membership where Email=@Email AND PasswordHash=@PasswordHash";
                    command.Parameters.Clear();
                    command.Parameters.AddWithValue("@Email", email);
                    command.Parameters.AddWithValue("@PasswordHash", passwordHash);
                    SqlDataReader rdr = command.ExecuteReader();
                    while (rdr.Read())
                    {
                        if (!bool.Parse(rdr["Confirmed"].ToString())) break;
                        user.Id = int.Parse(rdr["Id"].ToString());
                        user.Email = rdr["Email"].ToString();
                    }
                    rdr.Close();
                }
                catch (Exception ex)
                {
                    error = ex.Message;
                }
            }
            return user;
        }

        public void WriteNodeStatus(string ieeeAddress, double battery, double temperature, out string error)
        {
            error = "";
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                try
                {
                    connection.Open();
                    SqlCommand command = connection.CreateCommand();
                    command.Connection = connection;

                    // Get node id from ieee address
                    command.CommandText = "SELECT Id FROM nictordb.dbo.Node WHERE nictordb.dbo.Node.IeeeAddress=@IeeeAddress";
                    command.Parameters.Clear();
                    command.Parameters.AddWithValue("@IeeeAddress", ieeeAddress);
                    int? nodeId = (int)command.ExecuteScalar();
                    if (nodeId != null)
                    {
                        // Insert values into node status
                        command.CommandText = "INSERT INTO nictordb.dbo.NodeStatus " + 
                                                "(NodeId, Battery, Temperature) " + 
                                                "VALUES (@NodeId, @Battery, @Temperature);";
                        command.Parameters.Clear();
                        command.Parameters.AddWithValue("@NodeId", nodeId);
                        command.Parameters.AddWithValue("@Battery", battery);
                        command.Parameters.AddWithValue("@Temperature",temperature);
                        command.ExecuteNonQuery();
                    }
                }
                catch (Exception ex)
                {
                    error = ex.Message;
                }
            }
        }

        public void WriteNetworkStatus(string coordIeeeAddress, double battery, 
            double temperature, int networkState, int networkIntervalInMin, int networkWakeIntervalInSec, out string error)
        {
            error = "";
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                try
                {
                    connection.Open();
                    SqlCommand command = connection.CreateCommand();
                    command.Connection = connection;

                    // Get node id from ieee address
                    command.CommandText = "SELECT Id FROM nictordb.dbo.Network WHERE nictordb.dbo.Network.CoordIeeeAddress=@CoordIeeeAddress";
                    command.Parameters.Clear();
                    command.Parameters.AddWithValue("@CoordIeeeAddress", coordIeeeAddress);
                    int? networkId = (int)command.ExecuteScalar();
                    if (networkId != null)
                    {
                        // Insert values into node status
                        command.CommandText = "INSERT INTO nictordb.dbo.NetworkStatus " +
                                                "(NetworkId, Battery, Temperature, NetworkState, NetworkInterval, WakeInterval) " +
                                                "VALUES (@NetworkId, @Battery, @Temperature,@NetworkState,@NetworkInterval,@WakeInterval);";
                        command.Parameters.Clear();
                        command.Parameters.AddWithValue("@NetworkId", networkId);
                        command.Parameters.AddWithValue("@Battery", battery);
                        command.Parameters.AddWithValue("@Temperature", temperature);
                        command.Parameters.AddWithValue("@NetworkState", networkState);
                        command.Parameters.AddWithValue("@NetworkInterval", networkIntervalInMin);
                        command.Parameters.AddWithValue("@WakeInterval", networkWakeIntervalInSec);
                        command.ExecuteNonQuery();
                    }
                }
                catch (Exception ex)
                {
                    error = ex.Message;
                }
            }
        }

        public List<Node> GetNodesList(out string error)
        {
            error = "";
            List<Node> listOfNodes = new List<Node>();
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                try
                {
                    connection.Open();
                    SqlCommand command = connection.CreateCommand();
                    command.Connection = connection;
                    command.CommandText = "SELECT * FROM nictordb.dbo.Node";
                    SqlDataReader rdr = command.ExecuteReader();
                    while (rdr.Read())
                    {
                        Node node = new Node();
                        node.description = rdr["Description"].ToString();
                        node.ieeeAddress = rdr["IeeeAddress"].ToString();
                        node.nodeType = rdr["NodeType"].ToString();
                        node.lat = double.Parse(rdr["Lat"].ToString());
                        node.lng = double.Parse(rdr["Lng"].ToString());
                        node.relayIsOpen = bool.Parse(rdr["RelayIsOpen"].ToString());
                        listOfNodes.Add(node);
                    }
                    rdr.Close();
                }
                catch (Exception ex)
                {
                    error = ex.Message;
                }
            }
            return listOfNodes;
        }

        public List<User> GetUsers()
        {
            List<User> allUsers = new List<User>();
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                try
                {
                    connection.Open();
                    SqlCommand command = connection.CreateCommand();
                    command.Connection = connection;
                    command.CommandText = "SELECT * FROM nictordb.dbo.Membership;";
                    SqlDataReader rdr = command.ExecuteReader();
                    while (rdr.Read())
                    {
                        User user = new User(int.Parse(rdr["Id"].ToString()), rdr["Email"].ToString());
                        allUsers.Add(user);
                    }
                    rdr.Close();
                }
                catch {};
            }
            return allUsers;
        }

        public List<Network> GetNetworkListByUserId(int userId, out string error)
        {
            error = "";
            List<Network> listOfNetworks = new List<Network>();
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                try
                {
                    connection.Open();
                    SqlCommand command = connection.CreateCommand();
                    command.Connection = connection;
                    command.CommandText = "SELECT * FROM nictordb.dbo.Network JOIN " +
                                            "nictordb.dbo.NetworkUser ON " +
                                            "nictordb.dbo.NetworkUser.NetworkId = " +
                                            "nictordb.dbo.Network.Id " +
                                            "WHERE nictordb.dbo.NetworkUser.UserId=@UserId;";
                    command.Parameters.Clear();
                    command.Parameters.AddWithValue("@UserId", userId);
                    SqlDataReader rdr = command.ExecuteReader();
                    while (rdr.Read())
                    {
                        Network nwk = new Network();
                        nwk.id = int.Parse(rdr["id"].ToString());
                        nwk.description = rdr["Description"].ToString();
                        nwk.ieeeAddress = rdr["CoordIeeeAddress"].ToString();
                        nwk.lat = double.Parse(rdr["Lat"].ToString());
                        nwk.lng = double.Parse(rdr["Lng"].ToString());
                        nwk.latestBattery = -1;
                        nwk.latestRxTimestamp = "";
                        nwk.markerColor = rdr["MarkerColor"].ToString();
                        listOfNetworks.Add(nwk);
                    }
                    rdr.Close();
                    
                    foreach (Network nwk in listOfNetworks)
                    {
                        command.Connection = connection;

                        command.CommandText = "SELECT TOP (1) nictordb.dbo.NetworkStatus.RxTimeStamp, nictordb.dbo.NetworkStatus.Battery, nictordb.dbo.NetworkStatus.Temperature, nictordb.dbo.NetworkStatus.NetworkState " +
                                                "FROM nictordb.dbo.NetworkStatus INNER JOIN " +
                                                "nictordb.dbo.Network ON nictordb.dbo.Network.Id = nictordb.dbo.NetworkStatus.NetworkId " +
                                                "WHERE (nictordb.dbo.Network.CoordIeeeAddress = @IeeeAddress) " +
                                                "ORDER BY nictordb.dbo.NetworkStatus.RxTimeStamp DESC;";
                        command.Parameters.Clear();
                        command.Parameters.AddWithValue("@IeeeAddress", nwk.ieeeAddress);
                        rdr = command.ExecuteReader();
                        while (rdr.Read())
                        {
                            nwk.latestBattery = Math.Round(double.Parse(rdr["Battery"].ToString()),1);
                            nwk.isNetworkActive = (int.Parse(rdr["NetworkState"].ToString()) == 1);
                            DateTimeOffset d = DateTimeOffset.Parse(rdr["RxTimeStamp"].ToString());
                            TimeSpan i = DateTimeOffset.Now - d;
                            nwk.latestRxTimestamp = (i.TotalMinutes > 60) ? "  <b>Check battery</b>" : Math.Round(i.TotalMinutes, 0).ToString() + " mins ago";
                        }
                        rdr.Close();

                        command.CommandText = "SELECT * FROM nictordb.dbo.Node " +
                                                "WHERE nictordb.dbo.Node.NetworkId=@Id;";
                        command.Parameters.Clear();
                        command.Parameters.AddWithValue("@Id", nwk.id);
                        rdr = command.ExecuteReader();
                        List<Node> listOfNodes = new List<Node>();
                        while (rdr.Read())
                        {
                            Node node = new Node();
                            node.description = rdr["Description"].ToString();
                            node.nodeType = rdr["NodeType"].ToString();
                            node.ieeeAddress = rdr["IeeeAddress"].ToString();
                            node.lat = double.Parse(rdr["Lat"].ToString());
                            node.lng = double.Parse(rdr["Lng"].ToString());
                            node.relayIsOpen = bool.Parse(rdr["RelayIsOpen"].ToString());
                            node.latestBattery = -1;
                            node.latestRxTimestamp = "";
                            listOfNodes.Add(node);
                        }
                        rdr.Close();

                        for (int k = 0; k < listOfNodes.Count; k++ )
                        {
                            command.CommandText = "SELECT TOP (1) nictordb.dbo.NodeStatus.RxTimeStamp, nictordb.dbo.NodeStatus.Battery, nictordb.dbo.NodeStatus.Temperature " +
                                                    "FROM nictordb.dbo.NodeStatus INNER JOIN " +
                                                    "nictordb.dbo.Node ON nictordb.dbo.Node.Id = nictordb.dbo.NodeStatus.NodeId " +
                                                    "WHERE (nictordb.dbo.Node.IeeeAddress = @IeeeAddress) " +
                                                    "ORDER BY nictordb.dbo.NodeStatus.RxTimeStamp DESC;";
                            command.Parameters.Clear();
                            command.Parameters.AddWithValue("@IeeeAddress", listOfNodes[k].ieeeAddress);
                            rdr = command.ExecuteReader();
                            while (rdr.Read())
                            {
                                listOfNodes[k].latestBattery = Math.Round(double.Parse(rdr["Battery"].ToString()), 1);
                                DateTimeOffset d = DateTimeOffset.Parse(rdr["RxTimeStamp"].ToString());
                                TimeSpan i = DateTimeOffset.Now - d;
                                listOfNodes[k].latestRxTimestamp = (i.TotalMinutes > 60) ? "  <b>Check battery</b>" : Math.Round(i.TotalMinutes, 0).ToString() + " mins ago";
                            }
                            rdr.Close();
                        }

                        nwk.nodes = listOfNodes;
                    }
                }
                catch (Exception ex)
                {
                    error = ex.Message;
                }
            }
            return listOfNetworks;      
        }

        public Network GetNetwork(string ieeeAddress, out string error)
        {
            error = "";
            Network nwk = new Network();
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                try
                {
                    connection.Open();
                    SqlCommand command = connection.CreateCommand();
                    command.Connection = connection;
                    command.CommandText = "SELECT * FROM nictordb.dbo.Network WHERE nictordb.dbo.Network.CoordIeeeAddress=@ieeeAddress";
                    command.Parameters.Clear();
                    command.Parameters.AddWithValue("@ieeeAddress", ieeeAddress);
                    SqlDataReader rdr = command.ExecuteReader();
                    while (rdr.Read())
                    {
                        nwk.description = rdr["Description"].ToString();
                        nwk.ieeeAddress = rdr["CoordIeeeAddress"].ToString();
                        nwk.lat = double.Parse(rdr["Lat"].ToString());
                        nwk.lng = double.Parse(rdr["Lng"].ToString());
                        nwk.markerColor = rdr["MarkerColor"].ToString();
                    }
                    rdr.Close();
                }
                catch (Exception ex)
                {
                    error = ex.Message;
                }
            }
            return nwk;
        }

        public Node GetNode(string ieeeAddress, out string error)
        {
            error = "";
            Node node = new Node();
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                try
                {
                    connection.Open();
                    SqlCommand command = connection.CreateCommand();
                    command.Connection = connection;
                    command.CommandText = "SELECT * FROM nictordb.dbo.Node WHERE nictordb.dbo.Node.IeeeAddress=@ieeeAddress";
                    command.Parameters.Clear();
                    command.Parameters.AddWithValue("@ieeeAddress", ieeeAddress);
                    SqlDataReader rdr = command.ExecuteReader();
                    while (rdr.Read())
                    {
                        node.description = rdr["Description"].ToString();
                        node.nodeType = rdr["NodeType"].ToString();
                        node.ieeeAddress = rdr["IeeeAddress"].ToString();
                        node.lat = double.Parse(rdr["Lat"].ToString());
                        node.lng = double.Parse(rdr["Lng"].ToString());
                        node.relayIsOpen = bool.Parse(rdr["RelayIsOpen"].ToString());
                    }
                    rdr.Close();
                }
                catch (Exception ex)
                {
                    error = ex.Message;
                }
            }

            return node;
        }

        public bool isNetwork(string ieeeAddress, out string error)
        {
            error = "";
            bool result = false;
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                try
                {
                    connection.Open();
                    SqlCommand command = connection.CreateCommand();
                    command.Connection = connection;
                    command.CommandText = "SELECT COUNT(Id) FROM nictordb.dbo.Network " +
                                            "WHERE nictordb.dbo.Network.CoordIeeeAddress = @IeeeAddress;";
                    command.Parameters.Clear();
                    command.Parameters.AddWithValue("@IeeeAddress", ieeeAddress);
                    result = ((int)command.ExecuteScalar() == 1);
                }
                catch (Exception ex)
                {
                    error = ex.Message;
                }
            }
            return result;
        }

        public bool isNode(string ieeeAddress, out string error) 
        {
            error = "";
            bool result = false;
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                try
                {
                    connection.Open();
                    SqlCommand command = connection.CreateCommand();
                    command.Connection = connection;
                    command.CommandText = "SELECT COUNT(Id) FROM nictordb.dbo.Node " +
                                            "WHERE nictordb.dbo.Node.IeeeAddress = @IeeeAddress;";
                    command.Parameters.Clear();
                    command.Parameters.AddWithValue("@IeeeAddress", ieeeAddress);
                    result = ((int)command.ExecuteScalar() == 1);
                }
                catch (Exception ex)
                {
                    error = ex.Message;
                }
            }
            return result;
        }

        public void UpdateNetworkProperties(Network nwk, out string error)
        {
            error = "";
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                try
                {
                    connection.Open();
                    SqlCommand command = connection.CreateCommand();
                    command.Connection = connection;
                    command.CommandText = "UPDATE nictordb.dbo.Network SET " + 
                                            "nictordb.dbo.Network.Description=@description, " + 
                                            "nictordb.dbo.Network.MarkerColor=@markerColor " +
                                            "WHERE nictordb.dbo.Network.CoordIeeeAddress = @IeeeAddress;";
                    command.Parameters.Clear();
                    command.Parameters.AddWithValue("@description", nwk.description);
                    command.Parameters.AddWithValue("@markerColor", nwk.markerColor);
                    command.Parameters.AddWithValue("@IeeeAddress", nwk.ieeeAddress);
                    command.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    error = ex.Message;
                }
            }
        }

        public void UpdateNetworkLocation(Network nwk, out string error)
        {
            error = "";
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                try
                {
                    connection.Open();
                    SqlCommand command = connection.CreateCommand();
                    command.Connection = connection;
                    command.CommandText = "UPDATE nictordb.dbo.Network " +
                                            "SET nictordb.dbo.Network.Lat=@lat, nictordb.dbo.Network.Lng=@lng " +
                                            "WHERE nictordb.dbo.Network.CoordIeeeAddress = @IeeeAddress;";
                    command.Parameters.Clear();
                    command.Parameters.AddWithValue("@lat", nwk.lat);
                    command.Parameters.AddWithValue("@lng", nwk.lng);
                    command.Parameters.AddWithValue("@IeeeAddress", nwk.ieeeAddress);
                    command.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    error = ex.Message;
                }
            }
        }

        public void UpdateNodeLocation(Node node, out string error)
        {
            error = ""; 
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                try
                {
                    connection.Open();
                    SqlCommand command = connection.CreateCommand();
                    command.Connection = connection;
                    command.CommandText = "UPDATE nictordb.dbo.Node " +
                                            "SET nictordb.dbo.Node.Lat=@lat, nictordb.dbo.Node.Lng=@lng " +                                            
                                            "WHERE nictordb.dbo.Node.IeeeAddress = @IeeeAddress;";
                    command.Parameters.Clear();
                    command.Parameters.AddWithValue("@lat", node.lat);
                    command.Parameters.AddWithValue("@lng", node.lng);
                    command.Parameters.AddWithValue("@IeeeAddress", node.ieeeAddress);
                    command.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    error = ex.Message;
                }
            }
        }
        
        public List<NodeStatusItem> GetNodeStatus(string ieeeAddress, out string error)
        {
            error = "";
            List<NodeStatusItem> items = new List<NodeStatusItem>();
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                try
                {
                    connection.Open();
                    SqlCommand command = connection.CreateCommand();
                    command.Connection = connection;

                    // Get my local timeoffset
                    command.CommandText = "SELECT nictordb.dbo.Network.TimeOffset FROM nictordb.dbo.Network " +
                                            "INNER JOIN nictordb.dbo.Node ON nictordb.dbo.Network.Id=nictordb.dbo.Node.NetworkId " +
                                            "WHERE nictordb.dbo.Node.IeeeAddress=@IeeeAddress;";
                    command.Parameters.Clear();
                    command.Parameters.AddWithValue("@IeeeAddress", ieeeAddress);
                    SqlDataReader rdr0 = command.ExecuteReader();
                    int timeOffset = -9999;
                    while (rdr0.Read())
                    {
                        timeOffset = int.Parse(rdr0["TimeOffset"].ToString());
                    }
                    rdr0.Close();
                    if (timeOffset == -9999) { error = "Sorry, cannot process this request (reference 066718)";  return items; }


                    command.CommandText = "SELECT TOP 20 nictordb.dbo.NodeStatus.Id, nictordb.dbo.NodeStatus.NodeId, " +
                                            "nictordb.dbo.NodeStatus.RxTimeStamp, nictordb.dbo.NodeStatus.Battery, " +
                                            "nictordb.dbo.NodeStatus.Temperature FROM nictordb.dbo.NodeStatus " +
                                            "INNER JOIN nictordb.dbo.Node ON nictordb.dbo.NodeStatus.NodeId=nictordb.dbo.Node.Id " +
                                            "WHERE nictordb.dbo.Node.IeeeAddress = @IeeeAddress " + 
                                            "ORDER BY nictordb.dbo.NodeStatus.Id DESC;";
                    command.Parameters.Clear();
                    command.Parameters.AddWithValue("@IeeeAddress", ieeeAddress);
                    SqlDataReader rdr = command.ExecuteReader();
                    while (rdr.Read())
                    {
                        NodeStatusItem item = new NodeStatusItem();
                        item.batteryVoltage = double.Parse(rdr["Battery"].ToString());
                        item.temperature = double.Parse(rdr["Temperature"].ToString());
                        
                        DateTimeOffset dto = DateTimeOffset.Parse(rdr["RxTimeStamp"].ToString());
                        
                        DateTime utcTime = dto.UtcDateTime; // converted to UTC
                        TimeSpan ts = new TimeSpan(timeOffset, 0, 0);
                        utcTime = utcTime + ts;
                        //utcTime = utcTime.ToLocalTime();
                        // Create local time using time offset
                        //string format = "yyyy-MM-dd HH:mm:ss zzz";
                        string format = "yyyy-MM-dd HH:mm:ss";
                        item.timestamp = utcTime.ToString(format, null as DateTimeFormatInfo);

                        items.Add(item);
                    }
                    rdr.Close();
                }
                catch (Exception ex)
                {
                    error = ex.Message;
                }
            }
            return items;
        }

        public List<NetworkStatusItem> GetNetworkStatus(string ieeeAddress, out string error)
        {
            error = "";
            List<NetworkStatusItem> items = new List<NetworkStatusItem>();
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                try
                {
                    connection.Open();
                    SqlCommand command = connection.CreateCommand();
                    command.Connection = connection;
                    command.CommandText = "SELECT nictordb.dbo.NetworkStatus.Id, nictordb.dbo.NetworkStatus.NetworkId, " +
                                            "nictordb.dbo.NetworkStatus.RxTimeStamp, nictordb.dbo.NetworkStatus.Battery, " +
                                            "nictordb.dbo.NetworkStatus.Temperature, nictordb.dbo.NetworkStatus.NetworkState FROM nictordb.dbo.NetworkStatus " +
                                            "INNER JOIN nictordb.dbo.Network ON nictordb.dbo.NetworkStatus.NetworkId=nictordb.dbo.Network.Id " +
                                            "WHERE nictordb.dbo.Network.CoordIeeeAddress = @IeeeAddress ORDER BY nictordb.dbo.NetworkStatus.Id ASC;";
                    command.Parameters.Clear();
                    command.Parameters.AddWithValue("@IeeeAddress", ieeeAddress);
                    SqlDataReader rdr = command.ExecuteReader();
                    while (rdr.Read())
                    {
                        NetworkStatusItem item = new NetworkStatusItem();
                        item.batteryVoltage = double.Parse(rdr["Battery"].ToString());
                        item.temperature = double.Parse(rdr["Temperature"].ToString());
                        item.timestamp = rdr["RxTimeStamp"].ToString();
                        item.isNetworkActive = (int.Parse(rdr["NetworkState"].ToString()) == 1);
                        items.Add(item);
                    }
                    rdr.Close();
                }
                catch (Exception ex)
                {
                    error = ex.Message;
                }
            }
            return items;
        }

        public void UpdateSleepParameters(string coordIeeeAddress, int beaconInterval, int wakeInterval, out string error)
        {
            error = "";
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                try
                {
                    connection.Open();
                    SqlCommand command = connection.CreateCommand();
                    command.Connection = connection;
                    command.CommandText = "INSERT INTO nictordb.dbo.CommandAlt (CommandId, Txd, DestinationAddress,CommandPayload) " +
                                            "VALUES (@CommandId, 0, @DestinationAddress,@CommandPayload);";
                    command.Parameters.Clear();
                    command.Parameters.AddWithValue("@CommandId", NictorCommandIds.NICTOR_SLEEP_UPDATE_CMD_ID);
                    command.Parameters.AddWithValue("@DestinationAddress", coordIeeeAddress);

                    // Create byte command
                    byte[] beaconIntervalBytes = new byte[2];
                    byte[] wakeIntervalBytes = new byte[2];

                    beaconIntervalBytes = BitConverter.GetBytes((short)beaconInterval);
                    wakeIntervalBytes = BitConverter.GetBytes((short)wakeInterval);

                    byte[] byteCommand = new byte[4];
                    byteCommand[0] = beaconIntervalBytes[0];
                    byteCommand[1] = beaconIntervalBytes[1];
                    byteCommand[2] = wakeIntervalBytes[0];
                    byteCommand[3] = wakeIntervalBytes[1];

                    // Save byte command to string
                    command.Parameters.AddWithValue("@CommandPayload", BitConverter.ToString(byteCommand));
                    command.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    error = ex.Message;
                }
            }
        }

        public void OpenValve(string coordIeeeAddress, string nodeIeeeAddress, out string error)
        {
            error = "";
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                try
                {
                    connection.Open();
                    SqlCommand command = connection.CreateCommand();
                    command.Connection = connection;
                    command.CommandText = "INSERT INTO nictordb.dbo.CommandAlt (CommandId, Txd, DestinationAddress,CommandPayload) " +
                                            "VALUES (@CommandId, 0, @DestinationAddress,@CommandPayload);";
                    command.Parameters.Clear();
                    command.Parameters.AddWithValue("@CommandId", NictorCommandIds.NICTOR_ACTUATION_CMD_ID);
                    command.Parameters.AddWithValue("@DestinationAddress", coordIeeeAddress);

                    // Create byte command
                    byte[] byteCommand = new byte[2];
                    byteCommand[0] = Convert.ToByte(nodeIeeeAddress.Substring(14, 2), 16);  // LSB in nodeIeeeAddress
                    byteCommand[1] = Convert.ToByte(NictorActuationCommands.OPEN_VALVE);

                    // Save byte command to string
                    command.Parameters.AddWithValue("@CommandPayload", BitConverter.ToString(byteCommand));
                    command.ExecuteNonQuery();

                    command.CommandText = "UPDATE nictordb.dbo.Node " +
                                            "SET nictordb.dbo.Node.RelayIsOpen=@RelayIsOpen " +
                                            "WHERE nictordb.dbo.Node.IeeeAddress = @IeeeAddress;";
                    command.Parameters.Clear();
                    command.Parameters.AddWithValue("@RelayIsOpen", true);
                    command.Parameters.AddWithValue("@IeeeAddress", nodeIeeeAddress);
                    command.ExecuteNonQuery(); 
                }
                catch (Exception ex)
                {
                    error = ex.Message;
                }
            }
        }

        public bool IsValveOpen(string ieeeAddress, out string error)
        {
            error = "There was error processing this request.";
            bool isOpen = false;
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                try
                {
                    connection.Open();
                    SqlCommand command = connection.CreateCommand();
                    command.Connection = connection;
                    command.CommandText = "SELECT nictordb.dbo.Node.RelayIsOpen FROM nictordb.dbo.Node WHERE nictordb.dbo.Node.IeeeAddress = @IeeeAddress;";
                    command.Parameters.Clear();
                    command.Parameters.AddWithValue("@IeeeAddress", ieeeAddress);
                    SqlDataReader rdr = command.ExecuteReader();
                    while (rdr.Read())
                    {
                        isOpen = bool.Parse(rdr["RelayIsOpen"].ToString());
                        error = "";
                    }
                    rdr.Close();
                }
                catch (Exception ex)
                {
                    error = ex.Message;
                }
            }
            return isOpen;
        }

        public bool IsAnyValveOpen(out string error)
        {
            error = "There was error processing this request.";
            bool isOpen = false;
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                try
                {
                    connection.Open();
                    SqlCommand command = connection.CreateCommand();
                    command.Connection = connection;
                    command.CommandText = "SELECT nictordb.dbo.Node.RelayIsOpen FROM nictordb.dbo.Node;";
                    command.Parameters.Clear();
                    SqlDataReader rdr = command.ExecuteReader();
                    while (rdr.Read())
                    {
                        if (bool.Parse(rdr["RelayIsOpen"].ToString())) { isOpen = true; error = ""; break; }
                        error = "";
                    }
                    rdr.Close();
                }
                catch (Exception ex)
                {
                    error = ex.Message;
                }
            }
            return isOpen;
        }
        
        public void CloseValve(string coordIeeeAddress, string nodeIeeeAddress, out string error)
        {
            error = "";
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                try
                {
                    connection.Open();
                    SqlCommand command = connection.CreateCommand();
                    command.Connection = connection;
                    command.CommandText = "INSERT INTO nictordb.dbo.CommandAlt (CommandId, Txd, DestinationAddress,CommandPayload) " +
                                            "VALUES (@CommandId, 0, @DestinationAddress,@CommandPayload);";
                    command.Parameters.Clear();
                    command.Parameters.AddWithValue("@CommandId", NictorCommandIds.NICTOR_ACTUATION_CMD_ID);
                    command.Parameters.AddWithValue("@DestinationAddress", coordIeeeAddress);

                    // Create byte command
                    byte[] byteCommand = new byte[2];
                    byteCommand[0] = Convert.ToByte(nodeIeeeAddress.Substring(14, 2), 16);  // LSB in nodeIeeeAddress
                    byteCommand[1] = Convert.ToByte(NictorActuationCommands.CLOSE_VALVE);

                    // Save byte command to string
                    command.Parameters.AddWithValue("@CommandPayload", BitConverter.ToString(byteCommand));
                    command.ExecuteNonQuery();

                    command.CommandText = "UPDATE nictordb.dbo.Node " +
                                            "SET nictordb.dbo.Node.RelayIsOpen=@RelayIsOpen " +
                                            "WHERE nictordb.dbo.Node.IeeeAddress = @IeeeAddress;";
                    command.Parameters.Clear();
                    command.Parameters.AddWithValue("@RelayIsOpen", false);
                    command.Parameters.AddWithValue("@IeeeAddress", nodeIeeeAddress);
                    command.ExecuteNonQuery(); 
                }
                catch (Exception ex)
                {
                    error = ex.Message;
                }
            }
        }

        public void StartNetwork(string ieeeAddress, out string error)
        {
            error = "";
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                try
                {
                    connection.Open();
                    SqlCommand command = connection.CreateCommand();
                    command.Connection = connection;
                    command.CommandText = "INSERT INTO nictordb.dbo.CommandAlt (CommandId, Txd, DestinationAddress) " +
                                            "VALUES (@CommandId, 0, @DestinationAddress);";
                    command.Parameters.Clear();
                    command.Parameters.AddWithValue("@CommandId", NictorCommandIds.NICTOR_ACTIVE_MODE_CMD_ID);
                    command.Parameters.AddWithValue("@DestinationAddress", ieeeAddress);
                    command.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    error = ex.Message;
                }
            }            
        }

        public void StopNetwork(string ieeeAddress, out string error)
        {
            error = "";
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                try
                {
                    connection.Open();
                    SqlCommand command = connection.CreateCommand();
                    command.Connection = connection;
                    command.CommandText = "INSERT INTO nictordb.dbo.CommandAlt (CommandId, Txd, DestinationAddress) " +
                                            "VALUES (@CommandId, 0, @DestinationAddress);";
                    command.Parameters.Clear();
                    command.Parameters.AddWithValue("@CommandId", NictorCommandIds.NICTOR_IDLE_MODE_CMD_ID);
                    command.Parameters.AddWithValue("@DestinationAddress", ieeeAddress);
                    command.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    error = ex.Message;
                }
            }
        }

        public List<NictorCommand> GetCommandList(out string error) {
            error = "";
            List<NictorCommand> listOfCommands = new List<NictorCommand>();
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                try
                {
                    connection.Open();
                    SqlCommand command = connection.CreateCommand();
                    command.Connection = connection;
                    command.CommandText = "SELECT TOP 15 nictordb.dbo.CommandAlt.CommandId, " +
                                        "nictordb.dbo.CommandAlt.DestinationAddress, " +
                                        "nictordb.dbo.CommandAlt.Timestamp, nictordb.dbo.CommandAlt.Txd " + 
                                        "FROM nictordb.dbo.CommandAlt " + 
                                        "ORDER BY nictordb.dbo.CommandAlt.Timestamp DESC;";
                    SqlDataReader rdr = command.ExecuteReader();
                    while (rdr.Read())
                    {
                        NictorCommand cmd = new NictorCommand();
                        cmd.commandId = int.Parse(rdr["CommandId"].ToString());
                        cmd.ieeeAddress = rdr["DestinationAddress"].ToString();
                        cmd.timestamp = rdr["Timestamp"].ToString();
                        cmd.txd = int.Parse(rdr["Txd"].ToString());
                        listOfCommands.Add(cmd);
                    }
                    rdr.Close();
                }
                catch (Exception ex)
                {
                    error = ex.Message;
                }
            }
            return listOfCommands;
        }

         
        /// <summary>
        /// Get the oldest command
        /// </summary>
        /// <param name="coordIeeeAddress"></param>
        /// <param name="error"></param>
        /// <returns></returns>
        public string GetNextCommand(string coordIeeeAddress, out string error) {
            
            List<string> nodes = new List<string>();
            List<int> commandIds = new List<int>();
            nodes.Add(coordIeeeAddress);

            // [CMD TYPE, CMD ID, ERROR CODE, [LSB IEEE ADD, CMD] ... ]
            byte[] byteCommand = new byte[27];
            int commandBytes = 1;
            byteCommand[0] = 0x01;

            error = "";
            string resp = "";
            NictorCommand nictorCommand = new NictorCommand();

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                try
                {
                    connection.Open();
                    SqlCommand command = connection.CreateCommand();
                    command.Connection = connection;

                    // Get network id for this coordIeeeAddress
                    command.CommandText = "SELECT nictordb.dbo.Network.Id FROM nictordb.dbo.Network WHERE nictordb.dbo.Network.CoordIeeeAddress=@CoordIeeeAddress";
                    command.Parameters.Clear();
                    command.Parameters.AddWithValue("@CoordIeeeAddress", coordIeeeAddress);
                    SqlDataReader rdr0 = command.ExecuteReader();
                    int? networkId = null;
                    while (rdr0.Read())
                    {
                        networkId = int.Parse(rdr0["Id"].ToString());
                    }
                    rdr0.Close();

                    if (networkId != null)
                    {
                        // Get all nodes with this network id
                        command.CommandText = "SELECT nictordb.dbo.Node.IeeeAddress " + 
                                                "FROM nictordb.dbo.Node WHERE nictordb.dbo.Node.NetworkId=@NetworkId";
                        command.Parameters.Clear();
                        command.Parameters.AddWithValue("@NetworkId", networkId);                        
                        SqlDataReader rdr3 = command.ExecuteReader();                       
                        while (rdr3.Read())
                        {
                            string ieeeAddress = rdr3["IeeeAddress"].ToString();
                            nodes.Add(ieeeAddress);
                        }
                        rdr3.Close();

                        // For coord AND each child get oldest command that has not been actioned and set it to actioned
                        // Do coordinator fist, all other nodes are in the payload section
                        command.CommandText = "SELECT TOP 1 nictordb.dbo.CommandAlt.Id, nictordb.dbo.CommandAlt.Timestamp, " +
                                                "nictordb.dbo.CommandAlt.Txd, nictordb.dbo.CommandAlt.Ackd, " +
                                                "nictordb.dbo.CommandAlt.CommandId, nictordb.dbo.CommandAlt.DestinationAddress " +
                                                "FROM nictordb.dbo.CommandAlt WHERE " +
                                                "nictordb.dbo.CommandAlt.DestinationAddress=@Address AND nictordb.dbo.CommandAlt.Txd=0 " +
                                                "ORDER BY nictordb.dbo.CommandAlt.Timestamp";
                        command.Parameters.Clear();
                        command.Parameters.AddWithValue("@Address", nodes[0]);
                        SqlDataReader rdr1 = command.ExecuteReader();
                        while (rdr1.Read())
                        {
                            // Get data
                            int cmdId = int.Parse(rdr1["CommandId"].ToString());
                            byteCommand[commandBytes++] = Convert.ToByte(cmdId);
                            byteCommand[commandBytes++] = 0x00; // error code
                            commandIds.Add(int.Parse(rdr1["Id"].ToString()));
                        }
                        rdr1.Close();

                        // Now get commands for any children nodes.
                        for (int k = 1; k < nodes.Count; k++)
                        {
                            command.CommandText = "SELECT TOP 1 nictordb.dbo.CommandAlt.Id, nictordb.dbo.CommandAlt.Timestamp, " +
                                                    "nictordb.dbo.CommandAlt.Txd, nictordb.dbo.CommandAlt.Ackd, " +
                                                    "nictordb.dbo.CommandAlt.CommandId, nictordb.dbo.CommandAlt.DestinationAddress " +
                                                    "FROM nictordb.dbo.CommandAlt WHERE " +
                                                    "nictordb.dbo.CommandAlt.DestinationAddress=@Address AND nictordb.dbo.CommandAlt.Txd=0 " +
                                                    "ORDER BY nictordb.dbo.CommandAlt.Timestamp";
                            command.Parameters.Clear();
                            command.Parameters.AddWithValue("@Address", nodes[k]);

                            SqlDataReader rdr2 = command.ExecuteReader();
                            
                            while (rdr2.Read())
                            {
                                // Get data
                                string ieeeAddress = nodes[k];
                                int cmdId = int.Parse(rdr2["CommandId"].ToString());
                                byteCommand[commandBytes++] = byte.Parse(ieeeAddress.Substring(14, 2), System.Globalization.NumberStyles.HexNumber);
                                byteCommand[commandBytes++] = Convert.ToByte(cmdId);

                                if (commandBytes >= 27)
                                {
                                    break;
                                }
                                commandIds.Add(int.Parse(rdr2["Id"].ToString()));
                            }
                            rdr2.Close();

                            // Set the relevant commands to txd
                            foreach (int id in commandIds)
                            {
                                command.CommandText = "UPDATE nictordb.dbo.CommandAlt " +
                                                        "SET nictordb.dbo.CommandAlt.Txd=1 " +
                                                        "WHERE nictordb.dbo.CommandAlt.Id = @Id";
                                command.Parameters.Clear();
                                command.Parameters.AddWithValue("@Id", id);
                                command.ExecuteNonQuery();
                            }
                        }

                        resp = BitConverter.ToString(byteCommand).Replace("-", "");
                    }
                }
                catch (Exception ex)
                {
                    error = ex.Message;
                }
                return resp;
            }
        }

        /// <summary>
        /// Get the oldest command
        /// </summary>
        /// <param name="coordIeeeAddress"></param>
        /// <param name="error"></param>
        /// <returns></returns>
        public string GetNextCommandAlt(string coordIeeeAddress, out string error)
        {
            List<int> commandIds = new List<int>();
            int actionedCommandId = -1;

            // [CMD TYPE, CMD ID, ERROR CODE, [LSB IEEE ADD, CMD] ... ]
            byte[] byteCommand = new byte[27];
            int commandBytes = 1;
            byteCommand[0] = 0x01;

            error = "";
            string resp = "";
            NictorCommand nictorCommand = new NictorCommand();

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                try
                {
                    connection.Open();
                    SqlCommand command = connection.CreateCommand();
                    command.Connection = connection;
                    // Get the oldest command for this coordinator, that has not been actioned (Txd = 0)
                    command.CommandText = "SELECT TOP 1 nictordb.dbo.CommandAlt.Id, nictordb.dbo.CommandAlt.Timestamp, " +
                                            "nictordb.dbo.CommandAlt.Txd, nictordb.dbo.CommandAlt.Ackd, " +
                                            "nictordb.dbo.CommandAlt.CommandId, nictordb.dbo.CommandAlt.DestinationAddress, " +
                                            "nictordb.dbo.CommandAlt.CommandPayload " +
                                            "FROM nictordb.dbo.CommandAlt WHERE " +
                                            "nictordb.dbo.CommandAlt.DestinationAddress=@Address AND nictordb.dbo.CommandAlt.Txd=0 " +
                                            "ORDER BY nictordb.dbo.CommandAlt.Id ASC";
                    command.Parameters.Clear();
                    command.Parameters.AddWithValue("@Address", coordIeeeAddress);
                    SqlDataReader rdr1 = command.ExecuteReader();
                    while (rdr1.Read())
                    {
                        // Get data
                        int cmdId = int.Parse(rdr1["CommandId"].ToString());
                        byteCommand[commandBytes++] = Convert.ToByte(cmdId);
                        byteCommand[commandBytes++] = 0x00; // error code

                        // If there is payload add it here...
                        string commandPayload = rdr1["CommandPayload"].ToString().Replace("-","");

                        if (commandPayload != "")
                        {
                            Byte[] bytes = System.Linq.Enumerable.Range(0, commandPayload.Length)
                                                        .Where(x => x % 2 == 0)
                                                        .Select(x => Convert.ToByte(commandPayload.Substring(x, 2), 16))
                                                        .ToArray();
                            
                            for (int k = 0; k < bytes.Length; k++)
                            {
                                byteCommand[commandBytes++] = bytes[k];
                            }
                        }

                        if (commandBytes >= 27)
                        {
                            break;
                        }
                        actionedCommandId = int.Parse(rdr1["Id"].ToString());
                    }
                    rdr1.Close();

                    command.CommandText = "UPDATE nictordb.dbo.CommandAlt " +
                                            "SET nictordb.dbo.CommandAlt.Txd=1 " +
                                            "WHERE nictordb.dbo.CommandAlt.Id = @Id";
                    command.Parameters.Clear();
                    command.Parameters.AddWithValue("@Id", actionedCommandId);
                    command.ExecuteNonQuery();

                    resp = BitConverter.ToString(byteCommand).Replace("-", "");
                }
                catch (Exception ex)
                {
                    error = ex.Message;
                }
                return resp;
            }
        }

        public void InsertMeasurement(string ieeeAddress, string data, out string error)
        {
            error = "";
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                try {
                    // Get nodeId from ieeeAddress
                    connection.Open();
                    SqlCommand command = connection.CreateCommand();
                    command.Connection = connection;
                    command.CommandText = "SELECT nictordb.dbo.Node.Id FROM nictordb.dbo.Node " +
                                            "WHERE nictordb.dbo.Node.IeeeAddress = @IeeeAddress;";
                    command.Parameters.Clear();
                    command.Parameters.AddWithValue("@IeeeAddress", ieeeAddress);
                    SqlDataReader rdr = command.ExecuteReader();
                    int? nodeId = null;
                    while (rdr.Read())
                    {
                        nodeId = int.Parse(rdr["Id"].ToString());
                    }
                    rdr.Close();

                    // Insert the sensor
                    if (nodeId != null)
                    {
                        command.CommandText = "INSERT INTO nictordb.dbo.Measurement " +
                                                "(NodeId, Data) " +
                                                "VALUES " +
                                                "(@NodeId, @Data);";
                        command.Parameters.Clear();
                        command.Parameters.AddWithValue("@NodeId", nodeId);
                        command.Parameters.AddWithValue("@Data", data);
                        command.ExecuteNonQuery();
                    }
                }
                catch (Exception ex) {
                    error = ex.Message;
                }
            }
        }

        public List<Measurement> SelectMeasurement(string ieeeAddress, string startDate, string endDate, out string measType, out string error)
        {
            List<Measurement> listOfMeas = new List<Measurement>();
            error = "";
            measType = "";
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                try
                {
                    // Get nodeId from ieeeAddress
                    connection.Open();
                    SqlCommand command = connection.CreateCommand();
                    command.Connection = connection;
                    // Get my local timeoffset
                    command.CommandText = "SELECT nictordb.dbo.Network.TimeOffset FROM nictordb.dbo.Network " +
                                            "INNER JOIN nictordb.dbo.Node ON nictordb.dbo.Network.Id=nictordb.dbo.Node.NetworkId " +
                                            "WHERE nictordb.dbo.Node.IeeeAddress=@IeeeAddress;";
                    command.Parameters.Clear();
                    command.Parameters.AddWithValue("@IeeeAddress", ieeeAddress);
                    SqlDataReader rdr0 = command.ExecuteReader();
                    int timeOffset = -9999;
                    while (rdr0.Read())
                    {
                        timeOffset = int.Parse(rdr0["TimeOffset"].ToString());
                    }
                    rdr0.Close();
                    if (timeOffset == -9999) { error = "Sorry, cannot process this request (reference 03748)"; return listOfMeas; }

                    command.CommandText = "SELECT nictordb.dbo.Node.Id, nictordb.dbo.Node.NodeType FROM nictordb.dbo.Node " +
                                            "WHERE nictordb.dbo.Node.IeeeAddress = @IeeeAddress;";
                    command.Parameters.Clear();
                    command.Parameters.AddWithValue("@IeeeAddress", ieeeAddress);
                    SqlDataReader rdr = command.ExecuteReader();
                    int? nodeId = null;
                    string nodeType = "";
                    while (rdr.Read())
                    {
                        nodeId = int.Parse(rdr["Id"].ToString());
                        nodeType = rdr["NodeType"].ToString();
                    }
                    rdr.Close();
                    if (nodeType == "shule_ec20")
                    {
                        measType = "soil_moisture";
                    }
                    else if (nodeType == "shule_flow")
                    {
                        measType = "flow";
                    }
                    else { measType = "unknown"; }

                    // Select the data
                    if (nodeId != null)
                    {
                        command.CommandText = "SELECT TOP 500 nictordb.dbo.Measurement.RxTimeStamp, nictordb.dbo.Measurement.Data FROM nictordb.dbo.Measurement " +
                                                "WHERE nictordb.dbo.Measurement.NodeID = @Id " +
                                                "ORDER BY nictordb.dbo.Measurement.RxTimeStamp ASC;";
                        command.Parameters.Clear();
                        command.Parameters.AddWithValue("@Id", nodeId);
                        rdr = command.ExecuteReader();
                        double lastFlow = 0;
                        bool isFirstFlowMeas = true;
                        while (rdr.Read())
                        {
                            Measurement meas = new Measurement();
                            string temp = rdr["Data"].ToString().Replace("-","");
                            if (measType == "soil_moisture")
                            {
                                Byte[] msg = Enumerable.Range(0, temp.Length)
                                                .Where(x => x % 2 == 0)
                                                .Select(x => Convert.ToByte(temp.Substring(x, 2), 16))
                                                .ToArray();
                                ushort measured = (ushort)(((msg[1] & 0x00FF) << 8) | (msg[0] & 0x00FF));
                                ushort excitation = (ushort)(((msg[3] & 0x00FF) << 8) | (msg[2] & 0x00FF));
                                meas.data = (Convert.ToDouble(measured)/Convert.ToDouble(excitation))*100;
                                DateTimeOffset dto = DateTimeOffset.Parse(rdr["RxTimeStamp"].ToString());

                                DateTime utcTime = dto.UtcDateTime; // converted to UTC
                                TimeSpan ts = new TimeSpan(timeOffset, 0, 0);
                                utcTime = utcTime + ts;
                                // Create local time using time offset
                                string format = "yyyy-MM-dd HH:mm:ss";
                                meas.timestamp = utcTime.ToString(format, null as DateTimeFormatInfo);

                                //string format = "yyyy-MM-dd HH:mm:ss zzz";
                                //meas.timestamp = dto.ToString(format, null as DateTimeFormatInfo);

                                listOfMeas.Add(meas);
                            }
                            else if (measType == "flow")
                            {
                                Byte[] msg = Enumerable.Range(0, temp.Length)
                                                .Where(x => x % 2 == 0)
                                                .Select(x => Convert.ToByte(temp.Substring(x, 2), 16))
                                                .ToArray();
                                ulong msb = (ulong)(((msg[7] & 0x00000000000000FF) << 56) | ((msg[6] & 0x00000000000000FF) << 48) | ((msg[5] & 0x00000000000000FF) << 40) | ((msg[4] & 0x00000000000000FF) << 32) | ((msg[3] & 0x00000000000000FF) << 24) | ((msg[2] & 0x00000000000000FF) << 16) | ((msg[1] & 0x00000000000000FF) << 8) | (msg[0] & 0x00000000000000FF));
                                if (isFirstFlowMeas)
                                {
                                    lastFlow = Convert.ToDouble(msb);
                                    isFirstFlowMeas = false;
                                }
                                else {
                                    if (Convert.ToDouble(msb) > lastFlow) 
                                        meas.data = 1;
                                    else 
                                        meas.data = 0;

                                    lastFlow = Convert.ToDouble(msb);
                                    DateTimeOffset dto = DateTimeOffset.Parse(rdr["RxTimeStamp"].ToString());
                                    DateTime utcTime = dto.UtcDateTime; // converted to UTC
                                    TimeSpan ts = new TimeSpan(timeOffset, 0, 0);
                                    utcTime = utcTime + ts;
                                    // Create local time using time offset
                                    string format = "yyyy-MM-dd HH:mm:ss";
                                    meas.timestamp = utcTime.ToString(format, null as DateTimeFormatInfo);

                                    //string format = "yyyy-MM-dd HH:mm:ss zzz";
                                    //meas.timestamp = dto.ToString(format, null as DateTimeFormatInfo);

                                    listOfMeas.Add(meas);
                                }
                            }

                        }
                        rdr.Close();
                    }
                }
                catch (Exception ex)
                {
                    error = ex.Message;
                }
                return listOfMeas;
            }
        }

        public List<List<Measurement>> SelectNetworkMeasurement(string coordIeeeAddress, string startDate, string endDate, out string error)
        {
            List<List<Measurement>> listOfAllMeas = new List<List<Measurement>>();
            error = "";
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                try
                {
                    // Get networkId from coordIeeeAddress
                    int? networkId = null;
                    connection.Open();
                    SqlCommand command = connection.CreateCommand();
                    command.Connection = connection;
                    command.CommandText = "SELECT nictordb.dbo.Network.Id FROM nictordb.dbo.Network " +
                                            "WHERE nictordb.dbo.Network.CoordIeeeAddress = @CoordIeeeAddress;";
                    command.Parameters.Clear();
                    command.Parameters.AddWithValue("@CoordIeeeAddress", coordIeeeAddress);
                    SqlDataReader rdr = command.ExecuteReader();                    
                    while (rdr.Read())
                    {
                        networkId = int.Parse(rdr["Id"].ToString());
                    }
                    rdr.Close();

                    // Get all nodes in this network
                    if (networkId != null)
                    {
                        List<Node> allNodesInThisNetwork = new List<Node>();
                        command.CommandText = "SELECT Id, Description, NodeType, IeeeAddress FROM nictordb.dbo.Node " +
                                                "WHERE nictordb.dbo.Node.NetworkId = @NetworkId;";
                        command.Parameters.Clear();
                        command.Parameters.AddWithValue("@NetworkId", networkId);
                        rdr = command.ExecuteReader();
                        while (rdr.Read())
                        {
                            Node aNode = new Node();
                            aNode.id = int.Parse(rdr["Id"].ToString());
                            aNode.description = rdr["Description"].ToString();
                            aNode.nodeType = rdr["NodeType"].ToString();
                            aNode.ieeeAddress = rdr["IeeeAddress"].ToString();
                            allNodesInThisNetwork.Add(aNode);
                        }
                        rdr.Close();

                        if (allNodesInThisNetwork.Count > 0)
                        {
                            // Get data for all nodes
                            foreach (Node n in allNodesInThisNetwork)
                            {
                                List<Measurement> nodeMeas = new List<Measurement>();

                                command.CommandText = "SELECT TOP 100 nictordb.dbo.Measurement.RxTimeStamp, nictordb.dbo.Measurement.Data FROM nictordb.dbo.Measurement " +
                                                        "WHERE nictordb.dbo.Measurement.NodeID = @NodeId " +
                                                "ORDER BY nictordb.dbo.Measurement.RxTimeStamp ASC;";
                                command.Parameters.Clear();
                                command.Parameters.AddWithValue("@NodeId", n.id);
                                rdr = command.ExecuteReader();
                                while (rdr.Read())
                                {
                                    Measurement meas = new Measurement();
                                    meas.data = 1.0; // rdr["Data"].ToString();
                                    meas.timestamp = rdr["RxTimeStamp"].ToString();
                                    nodeMeas.Add(meas);
                                }
                                rdr.Close();
                                if (nodeMeas.Count > 0)
                                {
                                    listOfAllMeas.Add(nodeMeas);
                                }
                            }                            
                        }                       
                    }
                }
                catch (Exception ex)
                {
                    error = ex.Message;
                }
                return listOfAllMeas;
            }
        }

        public List<MarkerColor> GetMarkerColors(out string error)
        {
            error = "";
            List<MarkerColor> colors = new List<MarkerColor>();
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                try
                {
                    connection.Open();
                    SqlCommand command = connection.CreateCommand();
                    command.Connection = connection;
                    command.CommandText = "SELECT * FROM nictordb.dbo.MarkerColor;";
                    SqlDataReader rdr = command.ExecuteReader();
                    while (rdr.Read())
                    {
                        MarkerColor m = new MarkerColor();
                        m.color = rdr["Color"].ToString();
                        m.code = rdr["Code"].ToString();
                        colors.Add(m);
                    }
                    rdr.Close();
                }
                catch (Exception ex)
                {
                    error = ex.Message;
                }
            }
            return colors;
        }

        public DynamicIrrigationSchedule GetDynamicIrrigationSchedule(string actuatorIeeeAddress, out string error)
        {
            DynamicIrrigationSchedule dynamicIrrigationSchedule = new DynamicIrrigationSchedule();
            error = "";

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                try
                {
                    connection.Open();
                    SqlCommand command = connection.CreateCommand();
                    command.Connection = connection;

                    command.CommandText = "SELECT * FROM nictordb.dbo.Automation " +
                        "INNER JOIN nictordb.dbo.Node " +
                        "ON nictordb.dbo.Automation.ActuatorId = nictordb.dbo.Node.Id " +
                        "WHERE nictordb.dbo.Node.IeeeAddress = @ActuatorIeeeAddress;";
                    command.Parameters.Clear();
                    command.Parameters.AddWithValue("@ActuatorIeeeAddress", actuatorIeeeAddress);
                    SqlDataReader rdr = command.ExecuteReader();
                    int? sensorId = null;
                    int? actuatorId = null;
                    while (rdr.Read())
                    {
                        sensorId = int.Parse(rdr["SensorId"].ToString());
                        actuatorId = int.Parse(rdr["ActuatorId"].ToString());

                        dynamicIrrigationSchedule.ActuatorIeee = actuatorIeeeAddress;
                        dynamicIrrigationSchedule.IsOperating = bool.Parse(rdr["IsOperating"].ToString());
                        dynamicIrrigationSchedule.StopIrrigationLevel = double.Parse(rdr["StopIrrigationLevel"].ToString());
                        dynamicIrrigationSchedule.StartIrrigationLevel = double.Parse(rdr["StartIrrigationLevel"].ToString());

                    }
                    rdr.Close();

                    if ((sensorId != null) && (actuatorId != null))
                    {
                        command.CommandText = "SELECT nictordb.dbo.Node.IeeeAddress " +
                                                "WHERE nictor.dbo.Node.Id = @nodeId;";
                        command.Parameters.Clear();
                        command.Parameters.AddWithValue("@nodeId", sensorId);
                        SqlDataReader rdr1 = command.ExecuteReader();
                        while (rdr.Read())
                        {
                            dynamicIrrigationSchedule.SensorIeee = rdr["IeeeAddress"].ToString();
                        }
                    }
                }
                catch (Exception ex)
                {
                    error = ex.Message;
                }
            }

            return dynamicIrrigationSchedule;

        }

        public void InsertDynamicIrrigationSchedule(DynamicIrrigationSchedule dynamicIrrigationSchedule, out string error)
        {
            error = "";
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                try
                {
                    // Get ActuatorId and SensorId from ieeeAddress
                    int? actuatorId = null;
                    int? sensorId = null;

                    connection.Open();
                    SqlCommand command = connection.CreateCommand();
                    command.Connection = connection;
                    command.CommandText = "SELECT nictordb.dbo.Node.Id FROM nictordb.dbo.Node " +
                                            "WHERE nictordb.dbo.Node.IeeeAddress = @IeeeAddress;";
                    command.Parameters.Clear();
                    command.Parameters.AddWithValue("@IeeeAddress", dynamicIrrigationSchedule.ActuatorIeee);
                    SqlDataReader rdr = command.ExecuteReader();
                    while (rdr.Read())
                    {
                        actuatorId = int.Parse(rdr["Id"].ToString());
                    }
                    rdr.Close();

                    command.CommandText = "SELECT nictordb.dbo.Node.Id FROM nictordb.dbo.Node " +
                                            "WHERE nictordb.dbo.Node.IeeeAddress = @IeeeAddress;";
                    command.Parameters.Clear();
                    command.Parameters.AddWithValue("@IeeeAddress", dynamicIrrigationSchedule.SensorIeee);
                    while (rdr.Read())
                    {
                        sensorId = int.Parse(rdr["Id"].ToString());
                    }
                    rdr.Close();

                    if ((actuatorId != null) || (sensorId != null))
                    {
                        command.CommandText = "INSERT INTO nictordb.dbo.Automation " +
                                                "(ActuatorId, SensorId, IsOperating, StopIrrigationLevel, StartIrrigationLevel) " +
                                                "VALUES " +
                                                "(@ActuatorId, @SensorId, @IsOperating, @StopIrrigationLevel, @StartIrrigationLevel);";
                        command.Parameters.Clear();
                        command.Parameters.AddWithValue("@ActuatorId", actuatorId);
                        command.Parameters.AddWithValue("@SensorId", sensorId);
                        command.Parameters.AddWithValue("@IsOperating", true);
                        command.Parameters.AddWithValue("@StopIrrigationLevel", dynamicIrrigationSchedule.StopIrrigationLevel);
                        command.Parameters.AddWithValue("@StartIrrigationLevel", dynamicIrrigationSchedule.StartIrrigationLevel);
                        command.ExecuteNonQuery();
                    }
                }
                catch (Exception ex)
                {
                    error = ex.Message;
                }
            }
        }
        
        public void UpdateDynamicIrrigationSchedule(DynamicIrrigationSchedule dynamicIrrigationSchedule, out string error)
        {
            error = "";

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                try
                {
                    connection.Open();
                    SqlCommand command = connection.CreateCommand();
                    command.Connection = connection;                        
                    command.CommandText = "UPDATE nictordb.dbo.Automation " +
                                            "SET nictordb.dbo.Automation.IsOperating=@IsOperating, " +
                                            "nictordb.dbo.Automation.StopIrrigationLevel=@StopIrrigationLevel " +
                                            "nictordb.dbo.Automation.StartIrrigationLevel=@StartIrrigationLevel " +
                                            "INNER JOIN nictordb.dbo.Node " +
                                            "ON nictordb.dbo.Automation.ActuatorId = nictordb.dbo.Node.Id " +
                                            "WHERE nictordb.dbo.Node.IeeeAddress = @IeeeAddress;";
                    command.Parameters.Clear();
                    command.Parameters.AddWithValue("@IsOperating", dynamicIrrigationSchedule.IsOperating);
                    command.Parameters.AddWithValue("@StartIrrigationLevel", dynamicIrrigationSchedule.StartIrrigationLevel);
                    command.Parameters.AddWithValue("@StopIrrigationLevel", dynamicIrrigationSchedule.StopIrrigationLevel);
                    command.Parameters.AddWithValue("@IeeeAddress", dynamicIrrigationSchedule.ActuatorIeee);
                    command.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    error = ex.Message;
                }
            }
        }

        public HomeLocation GetHomeLocation(int userId, out string error)
        {
            HomeLocation homeLocn = new HomeLocation();
            error = "";

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                try
                {
                    connection.Open();
                    SqlCommand command = connection.CreateCommand();
                    command.Connection = connection;
                    command.CommandText = "SELECT HomeLat,HomeLng FROM nictordb.dbo.Membership " +
                        "WHERE nictordb.dbo.Membership.Id = @Id;";
                    command.Parameters.Clear();
                    command.Parameters.AddWithValue("@Id", userId);
                    SqlDataReader rdr = command.ExecuteReader();
                    while (rdr.Read())
                    {
                        double homeLat = double.Parse(rdr["HomeLat"].ToString());
                        double homeLng = double.Parse(rdr["HomeLng"].ToString());
                        homeLocn.Lat = homeLat;
                        homeLocn.Lng = homeLng;
                    }
                    rdr.Close();
                }
                catch (Exception ex)
                {
                    error = ex.Message;
                }
            }
            return homeLocn;
        }

        public void UpdateHomeLocation(int userId, double lat, double lng, out string error)
        {
            error = "";
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                try
                {
                    connection.Open();
                    SqlCommand command = connection.CreateCommand();
                    command.Connection = connection;
                    command.CommandText = "UPDATE nictordb.dbo.Membership " +
                                            "SET nictordb.dbo.Membership.HomeLat=@HomeLat, " +
                                            "nictordb.dbo.Membership.HomeLng=@HomeLng " +
                                            "WHERE nictordb.dbo.Membership.Id = @Id;";
                    command.Parameters.Clear();
                    command.Parameters.AddWithValue("@HomeLat", lat);
                    command.Parameters.AddWithValue("@HomeLng", lng);
                    command.Parameters.AddWithValue("@Id", userId);
                    command.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    error = ex.Message;
                }
            }
        }
    }
}