﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NictorWebClient
{
    public class Measurement
    {
        public string timestamp { get; set; }
        public double data { get; set; }
    }
}