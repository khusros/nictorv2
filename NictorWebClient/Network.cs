﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NictorWebClient
{
    public class Network
    {
        public int id { get; set; }
        public string ieeeAddress { get; set; }
        public string description { get; set; }
        public double lng { get; set; }
        public double lat { get; set; }
        public string markerColor { get; set; }
        public double latestBattery { get; set; }
        public string latestRxTimestamp { get; set; }
        public bool isNetworkActive { get; set; }
        public List<Node> nodes { get; set; }
    }
}