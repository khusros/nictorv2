﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NictorWebClient
{
    public class Node
    {
        public int id { get; set; }
        public string ieeeAddress { get; set; }
        public string description { get; set; }
        public string nodeType { get; set; }
        public double lng { get; set; }
        public double lat { get; set; }
        public double latestBattery { get; set; }
        public string latestRxTimestamp { get; set; }
        public bool relayIsOpen { get; set; }
    }
}