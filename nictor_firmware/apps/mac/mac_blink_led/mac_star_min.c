/*******************************************************************************
 *
 * Copyright (C) 2012 NICTA Ltd.
 *
 * The information contained in this material constitutes
 * trade secret of NICTA Ltd.  You are only authorized to use
 * such material for your personal evaluation purposes.  You
 * may only disclose such material to those of your full-time
 * employees having a need to know such information, and may
 * not disclose it to any other person. Such material is
 * provided purely for informational purposes and on an AS IS
 * basis without any warranty of any kind.  NICTA LTD. DOES
 * NOT GUARANTEE THE ACCURACY OR COMPLETENESS OF THE
 * INFORMATION PROVIDED AND DISCLAIMS ALL WARRANTIES, WHETHER
 * EXPRESS OR IMPLIED, INCLUDING, BUT NOT LIMITED TO WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE,
 * ACCURACY OR NON-INFRINGEMENT, WITH REGARD TO SUCH
 * INFORMATION OR THE RESULTS TO BE OBTAINED FROM ITS USE.
 * All content included in this material, such as its design,
 * text, graphics, and the selection and arrangements thereof,
 * is copyright (C) 2005 NICTA Ltd.
 *
 * NICTOR is a trademark owned by NICTA LTD. Any other
 * trademarks are owned by the entities referenced herein.
 * All rights are reserved.
 *
 *******************************************************************************
 *
 * mac_star_min.c
 *
 *
 *
 *
 * Revision (do not edit):
 *
 * $Id: mac_star_min.c,v 1.1 2013-05-23 03:02:19 ksaleem Exp $
 *
 *******************************************************************************/

/*! \file mac_star_min.c
 *  \brief
 *
 */


#include "mac_headers.h"
#include "eeprom_utility.h"
#include "uart.h"
#include "fifo.h"
#include "adc.h"
#include "hwtimer.h"
#include "battery.h"
#include "temperature.h"
#include <avr/wdt.h>
#include <avr/interrupt.h>
#include <util/crc16.h>
#include "cmd_interface.h"
#include "cmd_public.h"
#include "echo.h"
#include "rtclock.h"
#include <stdio.h>
#include <avr/interrupt.h>
#include <avr/sleep.h>
#include "globals.h"
#include <avr/power.h>
#include "flow.h"
#include <stdlib.h>

// Control commands
/* Command IDs
 */
#define NICTOR_NO_COMMAND_CMD_ID					(UINT8)0x00
#define NICTOR_OPEN_VALVE_CMD_ID                  	(UINT8)0xA0 // 160
#define NICTOR_CLOSE_VALVE_CMD_ID					(UINT8)0xA1 // 161


// THE FOLLOWING AREN'T DEFINED IN THE LIBRARY
#define WDTO_4S     8
#define WDTO_8S     9


#define RELAYS_ON_R1() \
    PORTC &= ~(0x02)
#define RELAYS_OFF_R1() \
    PORTC |= 0x02
#define RELAYS_ON_R2() \
    PORTC &= ~(0x01)
#define RELAYS_OFF_R2() \
    PORTC |= 0x01

#define COORDINATOR_NODE					FALSE
#define SENSOR_NODE							TRUE


#define SID									0xAABBCCDD11223303  // PRE-PROGRAMMED SENSOR ID (UNIQUE FOR LIFE OF DEVICE)
#define NID									0x11223344  		// PRE-PROGRAMMED NETWORK ID (UNIQUE)
#define PANID 								0x1234				// PRE-PROGRAMMED
#define COORD_SHORT 						0x5678				// PRE-PROGRAMMED
#define CHANNEL 							16					// PRE-PROGRAMMED
#define UNIQUE_802_15_4_BEACON_ID			NID					// PRE-PROGRAMMED (tbd should be nid)
#define ACL_ELEMENTS 3
QWORD pACL[ACL_ELEMENTS] = {0xAABBCCDD11223301, 0xAABBCCDD11223302, 0xAABBCCDD11223303};



/**********************************************************************************************************/
#define HAS_ECHO_SENSOR				 	TRUE
#define HAS_FLOW_SENSOR					FALSE
/**********************************************************************************************************/

#define MAX_CHILDREN 						4
#define AUTO_SCAN 							FALSE
#define BEACON_OTAMSG_ID    				1
#define DATA_OTAMSG_ID  					2
#define DATA_OTAMSG_SIZE 					50
#define BEACON_REBROADCAST_MSDU_HANDLE 		0
#define TRANSMIT_BEACON_MSDU_HANDLE 		1
#define DATA_OTAMSG_MSDU_HANDLE 			2
#define MAX_MISSED_BEACONS 					4
#define MAX_NO_PARENT_ACK 					2 // Should be greater than MAX_RETRIES below
#define MAX_INTERVAL_NO_CHILD_COMM			2
#define MAX_RETRIES 						100
#define MAX_OTA_MSG_LENGTH 					128
#define MAX_NORMAL_PAYLOAD_BYTES 			80// 50 // 127 - [2+1+2+8+2+8+0+x+2 + 8 (cipher (64 bit)) + 2 (crc) ]
#define MAX_BEACON_PAYLOAD_BYTES 			10
#define TOTAL_NORMAL_BYTES 					(MAX_NORMAL_PAYLOAD_BYTES + 8 + 2)
#define TOTAL_BEACON_BYTES 					(MAX_BEACON_PAYLOAD_BYTES + 8 + 2);
#define MAX_MEAS_BUFFER_SIZE 				127  // IEEE address + beacon seq number + 1
#define DEFAULT_NETWORK_INTERVAL_IN_MINUTES			2  // beacon interval in minutes
#define DEFAULT_NETWORK_WAKE_INTERVAL 				30 // (ALWAYS LESS THAN OR EQUAL TO 56)
#define IDLE_NETWORK_INTERVAL_IN_MINUTES			2  // beacon interval in minutes
#define IDLE_NETWORK_WAKE_INTERVAL 					30 // (ALWAYS LESS THAN OR EQUAL TO 56)
#define ACTIVE_NETWORK_INTERVAL_IN_MINUTES			2  // beacon interval in minutes
#define ACTIVE_NETWORK_WAKE_INTERVAL 				30 // (ALWAYS LESS THAN OR EQUAL TO 56)
#define FLOW_SENSOR_ACTIVE_NETWORK_WAKE_INTERVAL

#define NETWORK_STATE_ACTIVE				1
#define NETWORK_STATE_IDLE					0

#define MAX_ACTUATION_COMMANDS_PER_BEACON	12
#define BYTES_PER_ACTUATION_COMMAND			2
#define MAX_ACTUATION_COMMAND_BYTES			(MAX_ACTUATION_COMMANDS_PER_BEACON * BYTES_PER_ACTUATION_COMMAND)



UINT16 GNetworkIntervalInMinutes = IDLE_NETWORK_INTERVAL_IN_MINUTES;
UINT16 GNetworkWakeIntervalInSeconds = IDLE_NETWORK_WAKE_INTERVAL;
BYTE GNetworkState = NETWORK_STATE_IDLE;

BYTE pActuationCommands[MAX_ACTUATION_COMMAND_BYTES];

QWORD getExtendedAddress(void);

UINT16 minutesElapsedSinceOpenValve = 0;
BOOL isValveOpen = FALSE;

void OpenValve() {
	minutesElapsedSinceOpenValve = 0;
	isValveOpen = TRUE;
	RELAYS_ON_R1();
	halWait(65000);
	RELAYS_OFF_R1();
}

void CloseValve() {
	minutesElapsedSinceOpenValve = 0;
	isValveOpen = FALSE;
	RELAYS_ON_R2();
	halWait(65000);
	RELAYS_OFF_R2();
}

// Turn watchdog off
void WDT_off(void)
{
    cli();
    wdt_reset();
    MCUSR &= ~(1<<WDRF);
    WDTCSR |= (1<<WDCE) | (1<<WDE);
    WDTCSR = 0x00;
    sei();
}


void HW_init( void )
{
    /*
     * Globally disable interrupts.
     */
    cli();

    /**************  Set I/O ports direction registers  *******************/

    /***    PORTA   ***/
    /*
        PA7     GREEN2_LED     OP: ACTIVE LO
        PA6     RED2_LED       OP: ACTIVE LO
        PA5     TEMP_SCL       OP
        PA4     ANALOG_PWR_DN  OP  LO -> POWERED UP
        PA3     TEMP_SDA       OP (Bidirectional)
        PA2     LINK2          IP
        PA1     LINK1          IP
        PA0     LINK0          IP
    */

    PORTA |= 0xF8;    // set OP bits high
    DDRA = 0xF8;


    /***    PORTB   ***/
    /*
        PB7     FLASH_RESET     OP: ACTIVE LO
        PB6     CC2420_RESET    OP: ACTIVE LO
        PB5     CC2420_VREG_EN  OP: ACTIVE HI
        PB4     FLASH_CS        OP: ACTIVE LO
        PB3     MISO            IP
        PB2     MOSI            OP
        PB1     SCK             OP
        PB0     CC2420_CS       OP: ACTIVE LO
    */

    //PORTB |= 0xD7; //set ops hi
    //DDRB = 0xF7;

    /***    PORTC   ***/
    /*
        PC7     FORCE_ON_RS232   OP: ACTIVE HI  (X if FORCE_OFF is LO)
        PC6     FORCE_OFF_RS232  OP: ACTIVE LO
        PC5     GREEN1_LED       OP: ACTIVE LO
        PC4     YELLOW_LED       OP: ACTIVE LO
        PC3     ORANGE_LED       OP: ACTIVE LO
        PC2     RED1_LED         OP: ACTIVE LO
        PC1     RELAY1           OP: ACTIVE LO
        PC0     RELAY2           OP: ACTIVE LO
    */

	PORTC = 0x3F;
    DDRC = 0xFF;         //all ops


    /***    PORTD   ***/
    /*
        PD7     ECHO_OFF         OP: ACTIVE LO
        PD6     CCA              IP
        PD5     DIN3             IP
        PD4     SFD              IP
        PD3     TXD1             OP
        PD2     RXD1             IP
        PD1     FIFO             IP
        PD0     FIFOP            IP
    */

    PORTD = 0x88;       	//set ECHO_OFF HI
    DDRD = 0x88;        		 //was 0x80 28/3/6

    /***    PORTE   ***/
    /*
        PE7     DIN1             IP
        PE6     DIN0             IP
        PE5     INVALID_RS232    IP
        PE4     INT_RTC          IP
        PE3     DF_SI            OP
        PE2     DF_SO            IP
        PE1     TXD0             OP
        PE0     RXD0             IP
    */

    //PORTE |= 0x40;
    //DDRE &= 0xBF;

    /***    PORTF   ***/
    /*
        PF7     TDI
        PF6     TDO
        PF5     TMS
        PF4     TCK
        PF3     ADC3             IP
        PF2     ADC2             IP
        PF1     ADC1             IP
        PF0     ADC0             IP
    */

    DDRF &= ~(0x0F);

    /***    PORTG   ***/
    /*
        PG4     DIN2                IP
        PG3     DF_SCLK             OP:  HI
        PG2     RS232ENABLE         OP:  ACTIVE LO
        PG1     SCL_RTC             OP
        PG0     SDA_RTC             OP  (BIDIRECTIONAL)  //Note SDA, SCL were transposed here 28/3/6

    */

    PORTG = 0x0C;
    DDRG = 0x0F;

    // For reasons TBD, the analog section needs to be powered UP briefly on startup.
    PWR_UP_ANALOG();
    for (UINT8 k=0; k<100; k++) {
        UINT16 myTimeout = 1000;
        do {
            asm volatile("nop"::);
            asm volatile("nop"::);
            asm volatile("nop"::);
            asm volatile("nop"::);
        } while (--myTimeout);
    }
    //PWR_DN_ANALOG();

    /*
     * POSTs.
     */


    /*
     * We should go to main() after here.
     */

    /*
     * Globally enable interrtups.
     */
    sei();

    return;
}




typedef struct
ChildrenStruct
{
    BOOL free;
    UINT16 shortAddress;
    UINT8 missedPeriods;
    QWORD pAssociatedExtAddress;
    UINT8 numIntervalsNoCommunication;
} Children;



// The extended address of the coordinator
QWORD coordExtAddr;
// The PAN ID
WORD panId;
// Scan results (can overlap with the beacon payload or the MSDU buffer)
MAC_SCAN_RESULT scanResult;
// The payload of the packets transmitted by the device
BYTE pBuffer[MAX_OTA_MSG_LENGTH];
BYTE pBuffer1[1200]; // Should be renamed
BYTE pBeaconPayload[32];
BOOL GIsCoordinator;
// Used by the coordinator to store the short addresses of all assoc children in the network
WORD pAssociatedShortAddress[MAX_CHILDREN];
// Used by the coordinator to store the extended addresses of all assoc children in the network
QWORD pAssociatedExtAddress[MAX_CHILDREN];
// The coordinator's short address
ADDRESS coordAddr;
WORD newPanId;
BYTE msduHandle;
BOOL wasLastTxSuccessful;
UINT8 retriesRemaining;
UINT16 GBeaconSequenceNumber;
BOOL confirmReceived;
BYTE pMeasurementBuffer[MAX_MEAS_BUFFER_SIZE];
BYTE pMeasurementBuffer2[MAX_MEAS_BUFFER_SIZE];
BOOL isRadioOn;
Children myChildren[MAX_CHILDREN]; // The number of consecutive beacon periods I have not heard from my children

UINT8 GLinkQualityToChild = 0;

const UINT16 BROADCAST_ADDRESS = 0xFFFF;

// The short address given to
volatile WORD myShortAddr;
// Used by the coordinator to keep track of number of associated children in network
volatile UINT8 numAssociatedChildren;
// The number of beacons a end-device has received since association
volatile WORD myBeaconCount;
// And device will transmit if this variable this true
volatile BOOL startTransmit;
volatile BOOL receivedPreviousTransmitAck;
volatile BYTE txHandle;
volatile DWORD txSuccessful;
volatile DWORD txUnsuccessful;
volatile DWORD totalRx;
volatile DWORD calledDataRequest;
volatile BYTE secretBeaconValue;
volatile BOOL validPan;
volatile UINT16 panIdToJoin;
volatile UINT8 beaconRxSequenceNumberLowByte;
volatile UINT8 beaconRxSequenceNumberHighByte;
volatile BOOL beaconRetransmitted;
volatile UINT16 beaconRxSequenceNumber;
volatile UINT8 noParentAck;
volatile BOOL isAssocSuccess;
volatile BOOL noAssocResponse;

volatile UINT8 GActuationCommand = NICTOR_NO_COMMAND_CMD_ID;
volatile UINT8 isMeasuringFlow = FALSE;


//-------------------------------------------------------------------------------------------------------
//  void main(void)
//
//  DESCRIPTION:
//      Startup sequence and main loop.
//-------------------------------------------------------------------------------------------------------
void main(void) {

	GFatalErrorResetNow = FALSE;

    UINT8 n, k;
    PAN_DESCRIPTOR *pPanDescriptor;
    BYTE setAttributeValue;

    WDT_off();
    HW_init();

	ENABLE_LINK_RD();

    if (COORDINATOR_NODE) {
		UART_initializeUart0(CLK_8M_BAUD_38_4K);
		UART_initializeUart1(CLK_8M_BAUD_38_4K);
	}

    RELAYS_OFF_R1();
    RELAYS_OFF_R2();

    MAC_INIT();
    DB_PERIPHERAL_PORT_INIT();
    BATTERY_init();

    if (HAS_ECHO_SENSOR) {
	    ECHO_init();
	}

	if (HAS_FLOW_SENSOR) {
		FLOW_init();
		CloseValve();
	}

	if (COORDINATOR_NODE) {
	    CMD_objectInit();
	}


    // Power up
    mpmSetRequest(MPM_CC2420_ON);
    while (mpmGetState() != MPM_CC2420_ON);
    isRadioOn = TRUE;

    // Reset the MAC layer
    mlmeResetRequest(TRUE);

    // Set and/or get the extended address
    aExtendedAddress = getExtendedAddress();

    // Initialize the addresses for the associate devices
    for (k = 0; k < MAX_CHILDREN; k++)
        pAssociatedShortAddress[k] = 0x0001 + (WORD)k;
    numAssociatedChildren = 0;
    for (k = 0; k < MAX_CHILDREN; k++)
        pAssociatedExtAddress[k] = 0x0000000000000000;
    for (k = 0; k < MAX_CHILDREN; k++) {
        myChildren[k].free = TRUE;
        myChildren[k].pAssociatedExtAddress = 0x0000000000000000;
        myChildren[k].numIntervalsNoCommunication = 0;
    }
    myBeaconCount = 0;
    startTransmit = FALSE;
    receivedPreviousTransmitAck = TRUE;
    txHandle = 0;
    txSuccessful = 0;
    txUnsuccessful = 0;
    totalRx = 0;
    calledDataRequest = 0;

	for (k = 0; k < MAX_ACTUATION_COMMAND_BYTES; k++) {
		pActuationCommands[k] = 0x00;
	}

    while (TRUE) {


        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //
        // NON-BEACON ENABLED 802.15.4 PAN COORDINATOR
        //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        if (COORDINATOR_NODE)
        {
			SET_RLED2();

			// Start up the MAC
            GIsCoordinator = TRUE;
            myShortAddr = COORD_SHORT;
            panId = PANID;
            // Reset the MAC layer
            mlmeResetRequest(TRUE);
            // Modify necessary PIB attributes
            if (mlmeSetRequest(MAC_SHORT_ADDRESS, (BYTE*) &myShortAddr) != SUCCESS) {wdt_enable( WDTO_4S ); EXCEPTION1();}
            setAttributeValue = TRUE;
            if (mlmeSetRequest(MAC_RX_ON_WHEN_IDLE, &setAttributeValue) != SUCCESS) {wdt_enable( WDTO_4S ); EXCEPTION1();}
            if (mlmeSetRequest(MAC_ASSOCIATION_PERMIT, &setAttributeValue) != SUCCESS) {wdt_enable( WDTO_4S ); EXCEPTION1();}
            setAttributeValue = 5;
            if (mlmeSetRequest(MAC_MAX_CSMA_BACKOFFS, &setAttributeValue) != SUCCESS) {wdt_enable( WDTO_4S ); EXCEPTION1();}

            UINT8 channel = CHANNEL;

            // Set up NICTOR beacon payload to be transmitted inside 802.15.4 beacon
            // TBD: should really add a sequence number
            setAttributeValue = 6;
            pBeaconPayload[0] = (BYTE)(UNIQUE_802_15_4_BEACON_ID & 0xFF); // lsb;
            pBeaconPayload[1] = (BYTE)((UNIQUE_802_15_4_BEACON_ID >> 8) & 0xFF);
            pBeaconPayload[2] = (BYTE)((UNIQUE_802_15_4_BEACON_ID >> 16) & 0xFF);
            pBeaconPayload[3] = (BYTE)((UNIQUE_802_15_4_BEACON_ID >> 24) & 0xFF); // msb
            pBeaconPayload[4] = (BYTE)(panId & 0xFF); // lsb
            pBeaconPayload[5] = (BYTE)((panId >> 8) & 0xFF); // msb
            mlmeSetRequest(MAC_BEACON_PAYLOAD_LENGTH, &setAttributeValue);
            mlmeSetRequest(MAC_BEACON_PAYLOAD, pBeaconPayload);
            // Start as a PAN coordinator of a non-beacon PAN
            UINT8 BO = 15;
            UINT8 SO = 15;
            mlmeStartRequest(panId, channel, BO, SO, TRUE, FALSE, FALSE, FALSE);
            GBeaconSequenceNumber = 0;

			// Network ready
			GCoordinatorState = COORDINATOR_STATE_CAP;
			BOOL isModemOn = TRUE; // Because we don't want to transmit the first time
			RTC_clearAFE();
			UINT8 time0[8] = {0,0,0,0,0,0,0,0};
			RTC_setTime(time0);

			div_t divresult;
			divresult = div(GNetworkWakeIntervalInSeconds,60);

			//UINT8 minutes = 0;
			//UINT8 seconds = GNetworkWakeIntervalInSeconds;

			UINT8 minutes = (UINT8)divresult.quot;
			UINT8 seconds = (UINT8)divresult.rem;

			UINT8 alarm0[8] = {0,seconds,minutes,0,0,0,0,0};
			RTC_setAlarm(alarm0, 6);
			EIFR |= 0x10;
			EIMSK |= (1<<PIN4); // Enable INT4 (RTC interrupt)


			CLR_RLED2();
			SET_GLED2();


			while (1)
			{
				// Send NICTOR beacon
				DISABLE_GLOBAL_INT();
				FIFO__init();
				ENABLE_GLOBAL_INT();

				pBuffer[0] = BEACON_OTAMSG_ID;
				pBuffer[1] = (BYTE)((GBeaconSequenceNumber & 0xFF)); // low byte
				pBuffer[2] = (BYTE)((GBeaconSequenceNumber >> 8) & 0xFF);

				pBuffer[3] = (BYTE)(GNetworkIntervalInMinutes & 0xFF); 						// low byte
				pBuffer[4] = (BYTE)((GNetworkIntervalInMinutes >> 8) & 0xFF);

				pBuffer[5] = (BYTE)(GNetworkWakeIntervalInSeconds & 0xFF); 			// low byte
				pBuffer[6] = (BYTE)((GNetworkWakeIntervalInSeconds >> 8) & 0xFF);

				pBuffer[7] = (BYTE)GNetworkState;


				for (UINT8 i = 0; i < MAX_ACTUATION_COMMAND_BYTES; i++) {
					pBuffer[8 + i] = pActuationCommands[i];
				}

				//CMD__sendFreeForm0( 16, "0: Send beacon\n\r" );
				//CMD__sendFreeForm1( 16, "1: Send beacon\n\r" );

				mcpsDataRequest(SRC_ADDR_EXT | DEST_ADDR_SHORT,
							panId,
							(ADDRESS*) &aExtendedAddress,
							panId,
							(ADDRESS*) &BROADCAST_ADDRESS,
							32,
							pBuffer,
							TRANSMIT_BEACON_MSDU_HANDLE, // txHandle
							TX_OPT_NONE);
				GBeaconSequenceNumber++;

				//CMD__sendFreeForm0( 32, pBuffer );
				//if (pBuffer[8] != 0x00) {
					//CMD__sendFreeForm0( 32, pBuffer );
				//}
				// Clear all actuation commands
				for (UINT8 i = 0; i < MAX_ACTUATION_COMMAND_BYTES; i++) {
					pActuationCommands[i] = 0x00;
					pBuffer[8 + i] = 0x00;
				}

				// Update child comm info
				for ( UINT8 i = 0; i < MAX_CHILDREN; i++ )
				{
					if ( !(myChildren[i].free) )
					{
						myChildren[i].numIntervalsNoCommunication += 1;
						if (myChildren[i].numIntervalsNoCommunication > MAX_INTERVAL_NO_CHILD_COMM)
						{
							myChildren[i].free = TRUE;
							numAssociatedChildren--;
						}
					}
				}


				// Receiving data from other nodes
				SET_YLED();
				while (GCoordinatorState == COORDINATOR_STATE_CAP) {}
				CLR_YLED();

				// Done working, time to sleep, set the RTC to wake me up....


				RTC_clearAFE(); BYTE time1[8] = {0,0,0,0,0,0,0,0};
				RTC_setTime(time1);

				//minutes = GNetworkIntervalInMinutes - 1;
				//seconds = (60 - GNetworkWakeIntervalInSeconds);

				UINT16 intervalSeconds = GNetworkIntervalInMinutes*60;
				UINT16 sleepSeconds = intervalSeconds - GNetworkWakeIntervalInSeconds;
				div_t divresult;
				divresult = div(sleepSeconds, 60);
				minutes = (UINT8)divresult.quot;
				seconds = (UINT8)divresult.rem;

				BYTE alarm1[8] = {0,seconds,minutes,0,0,0,0,0};
				RTC_setAlarm(alarm1, 6);
				EIFR |= 0x10;
				EIMSK |= (1<<PIN4); // Enable INT4 (RTC interrupt)

				// But send data to server before we sleep...

				// TBD: Check the modem is ready

				// Also, make one off local measurement
				BYTE* b = &(pMeasurementBuffer[0]);
				UINT8 idx = 1;

				// CMD_TYPE (1) (0)
				b[idx] = EVENT_CMD_TYPE; idx++;
				// CMD_ID (1) (1)
				b[idx] = COORD_INFO_CMD_ID; idx++;
				// Error code
				b[idx] = 0x00; idx++;
				// NODE IEEE ADDRESS (8) (2)
				UINT8 s;
				for (s = 0; s < 8; s++)
				{
				    BYTE aByte = (BYTE)((aExtendedAddress >> 8*s) & 0xFF);
				    b[idx] = aByte; idx++;
				}
				// NODE BATT VOLTAGE (1) (34)
				b[idx] = BATTERY_readBatteryVoltageIn25thsVolt(); idx++;
				// NODE TEMPERATURE (1)
				b[idx] = (BYTE)(TEMP_readTemperature()); idx++;
				// Link quality
				b[idx] = GLinkQualityToChild; idx++;

				// Mode (IDLE = 0x00, SLEEP = 0x01)
				if (GNetworkState == NETWORK_STATE_ACTIVE) {
					b[idx] = 0x01; idx++;
				}
				else {
					b[idx] = 0x00; idx++;
				}
				
				// Network inteval
				b[idx] = (BYTE)(GNetworkIntervalInMinutes & 0xFF); idx++; 						// low byte
				b[idx] = (BYTE)((GNetworkIntervalInMinutes >> 8) & 0xFF); idx++;

				// Sleep interval
				b[idx] = (BYTE)(GNetworkWakeIntervalInSeconds & 0xFF); idx++; 			// low byte
				b[idx] = (BYTE)((GNetworkWakeIntervalInSeconds >> 8) & 0xFF); idx++;

				b[0] = idx - 1;

				DISABLE_GLOBAL_INT();
				FIFO__write(b[0] + 1, b);
				ENABLE_GLOBAL_INT();


				if (isModemOn) {
					BOOL moreData = TRUE;
					while (moreData)
					{
						UINT8 numBytes = 0;
						UINT16 theCRC = 0;
						DISABLE_GLOBAL_INT();
						numBytes = FIFO__read(&(pBuffer1[4]));
						ENABLE_GLOBAL_INT();

						if (numBytes > 0)
						{
							// Add the NID
							for (k = 0; k < 4; k++)
							{
								pBuffer1[k] = (BYTE)(((NID) >> (8 * k)) & 0xFF); numBytes++;
							}
							// Calculate the CRC
							for (k = 0; k < numBytes; k++)
							{
								theCRC = _crc_xmodem_update(theCRC, pBuffer1[k]);
							}
							// Attach the CRC
							BYTE highCRC = (BYTE)((theCRC >> 8) & 0xFF); // high byte
							pBuffer1[numBytes] = highCRC; numBytes++;
							BYTE lowCRC = (BYTE)(theCRC & 0xFF); // low byte
							pBuffer1[numBytes] = lowCRC; numBytes++;

							// Send the frame
							CMD__sendMessage0( numBytes, (const char*)&pBuffer1[0], TRUE );
							WAIT_UART0_TRANSMITTER_READY();

						}
						else {
							moreData = FALSE;
						}
					}

					// Tell the server we are done
					// Add the NID
					UINT8 numBytes = 0;
					UINT16 theCRC = 0;
					for (k = 0; k < 4; k++)
					{
						pBuffer1[k] = (BYTE)(((NID) >> (8 * k)) & 0xFF); numBytes++;
					}
					pBuffer1[4] = EVENT_CMD_TYPE; numBytes++;
					pBuffer1[5] = NODE_NO_MORE_DATA_CMD_ID; numBytes++;
					pBuffer1[6] = 0x00; numBytes++; // no error code

					// Calculate the CRC
					for (k = 0; k < numBytes; k++)
					{
						theCRC = _crc_xmodem_update(theCRC, pBuffer1[k]);
					}
					// Attach the CRC
					BYTE highCRC = (BYTE)((theCRC >> 8) & 0xFF); // high byte
					pBuffer1[numBytes] = highCRC; numBytes++;
					BYTE lowCRC = (BYTE)(theCRC & 0xFF); // low byte
					pBuffer1[numBytes] = lowCRC; numBytes++;

					// Send the frame

					CMD__sendMessage0( numBytes, (const char*)&pBuffer1[0], TRUE );

					WAIT_UART0_TRANSMITTER_READY();

					// Action any remote commands
					if (remoteCommand.isCommand) {


						switch (remoteCommand.cmdID)
						{
							case NICTOR_SLEEP_UPDATE_CMD_ID:
								// Set the new sleep intervals
								GNetworkIntervalInMinutes = (UINT16)(remoteCommand.cmdPayload[0] & 0x00FF) | (UINT16)((remoteCommand.cmdPayload[1] & 0x00FF) << 8);
								GNetworkWakeIntervalInSeconds = (UINT16)(remoteCommand.cmdPayload[2] & 0x00FF) | (UINT16)((remoteCommand.cmdPayload[3] & 0x00FF) << 8);
								PING_YLED();
								break;

							case NICTOR_ACTIVE_MODE_CMD_ID:


								GNetworkState = NETWORK_STATE_ACTIVE;
								GNetworkIntervalInMinutes = ACTIVE_NETWORK_INTERVAL_IN_MINUTES;
								GNetworkWakeIntervalInSeconds = ACTIVE_NETWORK_WAKE_INTERVAL;
								break;

							case NICTOR_IDLE_MODE_CMD_ID:
								PING_RLED();

								GNetworkState = NETWORK_STATE_IDLE;
								GNetworkIntervalInMinutes = IDLE_NETWORK_INTERVAL_IN_MINUTES;
								GNetworkWakeIntervalInSeconds = IDLE_NETWORK_WAKE_INTERVAL;
								break;

							case NICTOR_ACTUATION_CMD_ID:
								for (UINT8 i = 0; i < MAX_ACTUATION_COMMAND_BYTES; i++) {
									pActuationCommands[i] = remoteCommand.cmdPayload[i];
								}
								break;

							default:
								break;
						}

						// We are done with the remote command payload so clear it so there is no confusion with mixed bytes in the message
						CMD__clearRemoteCommandPayload();
						remoteCommand.isCommand = FALSE;
					}

				}

				if (GNetworkState == NETWORK_STATE_ACTIVE) {
					SET_RLED2();
					while (GCoordinatorState == COORDINATOR_STATE_END_CAP) {};
					EIFR &= ~(0x10);
					EIMSK &= ~(1<<PIN4); // Disable INT4 (RTC interrupt)
					CLR_RLED2();
				}
				else
				{
					SET_RLED2();
					while (GCoordinatorState == COORDINATOR_STATE_END_CAP) {};
					EIFR &= ~(0x10);
					EIMSK &= ~(1<<PIN4); // Disable INT4 (RTC interrupt)
					CLR_RLED2();
					isModemOn = TRUE;
				}

				RTC_clearAFE();
				BYTE time2[8] = {0,0,0,0,0,0,0,0};
				RTC_setTime(time2);

				//minutes = 0;
				//seconds = GNetworkWakeIntervalInSeconds;

				div_t divresult1;
				divresult1 = div(GNetworkWakeIntervalInSeconds,60);
				minutes = (UINT8)divresult1.quot;
				seconds = (UINT8)divresult1.rem;

				BYTE alarm2[8] = {0,seconds,minutes,0,0,0,0,0};
				RTC_setAlarm(alarm2, 6);
				EIFR |= 0x10; EIMSK |= (1<<PIN4); // Enable INT4 (RTC interrupt)
				SET_GLED2();
			}
        }


		// ROUTER
        if (SENSOR_NODE)
        {
			TURN_OFF_UART0_TX();
			TURN_OFF_UART1_TX();


			SET_RLED2();
            GRouterState = STATE_ASSOCIATING;
            GIsCoordinator = FALSE;

            // MAC layer initialization stuff
            mlmeResetRequest(TRUE);
            myShortAddr = 0xFFFF;
            if (mlmeSetRequest(MAC_SHORT_ADDRESS, (BYTE*) &myShortAddr) != SUCCESS) {wdt_enable( WDTO_4S ); EXCEPTION1();}
            setAttributeValue = TRUE;
            if (mlmeSetRequest(MAC_RX_ON_WHEN_IDLE, &setAttributeValue) != SUCCESS) {wdt_enable( WDTO_4S ); EXCEPTION1();}
            setAttributeValue = TRUE;
            if (mlmeSetRequest(MAC_ASSOCIATION_PERMIT, &setAttributeValue) != SUCCESS) {wdt_enable( WDTO_4S ); EXCEPTION1();}
            setAttributeValue = 5;
            if (mlmeSetRequest(MAC_MAX_CSMA_BACKOFFS, &setAttributeValue) != SUCCESS) {wdt_enable( WDTO_4S ); EXCEPTION1();}

            // Scan for coordinators on CHANNEL
            UINT8 channel = CHANNEL;
            validPan = FALSE;
            while (!validPan) {
        	    mlmeScanRequest( ACTIVE_SCAN, (UINT32) 1 << channel, 5, &scanResult );
			}

            // Go through the result list and pick a coordinator to associate with
            pPanDescriptor = NULL;
            isAssocSuccess = FALSE;

            for (n = 0; n < scanResult.resultListSize; n++)
            {
                if (scanResult.pPANDescriptorList[n].superframeSpec & SS_ASSOCIATION_PERMIT_BM)
                {
                    if (scanResult.pPANDescriptorList[n].coordPanId == panIdToJoin)
                    {
                        pPanDescriptor = &scanResult.pPANDescriptorList[n];

                        if (!pPanDescriptor)
                        {
                            wdt_enable( WDTO_4S );
                            EXCEPTION6();
                        }

                        // Set variables
                        panId = pPanDescriptor->coordPanId;
                        coordAddr = pPanDescriptor->coordAddress;

                        // Update PIB attributes
                        mlmeSetRequest(MAC_PAN_ID, &panId);

                        setAttributeValue = BF(pPanDescriptor->superframeSpec, SS_BEACON_ORDER_BM, SS_BEACON_ORDER_IDX);
                        mlmeSetRequest(MAC_BEACON_ORDER, &setAttributeValue);

                        setAttributeValue = BF(pPanDescriptor->superframeSpec, SS_SUPERFRAME_ORDER_BM, SS_SUPERFRAME_ORDER_IDX);
                        mlmeSetRequest(MAC_SUPERFRAME_ORDER, &setAttributeValue);

                        // Synchronize with the beacon in case of a beacon-enabled PAN
                        if ((pPanDescriptor->superframeSpec & SS_BEACON_ORDER_BM) != 15)
                        {
                            mlmeSyncRequest(pPanDescriptor->logicalChannel, TRUE);
                        }

                        // Associate with the selected coordinator
                        noAssocResponse = TRUE;
                        mlmeAssociateRequest(pPanDescriptor->logicalChannel, pPanDescriptor->coordAddrMode, panId, &coordAddr, CI_RX_ON_WHEN_IDLE_BM | CI_POWER_SOURCE_BM | CI_DEVICE_TYPE_IS_FFD_BM, FALSE);

                        wdt_enable( WDTO_8S );
                        while (noAssocResponse);
                        WDT_off();

                        // if valid then break;
                        if (isAssocSuccess)
                        {
                            break;
                        }
                    }
                }
            }

            if (!isAssocSuccess)
            {
                // Generate a random number between 0 and 9 and make this the delay to the watchdog
                UINT8 randomDelay = 0;
                for (UINT8 i = 0; i < 9; i++)
                {
                    randomDelay += (0x01 & (msupGetRandomByte()));
                }

                wdt_enable( randomDelay );
                EXCEPTION4();
            }

            // Initialize the payload
            for (n = 0; n < 64; n++)
            {
                pBuffer[n] = n;
            }

            // Set up 802.15.4 beacon payload
            setAttributeValue = 6;
            pBeaconPayload[0] = (BYTE)(UNIQUE_802_15_4_BEACON_ID & 0xFF); // lsb;
            pBeaconPayload[1] = (BYTE)((UNIQUE_802_15_4_BEACON_ID >> 8) & 0xFF);
            pBeaconPayload[2] = (BYTE)((UNIQUE_802_15_4_BEACON_ID >> 16) & 0xFF);
            pBeaconPayload[3] = (BYTE)((UNIQUE_802_15_4_BEACON_ID >> 24) & 0xFF); // msb
            pBeaconPayload[4] = (BYTE)(panId & 0xFF); // lsb
            pBeaconPayload[5] = (BYTE)((panId >> 8) & 0xFF); // msb
            mlmeSetRequest(MAC_BEACON_PAYLOAD_LENGTH, &setAttributeValue);
            mlmeSetRequest(MAC_BEACON_PAYLOAD, pBeaconPayload);


            // Start as a ROUTER of a non-beacon PAN
            UINT8 BO = 15;
            UINT8 SO = 15;
            mlmeStartRequest(panId, channel, BO, SO, TRUE, FALSE, FALSE, FALSE);

            // Prepare to rx beacon
            GRouterState = STATE_WAITING_FOR_BEACON;
            noParentAck = 0;
            msduHandle = 0;

			CLR_RLED2();
			SET_GLED2();

            while (TRUE)
            {

				// Update child comm info
				for ( UINT8 i = 0; i < MAX_CHILDREN; i++ )
				{
					if ( !(myChildren[i].free) )
					{
						myChildren[i].numIntervalsNoCommunication += 1;
						if (myChildren[i].numIntervalsNoCommunication > MAX_INTERVAL_NO_CHILD_COMM)
						{
							myChildren[i].free = TRUE;
							numAssociatedChildren--;
						}

					}
				}


                if (noParentAck > MAX_NO_PARENT_ACK)
                {
                    wdt_enable( WDTO_4S ); // TBD --- Remove this once we get the proper watchdog working
                    EXCEPTION6();  // TBD --- Sort these out
                }

				// Wait until you get a beacon
				while (GRouterState == STATE_WAITING_FOR_BEACON) {
					if (GFatalErrorResetNow) {
						EIFR &= ~(0x10);
						EIMSK &= ~(1<<PIN4); // Disable INT4 (RTC interrupt)
						wdt_enable( WDTO_4S ); // TBD --- Remove this once we get the proper watchdog working
						EXCEPTION6();  // TBD --- Sort these out
					}
				}

				// Got a beacon, set the wake interval with 'GNetworkWakeIntervalInSeconds'
				// and 'GNetworkIntervalInMinutes' which were received in beacon
				if (GRouterState == STATE_WAITING_FOR_END_OF_CAP) {
					RTC_clearAFE();
					BYTE time2[8] = {0,0,0,0,0,0,0,0};
					RTC_setTime(time2);
					// How many minutes and seconds in GNetworkWakeIntervalInSeconds
					div_t divresult;
					divresult = div(GNetworkWakeIntervalInSeconds, 60);
					UINT8 minutes = (UINT8)divresult.quot;
					UINT8 seconds = (UINT8)divresult.rem;
					BYTE alarm2[8] = {0,seconds,minutes,0,0,0,0,0};
					RTC_setAlarm(alarm2, 6);
					EIFR |= 0x10;
					EIMSK |= (1<<PIN4); // Enable INT4 (RTC interrupt)
				}

				DISABLE_GLOBAL_INT();
				FIFO__init();
				ENABLE_GLOBAL_INT();

				// Forward the beacon
				pBuffer[0] = BEACON_OTAMSG_ID;
				pBuffer[1] = (BYTE)((GBeaconSequenceNumber & 0xFF));						// low byte
				pBuffer[2] = (BYTE)((GBeaconSequenceNumber >> 8) & 0xFF);

				pBuffer[3] = (BYTE)(GNetworkIntervalInMinutes & 0xFF); 				// low byte
				pBuffer[4] = (BYTE)((GNetworkIntervalInMinutes >> 8) & 0xFF);

				pBuffer[5] = (BYTE)(GNetworkWakeIntervalInSeconds & 0xFF); 			// low byte
				pBuffer[6] = (BYTE)((GNetworkWakeIntervalInSeconds >> 8) & 0xFF);

				pBuffer[7] = (BYTE)GNetworkState;

				for (UINT8 i = 0; i < MAX_ACTUATION_COMMAND_BYTES; i++) {
					pBuffer[8 + i] = pActuationCommands[i];
				}

				mcpsDataRequest(SRC_ADDR_EXT | DEST_ADDR_SHORT,
							panId,
							(ADDRESS*) &aExtendedAddress,
							panId,
							(ADDRESS*) &BROADCAST_ADDRESS,
							32,
							pBuffer,
							TRANSMIT_BEACON_MSDU_HANDLE, // txHandle
							TX_OPT_NONE);

				// Actuation commands
				//
				switch (GActuationCommand) {
					case NICTOR_NO_COMMAND_CMD_ID:
						break;

					case NICTOR_OPEN_VALVE_CMD_ID:
						OpenValve();
						isMeasuringFlow = TRUE;
						break;

					case NICTOR_CLOSE_VALVE_CMD_ID:
						CloseValve();
						isMeasuringFlow = FALSE;
						break;

					default:
						break;
				}
				GActuationCommand = NICTOR_NO_COMMAND_CMD_ID;

				// Make a measurement
				BYTE* b = &(pMeasurementBuffer[0]);
				UINT8 idx = 1;

				if ((GNetworkState == NETWORK_STATE_ACTIVE) && HAS_ECHO_SENSOR)
				{
					// CMD_TYPE (1) (0)
					b[idx] = EVENT_CMD_TYPE; idx++;
					// CMD_ID (1) (1)
					b[idx] = ECHO_DATA_CMD_ID; idx++;
					// Error code
					b[idx] = 0x00; idx++;
					// NODE IEEE ADDRESS (8) (2)
					UINT8 s;
					for (s = 0; s < 8; s++)
					{
						BYTE aByte = (BYTE)((aExtendedAddress >> 8*s) & 0xFF);
						b[idx] = aByte; idx++;
					}
					// NODE BATT VOLTAGE (1) (34)
					b[idx] = BATTERY_readBatteryVoltageIn25thsVolt(); idx++;
					// NODE TEMPERATURE (1)
					b[idx] = (BYTE)(TEMP_readTemperature()); idx++;
					// Link quality
					b[idx] = GLinkQualityToChild; idx++;

					// ECHO probe soil moisture
					ECHO_init();
					UINT32 echoMeasurement = ECHO_getMeasurement();
					BYTE measuredLsb = (BYTE)((echoMeasurement >> 8*0) & 0xFF);
					b[idx] = measuredLsb; idx++;
					BYTE measuredMsb = (BYTE)((echoMeasurement >> 8*1) & 0xFF);
					b[idx] = measuredMsb; idx++;
					BYTE excitationLsb = (BYTE)((echoMeasurement >> 8*2) & 0xFF);
					b[idx] = excitationLsb; idx++;
					BYTE excitationMsb = (BYTE)((echoMeasurement >> 8*3) & 0xFF);
					b[idx] = excitationMsb; idx++;
				}
				else if ((GNetworkState == NETWORK_STATE_ACTIVE) && HAS_FLOW_SENSOR) {
					// CMD_TYPE (1) (0)
					b[idx] = EVENT_CMD_TYPE; idx++;
					// CMD_ID (1) (1)
					b[idx] = FLOW_DATA_CMD_ID; idx++;
					// Error code
					b[idx] = 0x00; idx++;
					// NODE IEEE ADDRESS (8) (2)
					UINT8 s;
					for (s = 0; s < 8; s++)
					{
						BYTE aByte = (BYTE)((aExtendedAddress >> 8*s) & 0xFF);
						b[idx] = aByte; idx++;
					}
					// NODE BATT VOLTAGE (1) (34)
					b[idx] = BATTERY_readBatteryVoltageIn25thsVolt(); idx++;
					// NODE TEMPERATURE (1)
					b[idx] = (BYTE)(TEMP_readTemperature()); idx++;
					// Link quality
					b[idx] = GLinkQualityToChild; idx++;

					// FLOW measurement here
					UINT64 flowMeasurement = FLOW_getMeasurement();

					b[idx] = (BYTE)((flowMeasurement >> 8*0) & 0xFF); idx++; // 00-11-22-33-44-55-66-77 on host
					b[idx] = (BYTE)((flowMeasurement >> 8*1) & 0xFF); idx++;
					b[idx] = (BYTE)((flowMeasurement >> 8*2) & 0xFF); idx++;
					b[idx] = (BYTE)((flowMeasurement >> 8*3) & 0xFF); idx++;
					b[idx] = (BYTE)((flowMeasurement >> 8*4) & 0xFF); idx++;
					b[idx] = (BYTE)((flowMeasurement >> 8*5) & 0xFF); idx++;
					b[idx] = (BYTE)((flowMeasurement >> 8*6) & 0xFF); idx++;
					b[idx] = (BYTE)((flowMeasurement >> 8*7) & 0xFF); idx++;
				}
				else
				{
					// CMD_TYPE (1) (0)
					b[idx] = EVENT_CMD_TYPE; idx++;
					// CMD_ID (1) (1)
					b[idx] = NODE_INFO_CMD_ID; idx++;
					// Error code
					b[idx] = 0x00; idx++;
					// NODE IEEE ADDRESS (8) (2)
					UINT8 s;
					for (s = 0; s < 8; s++)
					{
						BYTE aByte = (BYTE)((aExtendedAddress >> 8*s) & 0xFF);
						b[idx] = aByte; idx++;
					}
					// NODE BATT VOLTAGE (1) (34)
					b[idx] = BATTERY_readBatteryVoltageIn25thsVolt(); idx++;
					// NODE TEMPERATURE (1)
					b[idx] = (BYTE)(TEMP_readTemperature()); idx++;
					// Link quality
					b[idx] = GLinkQualityToChild; idx++;
				}

				b[0] = idx - 1;

				DISABLE_GLOBAL_INT();
				FIFO__write(b[0] + 1, b);
				ENABLE_GLOBAL_INT();

				while (GRouterState == STATE_WAITING_FOR_END_OF_CAP) {
					// Get FIFO content and send to parent
					DISABLE_GLOBAL_INT();
					UINT16 K = FIFO__read(&(pBuffer[2]));
					ENABLE_GLOBAL_INT();

					if (K > 0)
					{
						pBuffer[0] = DATA_OTAMSG_ID;
						pBuffer[1] = K;
						//
						// DATA_OTAMSG_ID | K | <msg>
						//
						msduHandle++;
						mcpsDataRequest(SRC_ADDR_EXT | DEST_ADDR_SHORT,
									panId,
									(ADDRESS*) &aExtendedAddress,
									panId,
									(ADDRESS*) &coordAddr,
									K + 2,
									pBuffer,
									msduHandle, // txHandle
									TX_OPT_ACK_REQ);
					}
				}

				RTC_clearAFE();
				BYTE time1[8] = {0,0,0,0,0,0,0,0};
				RTC_setTime(time1);

				UINT16 intervalSeconds = GNetworkIntervalInMinutes*60;
				UINT16 sleepSeconds = intervalSeconds - GNetworkWakeIntervalInSeconds - 4; // 4 to wake up 4 sec before beacon
				div_t divresult;
				divresult = div(sleepSeconds, 60);
				UINT8 minutes = (UINT8)divresult.quot;
				UINT8 seconds = (UINT8)divresult.rem;

				//UINT8 minutes = GNetworkIntervalInMinutes - 1;
				//UINT8 seconds = 60 - GNetworkWakeIntervalInSeconds - 4;

				BYTE alarm1[8] = {0,seconds,minutes,0,0,0,0,0};
				RTC_setAlarm(alarm1, 6);
				EIFR |= 0x10;
				EIMSK |= (1<<PIN4); // Enable INT4 (RTC interrupt)

				if (GNetworkState == NETWORK_STATE_ACTIVE) {
					// Go to sleep
					CLR_GLED2();

					// Disable flow interrupt 6
					EIMSK &= ~(1 << 6);
					power_adc_disable();
					PWR_DN_ANALOG();
					TEMP_SHUTDOWN();

					// power down the radio
					BOOL idx1 = FALSE;
					mlmeSetRequest(MAC_RX_ON_WHEN_IDLE, &idx1);
					mpmSetRequest(MPM_CC2420_XOSC_OFF);
					while (mpmGetState() != MPM_CC2420_XOSC_OFF);

					isRadioOn = FALSE;

					set_sleep_mode(SLEEP_MODE_PWR_DOWN);
					sleep_enable();
					sei();

					sleep_cpu();


					sleep_disable();
					EIFR &= ~(0x10);
					EIMSK &= ~(1<<PIN4); // Disable INT4 (RTC interrupt)

					// Peripherals on
					PWR_UP_ANALOG();
					power_adc_enable();
					TEMP_WAKEUP();

					// Enable flow interrupt 6
					EIMSK |= (1 << 6);

					// power up the radio
					mpmSetRequest(MPM_CC2420_ON);
					while (mpmGetState() != MPM_CC2420_ON);
					BOOL idx2 = TRUE;
					mlmeSetRequest(MAC_RX_ON_WHEN_IDLE, &idx2);
					isRadioOn = TRUE;
				}
				else {
					CLR_GLED2();
					while (GRouterState == STATE_SLEEPING) {};
					EIFR &= ~(0x10);
					EIMSK &= ~(1<<PIN4); // Disable INT4 (RTC interrupt)
				}

				SET_GLED2();

				// Set up a timer that will expire in one beacon period time to check that we have not missed a beacon
				RTC_clearAFE();
				BYTE time3[8] = {0,0,0,0,0,0,0,0};
				RTC_setTime(time3);
				UINT8 minutes3 = GNetworkIntervalInMinutes;
				UINT8 seconds3 = 0;
				BYTE alarm3[8] = {0,seconds3,minutes3,0,0,0,0,0};
				RTC_setAlarm(alarm3, 6);
				EIFR |= 0x10;
				EIMSK |= (1<<PIN4); // Enable INT4 (RTC interrupt)
			}
        }
    }

} // main

//-------------------------------------------------------------------------------------------------------
//  void mlmeAssociateIndication(ADDRESS deviceAddress, BYTE capabilityInformation, BOOL securityUse,...)
//
//  DESCRIPTION:
//      mlmeAssociateIndication is generated by the MAC layer upon reception of an associate request
//      command frame. For this demo application, all devices are allowed to associate, however only one
//      device at the time. The coordinator must be reset before a new device can associate.
//
//      The short address is assigned from the associatedAddress variable, which should have been
//      incremented if more devices could have joined.
//
//  PARAMETERS:
//      ADDRESS deviceAddress
//          The extended address of the device requesting association
//      BYTE capabilityInformation
//          The operational capabilities of the device requesting association
//              CI_ALTERNATE_PAN_COORD_BM   0x01
//              CI_DEVICE_TYPE_IS_FFD_BM    0x02
//              CI_POWER_SOURCE_BM          0x04
//              CI_RX_ON_WHEN_IDLE_BM       0x08
//              CI_SECURITY_CAPABILITY_BM   0x40
//              CI_ALLOCATE_ADDRESS_BM      0x80
//      BOOL securityUse
//          An indication of whether the received MAC command frame is using security. This value set to
//          TRUE if the security enable subfield was set to 1 or FALSE if the security enabled subfield
//          was set to 0
//      UINT8 aclEntry
//          The macSecurityMode parameter value from the ACL entry associated with the sender of the
//          data frame. This value is set to 0x08 if the sender of the data frame was not found in the
//          ACL.
//-------------------------------------------------------------------------------------------------------
void mlmeAssociateIndication(ADDRESS deviceAddress, BYTE capabilityInformation, BOOL securityUse, UINT8 aclEntry)
{
    WORD associatedAddress;

	// Check if this device is already associated
	BOOL exists = FALSE;
	for ( UINT8 k = 0; k < MAX_CHILDREN; k++ )
	{
		if ( ( myChildren[k].pAssociatedExtAddress == deviceAddress.Extended ) &&  ( !(myChildren[k].free) ) )
		{
			UINT16 deviceShortAddress = 0x0000;
			UINT8 i;
			for ( i = 0; i < 2; i++ )
			{
				/* Use the mac layer random byte generator. */
				deviceShortAddress |= (((UINT16)(msupGetRandomByte())) << (8*i));
			}
			associatedAddress = deviceShortAddress;
			myChildren[k].shortAddress = deviceShortAddress;
			mlmeAssociateResponse(&deviceAddress, associatedAddress, 0x00, FALSE);
			exists = TRUE;
			break;
		}
	}


    if ( numAssociatedChildren < MAX_CHILDREN )
    {
        // TBD --> Check if the device IEEE address is in the local ACL
        BOOL isAclAccess = FALSE;
		for ( UINT8 i = 0 ; i < ACL_ELEMENTS; i++) {
			if (pACL[i] == deviceAddress.Extended) {
				isAclAccess = TRUE;
				break;
			}
		}
		if (!isAclAccess) {
			mlmeAssociateResponse(&deviceAddress, 0xffff, PAN_ACCESS_DENIED, FALSE);
			return;
		}

        numAssociatedChildren++;

        UINT16 deviceShortAddress = 0x0000;

        for ( UINT8 i = 0; i < 2; i++ )
        {
            /* Use the mac layer random byte generator. */
            deviceShortAddress |= (((UINT16)(msupGetRandomByte())) << (8*i));
        }
        associatedAddress = deviceShortAddress;
		// TBD --> Check this is a unique short address and should be < 0xFFFE to ensure short address mode

        mlmeAssociateResponse(&deviceAddress, associatedAddress, 0x00, FALSE);

        for ( UINT8 i = 0; i < MAX_CHILDREN; i++ )
        {
            if ( myChildren[i].free )
            {
                myChildren[i].pAssociatedExtAddress = deviceAddress.Extended;
                myChildren[i].shortAddress = deviceShortAddress;
                myChildren[i].free = FALSE;
                myChildren[i].missedPeriods = 0;
                myChildren[i].numIntervalsNoCommunication = 0;
                break;
            }
        }
    }
    else {
		mlmeAssociateResponse(&deviceAddress, 0xffff, PAN_AT_CAPACITY, FALSE);
		return;
	}
} // mlmeAssociateIndication




//-------------------------------------------------------------------------------------------------------
//  void mcpsDataIndication(MCPS_DATA_INDICATION *pMDI)
//
//  DESCRIPTION:
//      mcpsDataIndication is generated by the MAC layer when a data frame is successfully received.
//      The higher layer is not required to return quickly from this callback. Note, however, that:
//          - No more incoming packets will be processed until this function has returned, in fact all
//            mechanisms except beacon handling and already scheduled transmissions will be halted.
//          - One slot in the RX packet pool will be occupied.
//
//      The coordinator will use the first MSDU byte as an index of which LED to turn on. Bit indexes 4
//      to 7 will not be shown, since there are only 4 LEDs (with indexes 0-3).
//
//  PARAMETERS:
//      MCPS_DATA_INDICATION *pMDI
//          A pointer to the structure containing the received packet
//-------------------------------------------------------------------------------------------------------
void mcpsDataIndication(MCPS_DATA_INDICATION *pMDI)
{
	if ( !GIsCoordinator )
	{
		PING_RLED2();
		if ( GRouterState == STATE_WAITING_FOR_BEACON )
		{
			if ( ( pMDI->pMsdu[0] == BEACON_OTAMSG_ID ) )
			{

				RTC_clearAFE(); // Clear this timer, because we have received the beacon

				if (HAS_FLOW_SENSOR && isValveOpen) {
					minutesElapsedSinceOpenValve = minutesElapsedSinceOpenValve + GNetworkIntervalInMinutes;
					if (minutesElapsedSinceOpenValve >= 5) {
						CloseValve();
					}
				}

				beaconRxSequenceNumberLowByte = pMDI->pMsdu[1];
				beaconRxSequenceNumberHighByte = pMDI->pMsdu[2];

				GNetworkIntervalInMinutes = (UINT16)(pMDI->pMsdu[3] & 0x00FF) | (UINT16)((pMDI->pMsdu[4] & 0x00FF) << 8);

				GNetworkWakeIntervalInSeconds = (UINT16)(pMDI->pMsdu[5] & 0x00FF) | (UINT16)((pMDI->pMsdu[6] & 0x00FF) << 8);

				GNetworkState = pMDI->pMsdu[7];

				GRouterState = STATE_WAITING_FOR_END_OF_CAP;


				// Check for commands
				// pBuffer[8] to pBuffer[31] available for over the air actuation commands

				for ( UINT8 n = 0; n < 12; n++ )
				{
					pActuationCommands[2*n] = pMDI->pMsdu[8 + 2*n];
					pActuationCommands[2*n + 1] = pMDI->pMsdu[8 + 2*n +1];

					// Is there an actuation command for me?
					// We only read the first and ignore anything after that.
					if ( pMDI->pMsdu[8 + 2*n] == ((BYTE)(aExtendedAddress & 0xFF)) )
					{
						GActuationCommand = pMDI->pMsdu[8 + 2*n + 1];
						break;
					}
				}
			}

		}
		if ( GRouterState == STATE_WAITING_FOR_END_OF_CAP )
		{
			if ( pMDI->pMsdu[0] == DATA_OTAMSG_ID )
			{
				pMeasurementBuffer2[0] = pMDI->pMsdu[1]; // + 1;  // add one a/c we will add link quality to the end
				UINT8 d = 1;
				for ( UINT8 n = 1; n <= pMDI->pMsdu[1]; n++ )
				{
					pMeasurementBuffer2[n] = pMDI->pMsdu[n + 1]; d++;
				}
				GLinkQualityToChild = pMDI->mpduLinkQuality;
				FIFO__write( pMeasurementBuffer2[0] + 1, pMeasurementBuffer2 );


				// Update child comm info
				for ( UINT8 i = 0; i < MAX_CHILDREN; i++ )
				{
					if ( myChildren[i].pAssociatedExtAddress == pMDI->srcAddr.Extended )
					{
						myChildren[i].numIntervalsNoCommunication = 0;
						break;
					}
				}

			}
		}
	}
    if ( GIsCoordinator && ( GCoordinatorState == COORDINATOR_STATE_CAP ) )
    {
        if ( pMDI->pMsdu[0] == DATA_OTAMSG_ID )
        {
            pMeasurementBuffer[0] = pMDI->pMsdu[1]; // + 1; // add one a/c we will add link quality to the end
            UINT8 c = 1;
            for ( UINT8 n = 1; n <= pMDI->pMsdu[1]; n++ )
            {
                pMeasurementBuffer[n] = pMDI->pMsdu[n + 1]; c++;
            }
            GLinkQualityToChild = pMDI->mpduLinkQuality;
            FIFO__write( pMeasurementBuffer[0] + 1, pMeasurementBuffer );

			// Update child comm info
			for ( UINT8 i = 0; i < MAX_CHILDREN; i++ )
			{
				if ( myChildren[i].pAssociatedExtAddress == pMDI->srcAddr.Extended )
				{
					myChildren[i].numIntervalsNoCommunication = 0;
					break;
				}
			}
        }
    }
} // mcpsDataIndication




//-------------------------------------------------------------------------------------------------------
//  void mlmeAssociateConfirm(WORD assocShortAddress, MAC_ENUM status)
//
//  DESCRIPTION:
//      mlmeAssociateConfirm is generated by the MAC layer when an association attempt has succeeded or
//      failed (initiated by mlmeAssociateRequest(...)).
//
//  PARAMETERS:
//      WORD assocShortAddress
//          The short device address allocated by the coordinator on successful association. This
//          parameter will be equal to 0xFFFF if the association attempt was unsuccessful.
//      MAC_ENUM status
//          The status of the association attempt (SUCCESS, CHANNEL_ACCESS_FAILURE, NO_DATA, etc.)
//-------------------------------------------------------------------------------------------------------
void mlmeAssociateConfirm(WORD assocShortAddress, MAC_ENUM status) {
    if ((status == SUCCESS) && (assocShortAddress <=  0xFFFD))
    {
        isAssocSuccess = TRUE;
    }
    else
    {
        isAssocSuccess = FALSE;
    }

    myShortAddr = assocShortAddress;
    noAssocResponse = FALSE;

} // mlmeAssociateConfirm




//-------------------------------------------------------------------------------------------------------
//  void mcpsDataConfirm(MAC_ENUM status, BYTE msduHandle)
//
//  DESCRIPTION:
//      mcpsDataConfirm is called by the MAC layer when a packet transmission initiated by
//      mcpsDataRequest(...) has succeeded or failed.
//
//      The device will use the first MSDU byte of the packet that was transmitted as an index of which
//      LED to turn on. The mask is only shown when the value is between 4 and 7, and the most
//      significant bit will be masked away.
//
//  PARAMETERS:
//      MAC_ENUM status
//          The data confirm status.
//      BYTE msduHandle
//          The handle of the confirmed frame
//-------------------------------------------------------------------------------------------------------
void mcpsDataConfirm(MAC_ENUM status, BYTE theMsduHandle) {

    if (!GIsCoordinator)
    {
        if ((GRouterState == STATE_WAITING_FOR_END_OF_CAP) && (theMsduHandle == msduHandle)) {
            confirmReceived = TRUE;
            if (status != SUCCESS) {
                ++noParentAck; // this should only happen if no ack?!?!?!?
            }
            else {
                noParentAck = 0;
            }
        }
	}
} // mcpsDataConfirm


//-------------------------------------------------------------------------------------------------------
//  void mlmeBeaconNotifyIndication( MLME_BEACON_NOTIFY_INDICATION *pMBNI)
//
//  DESCRIPTION:
//      mlmeBeaconNotifyIndication is called by the MAC layer when a beacon is received from the PAN coord
//
//      The device will start transmitting packets at this time
//
//  PARAMETERS:
//-------------------------------------------------------------------------------------------------------

void mlmeBeaconNotifyIndication( MLME_BEACON_NOTIFY_INDICATION *pMBNI)
{
    if (GRouterState == STATE_ASSOCIATING)
    {
		DWORD nid = (DWORD)((DWORD)(pMBNI->pSdu[3] & 0x000000FF) << 24) | (DWORD)((DWORD)(pMBNI->pSdu[2] & 0x000000FF) << 16) | (DWORD)((DWORD)(pMBNI->pSdu[1] & 0x000000FF) << 8) | (DWORD)(pMBNI->pSdu[0] & 0x000000FF);
		if (nid == UNIQUE_802_15_4_BEACON_ID)
		{
			validPan = TRUE;
			panIdToJoin = (WORD)((pMBNI->pSdu[5] & 0x00FF) << 8) | (WORD)(pMBNI->pSdu[4] & 0x00FF);
		}
    }
} //mlmeBeaconNotifyIndication


//-------------------------------------------------------------------------------------------------------
// Currently unused callbacks
//void mlmeBeaconNotifyIndication(MLME_BEACON_NOTIFY_INDICATION *pMBNI) {}
void mlmeCommStatusIndication(WORD panId, BYTE srcAddrMode, ADDRESS *pSrcAddr, BYTE dstAddrMode, ADDRESS *pDstAddr, BYTE status) {}
void mlmeDisassociateIndication(QWORD deviceAddress, BYTE disassociateReason, BOOL securityUse, BOOL aclEntry) {}
void mlmeDisassociateConfirm(MAC_ENUM status) {}
void mlmeOrphanIndication(QWORD orphanAddress, BOOL securityUse, BOOL aclEntry) {}
void mlmePollConfirm(MAC_ENUM status) {}
void mlmeRxEnableConfirm(MAC_ENUM status) {}
void mlmeSyncLossIndication(MAC_ENUM lossReason) {}
void mpmSetConfirm(BYTE status) {}
//-------------------------------------------------------------------------------------------------------


QWORD getExtendedAddress(void)
{
	QWORD x = SID;
	// Write the IEEE address.
	EEPROM_writeEeprom((BYTE*)(&x), EEPROM_IEEE_ADDR_ADDRESS, sizeof(QWORD));
	// Write the header info last in case something caused a hardware reset during the above process.
	UINT16 header = (EEPROM_INITIALIZED_BYTE_VALUE << 8);
	UINT8 version = 0x00;
	header = header | (version && 0xFF);
	EEPROM_writeEeprom((BYTE*)(&header), EEPROM_VERSION_ADDRESS, EEPROM_NUM_VERSION_BYTES);

	return x;
}

// end
