/**************************************************************
 *
 * Copyright (C) 2005 NICTA Ltd.
 *
 * The information contained in this material constitutes
 * trade secret of NICTA Ltd.  You are only authorized to use
 * such material for your personal evaluation purposes.  You
 * may only disclose such material to those of your full-time
 * employees having a need to know such information, and may
 * not disclose it to any other person. Such material is
 * provided purely for informational purposes and on an AS IS
 * basis without any warranty of any kind.  NICTA LTD. DOES
 * NOT GUARANTEE THE ACCURACY OR COMPLETENESS OF THE
 * INFORMATION PROVIDED AND DISCLAIMS ALL WARRANTIES, WHETHER
 * EXPRESS OR IMPLIED, INCLUDING, BUT NOT LIMITED TO WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE,
 * ACCURACY OR NON-INFRINGEMENT, WITH REGARD TO SUCH
 * INFORMATION OR THE RESULTS TO BE OBTAINED FROM ITS USE.
 * All content included in this material, such as its design,
 * text, graphics, and the selection and arrangements thereof,
 * is copyright (C) 2005 NICTA Ltd.
 *
 * NICTOR is a trademark owned by NICTA LTD. Any other
 * trademarks are owned by the entities referenced herein.
 * All rights are reserved.
 *
 **************************************************************
 *
 * cmd_interface.h
 *
 *
 * Revision (do not edit):
 *
 * $Id: cmd_interface.h,v 1.1 2013-05-23 03:02:19 ksaleem Exp $
 *
 **************************************************************/

/*! \file cmd_interface.h
 *  \brief The header file for the private NICTOR command interface.
 */

#ifndef _CMD_INTERFACE_H_
#define _CMD_INTERFACE_H_

/**************************************************************
 * INCLUDES
 **************************************************************/
#include "mac_headers.h"


/**************************************************************
 * PUBLIC DEFINES
 **************************************************************/
#define DLE                                          0x10                        // escape char
#define SOF                                          0x12                        // start of frame delimiter
#define EOF                                          0x13                        // end of frame delimiter

/* Command types
 */
#define NO_CMD_TYPE									(UINT8)0x00
#define SET_CMD_TYPE                                (UINT8)0x01
#define EVENT_CMD_TYPE                              (UINT8)0x02
#define GET_WITH_ACK_CMD_TYPE                       (UINT8)0x04
#define SET_WITH_ACK_CMD_TYPE                       (UINT8)0x05
#define EVENT_WITH_ACK_CMD_TYPE                     (UINT8)0x06
#define GET_RESPONSE_CMD_TYPE                       (UINT8)0x08
#define SET_RESPONSE_CMD_TYPE                       (UINT8)0x09
#define EVENT_RESPONSE_CMD_TYPE                     (UINT8)0x0A


/* Command IDs
 */
#define NICTOR_SLEEP_UPDATE_CMD_ID                  (UINT8)0x01
#define NICTOR_ACTIVE_MODE_CMD_ID					(UINT8)0x02
#define NICTOR_IDLE_MODE_CMD_ID						(UINT8)0x03
#define NICTOR_ACTUATION_CMD_ID						(UINT8)0x04

#define NODE_NO_MORE_DATA_CMD_ID					(UINT8)0xA1
#define NODE_INFO_CMD_ID							(UINT8)0xA0
#define COORD_INFO_CMD_ID							(UINT8)0xA2

#define ECHO_DATA_CMD_ID							(UINT8)0xFE
#define FLOW_DATA_CMD_ID							(UINT8)0xFD



/*! Error codes
 */
#define ERROR_CODE_SUCCESS                          (UINT8)0x00
#define ERROR_CODE_INVALID_ENDPOINT                 (UINT8)0x01
#define ERROR_CODE_UNSUPPORTED_ATTRIBUTE            (UINT8)0x03
#define ERROR_CODE_INVALID_COMMAND_TYPE             (UINT8)0x04
#define ERROR_CODE_INVALID_ATTRIBUTE_DATA_LENGTH    (UINT8)0x05
#define ERROR_CODE_INVALID_ATTRIBUTE_DATA           (UINT8)0x06
#define ERROR_CODE_INVALID_REQUEST                  (UINT8)0x07
#define ERROR_CODE_NO_SHORT_ADDRESS                 (UINT8)0x08
#define ERROR_CODE_FAILED_TO_SEND_OTA               (UINT8)0x09
#define ERROR_CODE_FAILED_TO_CONFIRM_OTA            (UINT8)0x0A
#define ERROR_CODE_TIMEOUT                          (UINT8)0xFE
#define ERROR_CODE_FAILURE                          (UINT8)0xFF

#define NICTOR_SERIAL_INTERFACE_RX_BUFFER_SIZE 200
#define NICTOR_REMOTE_COMMAND_PAYLOAD_SIZE (NICTOR_SERIAL_INTERFACE_RX_BUFFER_SIZE - 5)   // 5 becase we subtract off type[1]/id[1]/err[1]/crc[2]


/**************************************************************
 * PUBLIC TYPEDEFS
 **************************************************************/
typedef struct
RemoteCommandStruct
{
    BYTE cmdType;
    BYTE cmdID;
    BYTE errorCode;
    BYTE cmdPayload[NICTOR_REMOTE_COMMAND_PAYLOAD_SIZE];
    UINT8 cmdPayloadLength;
    BOOL isCommand;
} RemoteCommand;


/**************************************************************
 * PUBLIC GLOBALS
 **************************************************************/
extern RemoteCommand remoteCommand;

/**************************************************************
 * PUBLIC FUNCTION PROTOTYPES
 **************************************************************/
void CMD_objectInit(void);

void CMD__sendFreeForm1(UINT16 numBytes, const char * pPayload);
void CMD__sendMessage1(UINT16 numBytes, const char * pPayload, BOOL isCharStuffing);
void CMD__sendFreeForm0(UINT16 numBytes, const char * pPayload);
void CMD__sendMessage0(UINT16 numBytes, const char * pPayload, BOOL isCharStuffing);
void CMD__clearRemoteCommandPayload();

#endif /* _CMD_INTERFACE_H_ */


